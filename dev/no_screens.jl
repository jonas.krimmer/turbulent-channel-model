include(joinpath("..", "examples", "repeated_wave_propagation.jl"))

"""
no_screens_convergence

This function simulates the propagation of a laser beam through a turbulent medium using a single, chunked phase screen. It calculates the overlap integral between the propagated beam and a reference Gaussian beam.

"""
function no_screens_convergence(; Nx=256, dx=5e-4, Dz=1e2, Nz=2, l=0, m=0, w0=1e-2, λ=1.55e-6, Cn²=1e-13, seed=0)

    # Generate spatial grid for FFTW
    xy = fftfreq(Nx, Nx * dx)
    # Define propagation distance array
    z = range(0, Dz, Nz)
    # Initialize beam with given parameters
    beam = Beam(l, m, λ, w0)

    # Validity check: calculate Rytov variance
    σr² = rytov_pw(beam.k0, step(z), Cn²)
    (σr² > 0.1) && (@warn "Rytov number > 0.1. Not within weak fluctuation regime.")
    
    # Calculate reference beam at distance Dz
    ref = broadcast(beam.fun, xy, transpose(xy), Dz)
    # Calculate source beam at initial position
    src = broadcast(beam.fun, xy, transpose(xy), 0)
    
    # Calculate Fried parameter
    r0 = fried_pw(beam.k0, Dz, Cn²)
    # Set random seed for reproducibility
    Random.seed!(seed)
    # Generate phase screen
    Φ = ft_phase_screen!(zeros(Nx, Nx); r0, dx)
    # Repeat (weighted) phase screen for each propagation step and convert to complex exponential
    R = repeat(Φ ./ (Nz - 1), 1, 1, Nz - 1) .|> cis 
    
    # Perform split-step propagation
    split_step_sym!(src, λ, dx, dx, z, R)
    
    # Calculate overlap integral between **propagated** source and reference beams
    coupling_efficiency(src, ref)
end


"""
sweep_no_screens

This function performs a parameter sweep over the number of propagation steps (Nz) and Nseed seeds for random number generation. It calculates the overlap integral for each combination and plots the results averaged over the different seeds.

"""
function sweep_no_screens(Nzmin, Nzmax, Nseeds=1; kwargs...)
    # Define range of propagation steps and seeds
    Nzs = range(Nzmin, Nzmax)
    seeds = range(0, Nseeds - 1)
    # Initialize overlap matrix
    overlap = zeros(length(Nzs), length(seeds))

    # Loop over seeds and propagation steps
    @showprogress for (ℓ, seed) ∈ enumerate(seeds)
        for (k, Nz) ∈ enumerate(Nzs)
            # Calculate overlap integral for each combination
            overlap[k, ℓ] = no_screens_convergence(; Nz, seed, kwargs...)
        end
    end

    # Calculate mean overlap integral across seeds
    moverlap = mean(overlap, dims=2) |> vec

    # Create scatter plot of overlap integral vs. number of screens
    plt = plot.scatter(x=Nzs, y=moverlap)
    plt.layout.xaxis.title = "Number of screens"
    plt.layout.yaxis.title = "Overlap with Gaussian"

    plt
end