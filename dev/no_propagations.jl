include(joinpath("..", "examples", "repeated_wave_propagation.jl"))

"""
numerical_vs_analytical_lg

This function simulates the propagation of a Laguerre Gaussian beam through vacuum. It calculates the overlap integral between the numerically propagated beam and an analytically computed reference Gaussian beam.

"""
function numerical_vs_analytical_lg(; Nx=256, dx1=5e-4, dxn=dx1, Dz=1e2, Nz=2, l=0, m=0, w0=1e-2, λ=1.55e-6, debug=false)
    # Generate spatial grids for FFTW
    xy0 = fftfreq(Nx, Nx * dx1)
    xyn = fftfreq(Nx, Nx * dxn)
    # Define propagation distance array
    z = range(0, Dz, Nz)
    # Accordingly, consider linear increase of grid spacing 
    dx = range(dx1, dxn, Nz)
    
    # Initialize beam with given parameters
    beam = Beam(l, m, λ, w0)

    # Calculate Rayleigh range and beam waist at distance Dz
    zr = beam.k0 * beam.w0^2 / 2
    wz = beam.wlm * sqrt(1 + Dz^2 / zr^2)

    # Check sampling requirements for angular spectrum method
    check_sampling(z, Nx, dx1, 2beam.w0, dxn, min(2wz, 0.94Nx * dxn); λ, debug)

    # Calculate source beam at initial position
    approx = broadcast(beam.fun, xy0, transpose(xy0), 0)
    # Calculate reference beam at distance Dz
    ref = broadcast(beam.fun, xyn, transpose(xyn), Dz)

    # Perform split-step propagation without phase screens
    R = ones(ComplexF64, Nx, Nx, Nz - 1)
    split_step_asym!(approx, λ, dx, z, R)

    # Optional: Plot the propagated and reference beams
    if debug
        x0n = minimum(xyn)
        # highlight the 1/e-threshold of the super-Gaussian window
        sg_th = 0.47 * Nx * dxn
        shapes = [Config(type="circle", x0=-sg_th, x1=sg_th, y0=-sg_th, y1=sg_th, line=Config(width=2, dash="dash", color="white"))]
        # plot numerical result
        Plot(complex_plot(; z=fftshift(approx), x0=x0n, dx=dxn, y0=x0n, dy=dxn), Config(xaxis=Config(title="x"), yaxis=Config(title="y", scaleanchor="x"), title="Numerical result", showscale=true; shapes)) |> display
        # plot analytical result
        Plot(complex_plot(; z=fftshift(ref), x0=x0n, dx=dxn, y0=x0n, dy=dxn), Config(xaxis=Config(title="x"), yaxis=Config(title="y", scaleanchor="x"), title="Analytical result", showscale=true; shapes)) |> display
    end
    
    # Calculate overlap integral between approximate and reference beams
    coupling_efficiency(approx, ref)
end

"""
sweep_no_propagations

This function performs a parameter sweep over the number of propagation steps (Nz). It calculates the overlap integral for each value of Nz and plots the results.
"""
function sweep_no_propagations(Nzmin, Nzmax; kwargs...)
    # Define range of propagation steps
    Nzs = range(Nzmin, Nzmax)
    # Initialize overlap array
    overlap = zeros(length(Nzs))
    
    # Loop over propagation steps
    for (k, Nz) ∈ enumerate(Nzs)
        # Calculate overlap integral for each number of steps
        overlap[k] = numerical_vs_analytical_lg(; Nz, kwargs...)
    end

    # Create scatter plot of overlap integral vs. number of planes
    plt = plot.scatter(x=Nzs, y=overlap)
    plt.layout.xaxis.title = "Number of planes"
    plt.layout.yaxis.title = "Overlap with Gaussian"

    plt
end