using CSV

include(joinpath("..", "examples", "repeated_wave_propagation.jl"))
include(joinpath("..", "examples", "analyze_turbulent_channel_model.jl"))

"""
    batch_scintillation_index(argname=:Nz, xvals; iters=100, λ=1.55e-6, kwargs...)

    xvals has to be a vector/range/any iterable
"""
function batch_scintillation_index(args=Dict(:Nz => 4:16, :dz => 1e3 ./ (3:15)); iters=100, λ=1.55e-6, showprogress=true, samplingcheck=false, kwargs...)

    # make sure, that all value arrays of the argument dict are of the same size
    valuelen = length.(values(args))
    Nv = minimum(valuelen)
    @assert isone(length(unique(valuelen)))

    dir = timeprefix(fmt="YYYY-mm-dd")

    for (k, v) ∈ pairs(args)
        vmin, vmax = extrema(v)
        dir *= "$(k)_$(round(vmin,sigdigits=3))_$(round(vmax,sigdigits=3))"
    end

    if isdir(dir)
        if !isempty(readdir(dir))
            @warn "Directory $dir already exists! Do you want to write into this directory anyway? (y/N)"
            response = lowercase(strip(readline()))
            if response == "y"
                println("Continuing...")
            else
                println("Exiting...")
                return
            end
        end
    else
        mkdir(dir)
    end

    p = Progress(Nv; showspeed=true, enabled=showprogress)
    
    if :iters ∈ keys(args)
        # assume no other parameter is swept
        (length(args) > 1) && @warn "Other parameters besides iters are ignored..."
        for i ∈ 1:Nv
            xargs = (; (k => v[i] for (k, v) ∈ pairs(args))...)
            # we have to sort the outputs, why we prepend a number to the filenames
            repeated_wave_propagation(args[:iters][i]; λ, dir, fnprefix=lpad(i, ceil(Int, log10(Nv)), "0") * "_", showprogress=false, samplingcheck, meta=xargs, kwargs...)
            sleep(0.1)
            next!(p, showvalues = [(string(k), v) for (k, v) ∈ pairs(xargs)])
        end
    else
        for i ∈ 1:Nv
            xargs = (; (k => v[i] for (k, v) ∈ pairs(args))...)
            # we have to sort the outputs, why we prepend a number to the filenames
            repeated_wave_propagation(iters; xargs..., λ, dir, fnprefix=lpad(i, ceil(Int, log10(Nv)), "0") * "_", showprogress=false, samplingcheck, meta=xargs, kwargs...)
            sleep(0.1)
            next!(p, showvalues = [(string(k), v) for (k, v) ∈ pairs(xargs)])
        end
    end

    # Save to CSV
    df = DataFrame(args)
    open(joinpath(dir, "xvals.csv"), "w") do fid
        CSV.write(fid, df)
    end

    nothing
end

function analyze_batch_scintillation_index(dirname; xscale="linear", yscale="linear", dfcol=1)

    # gather all hdf5 files in the given directory `dirname`
    files = readdir(dirname, join=true)
    csvidx = findfirst(contains("xvals.csv"), files)
    h5files = filter(x -> splitext(x)[2] == ".hdf5", files)

    Nf = length(h5files)

    # gather xvals from corresponding csv if possible - otherwise 1:N
    xval_df = if isnothing(csvidx)
        DataFrame(:File => range(1, Nf))
    else
        CSV.read(files[csvidx], DataFrame)
    end

    # choose column of xval if multiple parameters have been swept simultaneously
    xvals = xval_df[!, names(xval_df)[dfcol]]

    SI_est = zeros(Nf)
    SI_pw = zeros(Nf)
    SI_sw = zeros(Nf)
    SI_gaussian = zeros(Nf)

    for (k, file) ∈ enumerate(h5files)
        SI_est[k] = scintillation_index(file)

        metadata = h5open(file, "r") do h5
            Dict(attrs(h5))
        end

        Cn² = metadata["Cn2"]
        λ = metadata["lambda"]
        Dz = metadata["Dz"]
        w0 = metadata["w0"]
        
        l0 = metadata["l0"]
        L0 = metadata["L0"]

        # the transverse grid resolution is inherently an inner scale of turbulence
        dxm = maximum(metadata["dx"])
        l0 = min(l0, dxm)

        SI_pw[k] = scintillation_index_pw(2π / λ, Dz, Cn², l0, L0)
        SI_sw[k] = scintillation_index_sw(2π / λ, Dz, Cn², l0, L0)
        SI_gaussian[k] = scintillation_index_gaussian(2π / λ, Dz, Cn², w0, l0, L0)
    end

    # visualize results
    plt = plot.scatter(x=xvals, y=SI_est, name="Estimated SI")
    plt.scatter(x=xvals, y=SI_gaussian, name="SI - Gaussian")
    plt.scatter(x=xvals, y=SI_pw, line=Config(color="black"), name="SI - PW")
    plt.scatter(x=xvals, y=SI_sw, line=Config(color="gray"), name="SI - SW")
    # set lower yaxis limit to 0
    yl = plt.layout.yaxis.range
    yl[1] = 0
    plt.layout.yaxis.range = yl
    # set x/y axis to logarithmic
    plt.layout.xaxis.type = xscale
    plt.layout.yaxis.type = yscale

    plt
end