using LinearAlgebra
using Random

include("../src/phase_spectrum.jl")
include("../src/estimate_statistics.jl")

function covariance_phase_screen(r0=0.1, N=4, dx=2 / N, l0=1e-2, L0=1e2, Nscreens=1; model=andrews)

    S = phase_spectrum(r0, l0, L0, model)

    x0 = range(0.0, length=N)
    x, y = repeat(x0, outer=N), repeat(x0, inner=N)
    d² = (x .- transpose(x)) .^ 2 .+ (y .- transpose(y)) .^ 2
    # the first column/row of the (squared) distance matrix already contains all the occurrences
    ud² = unique(view(d², :, 1))
    uB = num_cov(S, sqrt.(ud²) .* dx, 0, 1e-8)

    map = Dict(zip(ud², uB))

    B = d²
    @inbounds for k ∈ eachindex(B)
        B[k] = map[B[k]]
    end

    x = randn(N^2, Nscreens)
    L, _ = cholesky!(Symmetric(B))
    L * x
end

# using PlotlyJS
# function test_cov_screen(; iters=100, Nx=32, d=1e-1, model=andrews, seed=0)

#     r0 = 0.1
#     dx = d / Nx
#     l0 = 1e-3
#     L0 = 1e1

#     D = zeros(Nx)
#     Random.seed!(seed)
#     S = covariance_phase_screen(r0, Nx, dx, l0, L0, iters; model)

#     Sr = reshape(S, Nx, Nx, :)
#     D = 0.5(sf_ft(Sr, dim=1) .+ sf_ft(Sr, dim=2))


#     r = (0:Nx-1) * dx
#     D_th = num_sf(phase_spectrum(r0, l0, L0, model), r, 0)

#     relerr(x, y) = iszero(y) ? zero(x) : abs(1 - x / y)
#     plot([scatter(x=r, y=D_th, name="Theoretical"); scatter(x=r, y=D, name="Achieved")]) |> display
#     plot([scatter(x=r, y=relerr.(D, D_th), name="Rel. error")]) |> display

#     nothing
# end