using PlotlyLight
using ProgressMeter

include("../src/ft_phase_screen.jl")
include("../src/estimate_statistics.jl")

num_sf_3d(S::Function, r, κmin=zero(eltype(r)), rtol=1e-6) = quadgk(κ -> 8π * (1 .- sinc.(κ * r / π)) * S(κ) * κ^2, κmin, Inf, rtol=rtol)[1]

function spectral_coefficients_3d!(c::AbstractArray{Complex{T},3}, Φ::Function, κx::AbstractVector, κy::AbstractVector, κz::AbstractVector, dκx::AbstractVector, dκy::AbstractVector, dκz::AbstractVector; δκx=zero(T), δκy=zero(T), δκz=zero(T)) where {T<:Real}

    randn!(c)

    @inbounds for I ∈ CartesianIndices(c)
        m, n, o = I.I
        κ = sqrt((κx[m] + δκx)^2 + (κy[n] + δκy)^2 + (κz[o] + δκz)^2)
        # factor two for correct variance in real and imaginary parts
        c[I] *= sqrt(2Φ(κ) * dκx[m] * dκy[n] * dκz[o])
        iszero(κ) && (c[I] = 0)
    end
end


function spectral_grid(N, dx; uniform=false, fun="asinh")
    if uniform
        dκ = 2(π/(N*dx))
        κ = fftfreq(N, N*dκ) |> fftshift |> collect
        κ, fill(dκ, N)
    else
        nu_spectral_grid(N, dx; sym=true, fun)
    end
end


function phase_profile!(screen::AbstractArray{Complex{T},3}; Cn²=1e-14, Nx=size(screen, 1), Ny=size(screen, 2), Nz=size(screen, 3), dx=1e0 / Nx, dy=dx, dz=1e1 / Nz, l0=1e-3, L0=1e1, centered::Bool=true, tol=1e-5, fun="asinh", uniform=false, nthreads=Threads.nthreads(), upsampfac=0.0, model=andrews) where {T<:Union{Float32,Float64}}

    # setup the spectrum
    κ0 = 2(π / L0)
    κl = (33 // 10) / l0
    κm = (592 // 100) / l0
    Φ = model(0.033Cn²; κ0, κl, κm)

    # preallocate complex fourier coefficients
    cn = zeros(complex(T), Nx, Ny, Nz)

    # preallocate frequency grid
    κx, dκx = spectral_grid(Nx, T(dx); uniform, fun)
    κy, dκy = spectral_grid(Ny, T(dy); uniform, fun)
    κz, dκz = spectral_grid(Nz, T(dz); uniform, fun)

    spectral_coefficients_3d!(cn, Φ, κx, κy, κz, dκx, dκy, dκz)

    # frequencies in range [-3π,3π] required ⟹ normalization
    κx .*= dx
    κy .*= dy
    κz .*= dz
    # CMCL mode ordering (neg. to pos.) ⟹ 0 OR FFTW mode ordering ⟹ 1
    modeord = Int(!centered)
    
    # synthesize the phase screen
    nufft3d1!(ndgrid(κx, κy, κz)..., cn, 1, T(tol), screen; nthreads, upsampfac, modeord)
end


function statistics_3d_screen(iters=100; Nx=64, Ny=Nx, Nz=Nx, dx=1e-3, dy=dx, dz=1e-1, Cn²=1e-14, seed=0, chunk=Nz, uniform=false, fun="sqrt", model=andrews, showplot=true)
    
    T = Float32

    k0 = 2π / 1.55e-6
    l0 = 1e-2
    L0 = 1e1
    κ0 = 2(π / L0)
    κl = (33 // 10) / l0
    κm = (592 // 100) / l0

    cmplx_screen = zeros(Complex{T}, Nx, Ny, Nz)

    # use real and imaginary parts --> half the number of iterations required
    iters >>= 1

    Dx = zeros(T, Nx, iters)
    Dy = zeros(T, Ny, iters)
    Dz = zeros(T, Nz, iters)

    Dsx = zeros(T, Nx, iters)
    Dsy = zeros(T, Ny, iters)

    Nchunk = fld(Nz, chunk)
    cmplx_screen_chunk = zeros(Complex{T}, Nx, Ny)

    prg = Progress(iters, desc="Iterate...")

    #Threads.@threads 
    for i ∈ Base.OneTo(iters)
        Random.seed!(seed + i)
        phase_profile!(cmplx_screen; dx, dy, dz, Cn², l0, L0, uniform, fun, model)

        Dx[:, i] .= sf_ft(cmplx_screen, dim=1)
        Dy[:, i] .= sf_ft(cmplx_screen; dim=2)
        Dz[:, i] .= sf_ft(cmplx_screen; dim=3)

        @inbounds for n ∈ Base.OneTo(Nchunk)
            for ℓ ∈ axes(cmplx_screen, 2), k ∈ axes(cmplx_screen, 1)
                for m ∈ range((n - 1) * chunk + 1, n * chunk)
                    cmplx_screen_chunk[k, ℓ] += cmplx_screen[k, ℓ, m]
                end
                cmplx_screen_chunk[k, ℓ] *= k0 * dz
            end

            Dsx[:, i] .+= sf_ft(cmplx_screen_chunk; dim=1)
            Dsy[:, i] .+= sf_ft(cmplx_screen_chunk; dim=2)

            fill!(cmplx_screen_chunk, 0)
        end

        next!(prg)
    end

    Dx ./= 2
    Dy ./= 2
    Dz ./= 2

    Dsx ./= 2Nchunk
    Dsy ./= 2Nchunk

    x = (0:(Nx-1)) * dx
    y = (0:(Ny-1)) * dy
    z = (0:(Nz-1)) * dz

    Φ = model(0.033Cn²; κ0, κl, κm)
    Dx_th = num_sf_3d(Φ, x, 0)
    Dy_th = num_sf_3d(Φ, y, 0)
    Dz_th = num_sf_3d(Φ, z, 0)

    r0_chunk = (0.423 * k0^2 * dz * chunk * Cn²)^(-3 / 5)
    Φ_chunk = model(0.49r0_chunk^(-5 // 3); κ0, κl, κm)
    Dsx_th = num_sf(Φ_chunk, x, 0)
    Dsy_th = num_sf(Φ_chunk, y, 0)

    if showplot
        Plot([Config(x=x, y=Dx_th, name="Theoretical"), Config(x=x, y=mean(Dx, dims=2) |> vec, name="D along x")]) |> display
        # Plot([Config(x=y, y=Dy_th, name="Theoretical"), Config(x=y, y=mean(Dy, dims=2) |> vec, name="D along y")]) |> display
        Plot([Config(x=z, y=Dz_th, name="Theoretical"), Config(x=z, y=mean(Dz, dims=2) |> vec, name="D along z")]) |> display

        # Plot([Config(x=x, y=Dsx_th, name="Theoretical"), Config(x=x, y=mean(Dsx, dims=2) |> vec, name="D along x")]) |> display
        # Plot([Config(x=y, y=Dsy_th, name="Theoretical"), Config(x=y, y=mean(Dsy, dims=2) |> vec, name="D along y")]) |> display
    end

    nothing
end



function plot_3d_screen(; Nx=64, Ny=Nx, Nz=Nx, dx=1e-1, dy=dx, dz=1e-1, Cn²=1e-14)

    k0 = 2π / 1.55e-6
    r0 = (0.423 * k0^2 * dz * Cn²)^(-3 / 5)

    cmplx_screen = zeros(ComplexF32, Nx, Ny, Nz)
    phase_profile!(cmplx_screen; Nx, Ny, Nz, dz, r0)

    x = fftfreq(Nx, Nx * dx) |> fftshift
    y = fftfreq(Ny, Ny * dy) |> fftshift
    z = fftfreq(Nz, Nz * dz) |> fftshift

    screen = real(cmplx_screen)
    cmax = maximum(abs, screen)

    xg, yg, zg = ndgrid(x, y, z)
    fig = plot.volume(x=vec(xg), y=vec(yg), z=vec(zg), value=vec(screen), cmin=-cmax, cmax=cmax, isomin=nothing, isomax=nothing, surface=Config(count=9, show=true), opacity=0.2, opacityscale="extremes") # , slices=Config(x_show=true, y_show=true, z_show=true)
    fig.layout = Config(
        scene=Config(
            aspectratio=Config(x=1, y=1, z=1), 
            xaxis=Config(range=extrema(x)),
            yaxis=Config(range=extrema(y)),
            zaxis=Config(range=extrema(z))
        )
    )

    fig
end


sph2cart(ϕ, ϑ, r) = r * sin(ϑ) * cos(ϕ), r * sin(ϑ) * sin(ϕ), r * cos(ϑ)
# special function operating in-place on 3d grid
function sph2cart!(ϕ, ϑ, r) # ϕ -> x, ϑ -> y, r -> z
    @inbounds for k ∈ eachindex(ϕ, ϑ, r)
        ϕ[k], ϑ[k], r[k] = sph2cart(ϕ[k], ϑ[k], r[k])
    end
end

function calculate_isotropic3d_structure_function_ft(x0::AbstractArray{T,N}; tol=1e-5, sf_len=size(x0, 1), nthreads=1) where {T,N}
    # calculate avg. structure function along dimension dim, compare Marcotte 1996
    fftlen = 2size(x0, 1), 2size(x0, 2), 2size(x0, 3)

    # window function/indicator matrix
    w = zeros(T, fftlen..., ntuple(k -> size(x0, k + 3), Val(N - 3))...)
    w[CartesianIndices(axes(x0))] .= one(T)

    r = range(0, real(T)(2π), length=fftlen[1])
    ϑ = range(0, real(T)(π), length=fftlen[2])
    ϕ = range(0, real(T)(2π), length=fftlen[3])
    grid = ndgrid(ϕ, ϑ, r)
    sph2cart!(grid...)
    

    # zeropad to fftlen
    x = zeros(T, fftlen..., ntuple(k -> size(x0, k + 3), Val(N - 3))...)
    x[axes(x0)...] .= x0

    P = plan_fft(x, 1:3)

    X = P * x
    @. x = abs2(x)
    X2 = P * x
    W2 = P * w # w^2 = w

    # compute variogram
    @. X = 2 * (real(X2 * conj(W2)) - abs2(X))
    # compute number of pairs at all lags
    @. W2 = abs2(W2)

    D0 = real(nufft3d2(grid..., 1, tol, X, modeord=1; nthreads) ./ nufft3d2(grid..., 1, tol, W2, modeord=1; nthreads))

    D = view(reshape(D0, fftlen..., :), axes(x0)...)
    # deleteat!(mean(D, dims=(1, 4:N...)) |> vec, sf_len+1:size(D, 2))
    mean(D, dims=1:2) |> vec
end