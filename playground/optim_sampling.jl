include("../src/phase_spectrum.jl")

using QuadGK
using Optim
using SparseArrays
using LinearAlgebra

logrange(x1, x2, n) = (sign(y) * (10^abs(y) - 1) for y in range(sign(x1) * log10(abs(x1) + 1), sign(x2) * log10(abs(x2) + 1), length=n))
asinhrange(x1, x2, n) = (sinh(y) for y in range(asinh(x1), asinh(x2), length=n))
sqrtrange(x1, x2, n) = (sign(y) * y^2 for y in range(sign(x1) * sqrt(abs(x1)), sign(x2) * sqrt(abs(x2)), length=n))



function main(Nx=128, d=1e-1; r0=5e-2, l0=1e-3, L0=1e1)

    Φ = spectrum(r0, l0, L0)
    dx = d/Nx

    ref, ϵ = quadgk(Φ, 0, Inf)
    f(κ) = (mapreduce(x -> Φ((κ[x+1] + κ[x])/2)*(κ[x+1] - κ[x]), +, 1:length(κ)-1) - ref)^2

    
    testfun(rangefun, κmin, κmax) = rangefun(κmin, κmax, Nx)
    κmax = π / dx
    
    for fun ∈ (asinhrange, threeelevenrange, logrange)
        κmin = (fun == asinhrange) ? 0 : min(2π / 100L0, 2π / d)
        κ = testfun(fun, κmin, κmax) |> collect
    
        dκ = [fdiff(κ, n) |> abs for n ∈ eachindex(κ)]

        res = mapreduce(x -> Φ(κ[x])*fdiff(κ, x), +, eachindex(κ))
        @info "Rel. approximation error with $fun:" abs(1 - res/ref)
    end

    κ0 = testfun(asinhrange, 0, π/l0) |> collect
    kmin = fill(0.0, size(κ0))
    kmax = fill(π/l0, size(κ0))
    # @time opt = optimize(f, kmin, kmax, κ0, autodiff=:forward)
    # kmin = κ0[1:end-1]
    # kmax = κ0[2:end]
    # @time opt = optimize(f, kmin, kmax, fill(π/2l0, Nx), autodiff=:forward)

    # κ0 = fill(π/2l0, Nx)
    κ0 = range(π/4l0, 3π/4l0, length=Nx) |> collect
    A = spdiagm(0 => ones(Nx-1), 1 => -ones(Nx-1))[1:end-1,:]
    con_c!(c, x) = (mul!(c, A, x); c)
    df = TwiceDifferentiable(f, κ0, autodiff=:forward)
    dfc = TwiceDifferentiableConstraints(con_c!, kmin, kmax, fill(-Inf, Nx-1), zeros(Nx-1), :forward)
    # dfc = TwiceDifferentiableConstraints(kmin, kmax)
    @time opt = optimize(df, dfc, κ0, IPNewton(), autodiff=:forward)
    display(opt)
    κ = Optim.minimizer(opt)
end


# function optim_spectral_grid(N, dx, Φ, l0)

#     ref, ϵ = quadgk(Φ, 0, Inf)

#     κmax = π / max(l0, dx)

#     f(κ) = (mapreduce(x -> Φ(κ[x])*fdiff(κ, x), +, eachindex(κ)) - ref)^2

#     κa = collect(asinhrange(0, κmax, (N >> 1) + 1))
#     kmin = κa[1:end-1]
#     kmax = κa[2:end]
#     opt = optimize(f, kmin, kmax, 0.5.*(κa[1:end-1] .+ κa[2:end]), autodiff=:forward)
#     κ0 = Optim.minimizer(opt)
#     κ = vcat(-reverse(κ0), κ0)
# end

function optim_spectral_grid(N, dx, Φ, l0)

    ref, ϵ = quadgk(Φ, 0, Inf)

    κmax = π / max(l0, dx)

    f(κ) = (mapreduce(x -> Φ(κ[x])*fdiff(κ, x), +, eachindex(κ)) - ref)^2

    κa = collect(asinhrange(0, κmax, (N >> 1) + 1))
    kmin = κa[1:end-1]
    kmax = κa[2:end]
    opt = optimize(f, kmin, kmax, 0.5.*(κa[1:end-1] .+ κa[2:end]), autodiff=:forward)
    κ0 = Optim.minimizer(opt)
    κ = vcat(-reverse(κ0), κ0)
end


function integration_accuracy(Nx=128, d=1e-1; r0=5e-2, l0=1e-3, L0=1e1)

    Φ = spectrum(r0, l0, L0)

    ref_asym, _ = quadgk(Φ, 0, Inf)
    ref_sym, _ = quadgk(Φ, 0, Inf)
    ref_sym *= 2

    testfun(rangefun, κmin, κmax) = rangefun(κmin, κmax, Nx)
    κmax = π / l0
    
    for fun ∈ (asinhrange, threeelevenrange, logrange)
        κmin = (fun == asinhrange) ? 0 : min(2π / 100L0, 2π / d)
        κ = testfun(fun, κmin, κmax) |> collect
    
        dκ = [fdiff(κ, n) |> abs for n ∈ eachindex(κ)]

        res_asym = mapreduce(x -> Φ(abs(κ[x]))*fdiff(κ, x), +, eachindex(κ))
        @info "Rel. approximation error with $fun:" abs(1 - res_asym/ref_asym)



        if (fun == asinhrange)
            κ = testfun(fun, -κmax, κmax) |> collect
        else
            κ0 = testfun(fun, κmin, κmax) |> collect
            κ = vcat(-reverse(κ0), κ0)
        end
    
        dκ = [fdiff(κ, n) |> abs for n ∈ eachindex(κ)]

        res_sym = mapreduce(x -> Φ(abs(κ[x]))*fdiff(κ, x), +, eachindex(κ))
        @info "Rel. approximation error with $fun:" abs(1 - res_sym/ref_sym)
    end

end