include(joinpath("..", "src", "helpers.jl"))
include(joinpath("..", "src", "newtypes.jl"))
include(joinpath("..", "src", "estimate_statistics.jl"))
include(joinpath("..", "src", "freespace_coupling.jl"))
include(joinpath("..", "src", "plot_basics.jl"))

using HDF5
using H5Zblosc
using FourierTools


function mdl(H)
    σ = svdvals(H)
    return -20log10(minimum(σ) / maximum(σ))
end

function il(H)
    σ = svdvals(H)
    return -10log10(mean(abs2, σ))
end

"""

fun has to be a function accepting a single argument, i.e., the transfer matrix.

"""

function tf_stats(urx::AbstractArray, utx::AbstractArray, statfun::Function=mdl)

    ntxmodes = size(utx, 3)
    nrxmodes = size(urx, 3)
    iters = size(urx, 4)

    # transfer matrix
    H = zeros(ComplexF64, nrxmodes, ntxmodes)
    stat = zeros(iters)

    @views for i ∈ axes(urx, 4)
        # compute transfer matrix
        transfer_matrix!(H, urx[:, :, :, i], utx)
        stat[i] = statfun(H)
    end

    return stat
end


"""

midx is an array of mode index tuples
"""
function optimize_reference_modes(urx::AbstractArray{Complex{T}}, midx, domain::Domain, λ; target::Function=mean, show_trace=true) where {T<:Real}

    modes = zeros(ComplexF64, domain.Nx, domain.Ny, length(midx))
    x = domain.x(domain.Nz)
    yt = transpose(domain.y(domain.Nz))

    set_tx_modes!(modes, w0, z) = begin
        for (k, m) ∈ enumerate(midx)
            # we optimize w.r.t. w0...
            _lgfun_m = lg_fun_cart(m[1], m[2], w0, λ)
            # set distance from focus
            broadcast!(_lgfun_m, view(modes, :, :, k), x, yt, z)
        end
    end

    J(q) = begin
        set_tx_modes!(modes, q[1], domain.Dz)
        mdl = tf_stats(urx, modes, mdl)
        return target(mdl)
    end

    # opt = optimize(J, [min(domain.Dx, domain.Dy) / 4], NelderMead(), Optim.Options(; show_trace, iterations=50))
    opt = optimize(J, [min(domain.dx, domain.dy)], [Inf], [min(domain.Dx, domain.Dy) / 4], Fminbox(NelderMead()), Optim.Options(; show_trace, iterations=50, outer_iterations=5, outer_f_tol=1e-3)) # we optimize for a dB-value, i.e., do not care for deviations ~ 1e-3

    w0 = opt.minimizer[1]
    # z0 = opt.minimizer[2]

    # @info w0
    # @info (w0, z0)

    set_tx_modes!(modes, w0, domain.Dz)

    # # plot one of the output modes for debugging purposes 
    # display(Plot(complex_plot(z=modes[:, :, 2])))

    return modes
end


"""

"""
function generate_reference_modes(midx, domain::Domain{T}, λ, w0) where {T}
    modes = zeros(Complex{T}, domain.Nx, domain.Ny, length(midx))
    x = domain.x(domain.Nz)
    yt = transpose(domain.y(domain.Nz))

    for (k, m) ∈ enumerate(midx)
        # we optimize w.r.t. w0...
        _lgfun_m = lg_fun_cart(m[1], m[2], w0, λ)
        # set distance from focus
        broadcast!(_lgfun_m, view(modes, :, :, k), x, yt, domain.Dz)
    end

    modes
end


"""

transfer_matrix!()

    The input arrays uin and uout are three dimensional arrays, where the different modes are concatenated along the third dimension.

"""
function transfer_matrix!(H, uout::AbstractArray{T1,3}, uin::AbstractArray{T2,3}) where {T1,T2}
    for n ∈ axes(uin, 3)
        for m ∈ axes(uout, 3)
            @views H[m, n] = overlap_integral(uout[:, :, m], uin[:, :, n])
        end
    end
end

"""
mdl_statistics()

"""

function sdm_statistics(fn; ng=nothing, basedir=pwd(), Nopt=10, statfun=mdl, target=mean, show_trace=false, showplot=false)

    fn = joinpath(basedir, fn)

    # load data & metadata   
    urx, metadata = h5open(fn, "r") do h5
        read(h5["RXFields"]), Dict(attrs(h5))
    end

    midx = if isnothing(ng)
        values.(metadata["modes"])
    else
        midx = sort!([(m, n) for m ∈ range(0, ng - 1) for n ∈ range(0, ng - m - 1)], by=sum)
    end

    λ = metadata["lambda"]

    Nx = metadata["Nx"]
    Ny = metadata["Ny"]
    Nz = metadata["Nz"]
    dx = metadata["dx"]
    dy = metadata["dy"]
    dz = metadata["dz"]
    w0 = metadata["w0"]

    T = eltype(dx)
    domain = Domain{T}(dx[end], dy[end], dz, Nx, Ny, Nz)

    # TODO: we have to optimize w0, but over how many iterations? Convergence analysis? Too many iterations seem to make the optimization quite challenging and might result in bad convergence
    # reference_modes = optimize_reference_modes(view(urx, :, :, :, 1:Nopt), midx, domain, λ; target, show_trace)
    # lazy or more correct? Consider the analytical LG modes in distance domain.Dz with the given w0
    reference_modes = generate_reference_modes(midx, domain, λ, w0)

    stat = tf_stats(urx, reference_modes, statfun)

    @info statfun
    summarystats(stat) |> display

    if showplot
        x, y = estimate_ccdf(stat)
        plot.scatter(; x, y)
    end
end