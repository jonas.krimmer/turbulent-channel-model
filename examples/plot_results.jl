using FourierTools: center_extract

function plot_rxfields(; Nx=256, Nz=11, dx1=1e-3, dxn=dx1, dz=1e2, l=0, m=0, w0=1e-2, λ=1.55e-6, Cn²=1e-14, l0=1e-3, L0=1e1, model=andrews, uniform::Bool=false, collimate=true, seed=1)
    
    # set up domain and channel elements
    domain = if dx1 == dxn
        Domain(dx1, dz, Nx, Nz)
    else
        Domain(dx1, dxn, dz, Nx, Nz)
    end
    channel = TurbulentChannel(Cn², l0, L0, model)

    # set up source beam
    beam = Beam(l, m, λ, w0)
    # estimated beam waist radius at receiver
    zr = beam.k0 * beam.w0^2 / 2
    wz = beam.wlm * sqrt(1 + domain.Dz^2 / zr^2)
    # generate complex amplitude
    u = gaussian_source(domain, w0, λ, (l, m))

    # Fried param
    r0 = partial_r0_pw(beam.k0, domain.z, channel.Cn²)
    r0eff = fried_pw(beam.k0, domain.Dz, channel.Cn²)

    # Check sampling requirements for angular spectrum method
    D1 = 2w0 # source plane ROI
    Dn = 4wz # observation plane ROI
    @info "Source plane ROI diameter: " D1
    @info "Observation plane ROI diameter: " Dn
    check_sampling(domain, D1, Dn; λ, r0=r0eff)

    # reproducible results
    Random.seed!(seed)

    # geometric loss
    P1 = sum(abs2, u) * dx1^2

    # propagate
    if !iszero(abs(dz))
        turbulent_channel!(u, r0, domain, channel; uniform)
    end

    # geometric loss
    Pn = sum(abs2, u) * dxn^2
    @info "$(Pn / P1 * 100) % of the power remains within the domain."

    # get axes
    xn = domain.x(domain.Nz)
    yn = domain.y(domain.Nz)
    # optionally collimate the beam
    if collimate
        if domain.Dz < zr
            @warn "Still within Rayleigh length, collimation does not make sense."
        end
        ynt = transpose(yn)
        @. u *= cispi(1 / (λ * domain.Dz) * (xn^2 + ynt^2))
    end

    z = Adapt.adapt(Array, u) |> fftshift
    # plot receive field
    tr = complex_plot(; x=xn, y=yn, z)
    # reference circle → observer ROI 
    shapes = [Config(type="circle", x0=-Dn / 2, x1=Dn / 2, y0=-Dn / 2, y1=Dn / 2, line=Config(width=1, dash="dash", color="white"))]
    # autorange=true avoids the flipped y-axis for the (complex) image plot 
    lt = Config(; xaxis=Config(title="x"), yaxis=Config(title="y", scaleanchor="x", autorange=true), showscale=true, shapes)
    Plot(tr, lt) |> display

    return
end

# TODO: maybe deprecate?
function plot_fields(T::DataType=Float64; domain::Domain=Domain{T}(), beam::Beam=Beam{T}(), channel::TurbulentChannel=TurbulentChannel{T}(), target=FiberModes(9e-6, 0.1, 7.8e-6), uniform::Bool=false, seed=1, n_os=1, plotrx=true, plotfocused=false, plotmodes=false, collimate=false)

    # Calculate Fried parameter, Rytov variance, Rayleigh range, and beam waist at distance Dz
    r0eff = fried_pw(beam.k0, domain.Dz, channel.Cn²)
    r0 = partial_r0_pw(beam.k0, domain.z, channel.Cn²)
    σᵣ² = rytov_pw(beam.k0, domain.Dz, channel.Cn²)
    zr = beam.k0 * beam.w0^2 / 2
    wz = beam.wlm * sqrt(1 + domain.Dz^2 / zr^2)

    @info "D/r0: " 2wz / r0eff
    @info "σᵣ²: " σᵣ²

    # compute optimal focal length
    f = optimize_focus(domain, beam, target; debug=true)

    # focal plane grid and destination boundaries
    ref = _magnification_ref(target, beam.k0)
    dxf = beam.λ * f / domain.Dx[end]
    dyf = dxf
    # dxf, dyf = focal_plane_res(domain, beam, target, f)
    # generate modes in observation plane, focal plane resolution manually specified. Critical sampling for dx = beam.λ*f/domain.d
    push!(target.modes, pairs(generate_fiber_modes(domain.Nx, domain.Ny, dxf, dyf, target.a, target.a * beam.k0 * target.NA))...)

    # check sampling requirements for focusing simulation
    check_sampling(f, domain.Nx, domain.dx[end], 4wz, dxf, 4ref, R=-f)

    # reproducible results
    maybe_cuda_seed!(seed)

    # generate Gaussian beam
    u = maybe_cuda(broadcast(beam.fun, domain.x(1), domain.y(1) |> transpose, zero(T)))
    # propagate and couple
    urx = turbulent_channel!(u, r0, domain, channel; uniform)

    # optionally collimate the beam
    if collimate
        if domain.Dz < zr
            @warn "Still within Rayleigh length, collimation does not make sense."
        end
        xn = domain.x(domain.Nz)
        ynt = transpose(domain.y(domain.Nz))
        r² = similar(u) 
        @. r² = xn^2 + ynt^2
        @. u *= cispi(1 / (beam.λ * domain.Dz) * r²)
    end
    
    uf = copy(urx)
    η = freespace_coupling!(uf, domain, beam, target, f, dxf)
   
    # reference circle, e.g., fiber core interface
    shapes = [Config(type="circle", x0=-ref, x1=ref, y0=-ref, y1=ref, line=Config(width=1, dash="dash", color="white"))]

    # extract center and oversample:
    extract_and_oversample(z) = FourierTools.resample(center_extract(z, size(z) .÷ n_os), size(z))

    # convert all arrays to CPU for plotting
    urx = Adapt.adapt(Array, urx)
    uf = Adapt.adapt(Array, uf)

    # plot receive field
    plotrx && Plot(complex_plot(x=domain.x(domain.Nz), y=domain.y(domain.Nz), z=fftshift(urx)), Config(xaxis=Config(title="x"), yaxis=Config(title="y", scaleanchor="x", autorange=true), showscale=true)) |> display

    # focal plane coordinates: consider oversampling and cropping right away
    xf = fftfreq(domain.Nx, domain.Nx ÷ n_os * dxf)
    yf = fftfreq(domain.Ny, domain.Ny ÷ n_os * dyf)

    # plot focused field
    plotfocused && Plot(complex_plot(x=xf, y=yf, z=extract_and_oversample(fftshift(uf))), Config(xaxis=Config(title="x"), yaxis=Config(title="y", scaleanchor="x", autorange=true), showscale=true; shapes)) |> display

    # plot reference mode fields
    plotmodes && [Plot(complex_plot(x=xf, y=yf, z=extract_and_oversample(Adapt.adapt(Array, fftshift(v)))), Config(xaxis=Config(title="x"), yaxis=Config(title="y", scaleanchor="x", autorange=true), title=string(k...); shapes)) |> display for (k, v) ∈ pairs(target.modes)]


    ηtot = sum(values(η))
    @info "Total coupling loss: " -10log10(ηtot)

    η
end


function plot_from_dump(fn; iter=1, mode=1, collimate=false)
    z0, meta = h5open(fn, "r") do fid
        rx = fid["RXFields"]
        _colons = ntuple(_ -> Colon(), ndims(rx) - 1)

        getindex(rx, _colons..., iter), Dict(attrs(fid))
    end

    # third dimension can contain fields for different TX modes
    z = z0[:, :, mode]

    Dx = meta["Dx"]
    Dy = meta["Dy"]
    Nx = meta["Nx"]
    Ny = meta["Ny"]

    x, y = fftfreq(Nx, Dx[end]), fftfreq(Ny, Dy[end])

    if collimate
        λ = meta["lambda"]
        Dz = meta["Dz"]
        yt = transpose(y)
        r² = @. x^2 + yt^2
        @. z *= cis(π / (λ * Dz) * r²)
    end

    tr = complex_plot(; x, y, z=fftshift(z))
    lt = Config(; xaxis=Config(title="x"), yaxis=Config(title="y", scaleanchor="x", autorange=true))
    Plot(tr, lt)
end

function attributes_from_dump(fn)
    h5open(fn, "r") do fid
        OrderedDict(attrs(fid))
    end
end