using ProgressMeter
using Roots
using HDF5
using DelimitedFiles
using OrderedCollections
using DataFrames
using FastGaussQuadrature: approx_besselroots
import Dates: now, format

include(joinpath("..", "src", "estimate_statistics.jl"))


function analyze_coupling_data(fn; Lrange=(0, 20), probability=1e-2)

    data = h5open(fn, "r")

    ηraw = DataFrame(Dict(k => read(v) for (k, v) ∈ pairs(data["Efficiencies"])))

    modes = names(ηraw)
    # there are only two mode indices
    modeidx = [tuple((parse(Int, m.match) for m ∈ eachmatch(r"(\d+)", mode))...) for mode ∈ modes]

    # group coupling efficiencies by even/odd
    η = DataFrame(Dict(
        m => sum(eachcol(select(ηraw, Regex(m)))) for m ∈ string.(modeidx)
    ))

    # Add final "total" column
    transform!(η, names(η) => ByRow(+) => :Total)
    # compute corresponding coupling losses
    L = -10 .* log10.(η)

    fun = ecdf(L[!, :Total])
    ccdf = x -> 1 - fun(x)
    thresh = find_zero(x -> fun(x) - 1 + probability, Lrange)

    meta = Dict(attrs(data))

    close(data)

    ccdf, thresh, L, meta
end


function process_fiber_coupling_data(fn; dump=true)

    ηraw = h5open(fn, "r") do data
        DataFrame(Dict(k => read(v) for (k, v) ∈ pairs(data["Efficiencies"])))
    end

    modes = names(ηraw)
    # there are only two mode indices
    modeidx = [tuple((parse(Int, m.match) for m ∈ eachmatch(r"(\d+)", mode))...) for mode ∈ modes]

    # group coupling efficiencies by even/odd
    η = DataFrame(Dict(
        m => sum(eachcol(select(ηraw, Regex(m)))) for m ∈ string.(modeidx)
    ))

    # group by modegroups
    unique!(modeidx)
    
    ℓs = getindex.(modeidx, 1)
    ms = getindex.(modeidx, 2)
    besselroots = Dict(ℓ => approx_besselroots(ℓ-1, maximum(ms)) for ℓ ∈ ℓs)

    cutoff = OrderedDict((ℓ, m) => besselroots[ℓ][m] for (ℓ,m) ∈ modeidx)

    
    # group coupling efficiencies by modegroup (determined by cutoff), small positive offset for filtering
    ucutoffs = unique(x -> round(x, sigdigits=8), values(cutoff)) .+ 1e-8

    # build modegroups based on cutoff values, does not work for more than 6 modes!
    modegrps = [getindex(modeidx, findall(<(u), collect(values(cutoff)))) for u ∈ ucutoffs]
    # sort modegroups by number of modes (increasing)
    modegrps = modegrps[sortperm(ucutoffs)]
    # array of DataFrames, one for each modegroup
    ηgrp = [select(η, (Regex(string(m)) for m ∈ mgrp)...) for mgrp ∈ modegrps]

    for g ∈ ηgrp
        # Add final "total" column
        transform!(g, names(g) => ByRow(+) => :Total)
    end

    Lgrp = [-10 .* log10.(g) for g ∈ ηgrp]

    basefn, ext = splitext(fn)

    dump && h5open(basefn * "_proc.hdf5", "w") do fid
        for (k, g) ∈ enumerate(Lgrp)
            h5grp = create_group(fid, "Modegroup$k")
            [h5grp[string(k...)] = v for (k, v) ∈ zip(names(g), eachcol(g))]
        end
    end

    return nothing
end


function batch_analyze_coupling_data(dirs...; xvalue="Cn2", conditions=(;), dump::Bool=false, kwargs...)

    fns = filter(x -> last(splitext(x)) == ".hdf5", [fn for d ∈ dirs for fn ∈ readdir(d; join=true)])
    filter!(contains("coupling"), fns)

    Nf = length(fns)

    prg = Progress(Nf, desc="Evaluating datasets...", color=:yellow)

    # preallocate
    meta = Vector{Dict}(undef, Nf)
    thresh = zeros(Nf)

    @showprogress for (k, fn) ∈ enumerate(fns)
        _, thresh[k], _, meta[k] = analyze_coupling_data(fn; kwargs...)
        next!(prg)
    end

    # remove entries from files that do not satisfy the condition
    idx = [haskey(d, string(k)) ? d[string(k)] !== v : false for d ∈ meta for (k, v) ∈ pairs(conditions)]
    deleteat!(meta, idx)
    deleteat!(thresh, idx)
    deleteat!(fns, idx)
    # keep only the metadata that all files provide
    commonkeys = intersect(keys.(meta)...)
    meta = [OrderedDict(k => m[k] for k ∈ commonkeys) for m ∈ meta]
    # build dataframe from metadata, ...
    df = DataFrame(pairs(d) for d ∈ meta)
    # add parent directory, ...
    df.Directory = map(x -> splitdir(x)[1], fns)
    # and finally coupling thresholds
    df.threshold = thresh

    traces = [Config(type="scatter", x=gdf[!, xvalue], y=gdf[!, :threshold], mode="markers", name=last(splitpath(values(dir) |> first))) for (dir, gdf) ∈ pairs(groupby(df, :Directory))]
    Plot(traces, Config(xaxis=Config(title=xvalue), yaxis=Config(title="Coupling Loss Threshold [dB]"))) |> display

    if dump
        fn = timeprefix() * "coupling_analysis_$xvalue"
        # hdf5 export
        h5open(fn * ".hdf5", "w") do fid
            [fid[string(k)] = v for (k, v) ∈ pairs(eachcol(df))]
        end
        # csv export
        CSV.write(fn * ".csv", df)
    end

    return df
end