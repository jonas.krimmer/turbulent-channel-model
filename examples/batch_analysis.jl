include("analyze_turbulent_channel_model.jl")
include("analyze_coupling_data.jl")

function batch_repeated_wave_propagation(args...; param::Symbol=:λ, xmin=1.530e-6, xmax=1.565e-6, N=51, kwargs...)

    prg = Progress(N, "Total progress", showspeed=true)

    # integer-aware logic
    dx = (xmax - xmin) / (N - 1)
    if isapprox(round(Int, dx), dx, rtol=1e-14)
        dx = Int(dx)
    end

    x = range(xmin, step=dx, length=N)

    for cx ∈ x
        kv = (; Dict(param => cx)...)
        repeated_wave_propagation(args...; kwargs..., kv..., showprogress=false, meta=kv)
        next!(prg)
    end
end

function batch_coupling(; basedir=pwd())
    fns = readdir(basedir, join=true)
    filter!(contains(r"\d+\.hdf5"), fns)

    prg = Progress(length(fns), "Total progress", showspeed=true)
    for fn ∈ fns
        obtain_coupling_statistics(fn, optim_focus=true, dump=true, showprogress=false)
        next!(prg)
    end
end

function mean_couplingloss_vs_xval(xval="lambda"; basedir=pwd())
    fns = readdir(basedir, join=true)
    filter!(contains(r"coupling.hdf5"), fns)

    # for now, consider total coupling loss only
    df = DataFrame()

    for fn ∈ fns
        _, _, cL, meta = analyze_coupling_data(fn; Lrange=(0, 30))
        df[!, string(meta[xval])] = cL[!, :Total]
    end

    λ = parse.(Float64, names(df))
    Lm = vec(mean(Matrix(df), dims=1))
    
    plot.scatter(x=λ, y=Lm)
end