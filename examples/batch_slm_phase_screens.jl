using ProgressMeter
using Dates
using FileIO


include(joinpath("..", "src", "ft_phase_screen.jl"))

log10range(x1, x2, n) = (exp10(y) for y in range(log10(x1), log10(x2), length=n))

function generate_n_slm_screens(N=100; r0=1e-3, seed=0, model=kolmogorov)

    # assume Holoeye GAEA2
    Nx = 2464
    Ny = 4160
    dx = 3.74e-6
    dy = 3.74e-6
    # scale sizes for non-kolmogorov spectra
    l0 = 1e-3
    L0 = 1e1

    fun = "log"

    # real and imaginary parts of complex valued realizations contain independent phase screens
    N_2 = cld(N, 2) |> Int

    targetdir = "GAEA2_screens-r0_$(round(r0, sigdigits=2))"
    mkpath(targetdir)
    fnbase = Dates.format(now(), "YYYY-mm-dd") * "-GAEA2_screen-r0_" * string(round(r0, sigdigits=2)) * "-"

    
    p = Progress(N_2, showspeed=true, color=:yellow)
    
    Threads.@threads for k ∈ Base.OneTo(N_2)
        
        # reallocate for thread-safety
        screenarray = zeros(ComplexF64, Nx, Ny)
        screens = [zeros(UInt8, Nx, Ny) for k ∈ 1:2]

        Random.seed!(seed + k - 1)
        nuft_phase_screen!(screenarray; r0, dx, dy, model, tol=1e-5)

        # GAEA2 -> is UInt8 the correct format?
        for (ℓ, screen) ∈ enumerate(reim(screenarray))
            @. screens[ℓ] = round(UInt8, mod2pi(screen + π) * 255 / 2π)
            targetfile = fnbase * lpad(2(k - 1) + ℓ, floor(Int, log10(N)) + 1, "0") * ".png"
            save(joinpath(targetdir, targetfile), screens[ℓ])
        end

        next!(p)
    end
end

function generate_blank_slm_screen()
    # assume Holoeye GAEA2
    Nx = 2464
    Ny = 4160
    dx = 3.74e-6
    dy = 3.74e-6

    screen = fill(UInt8(128), Nx, Ny)
    save(Dates.format(now(), "YYYY-mm-dd") * "-GAEA2_screen-blank.png", screen)
    
    nothing
end

function generate_for_different_r0(lower=1e-4, upper=1e-1, N=4, args...; spacing::Function=log10range, kwargs...)

    r0 = spacing(lower, upper, N)

    p = Progress(N, desc="Overall progress: "; showspeed=false, offset=1)
    for r0 ∈ r0
        generate_n_slm_screens(args...; r0, kwargs...)
        next!(p)
    end
end