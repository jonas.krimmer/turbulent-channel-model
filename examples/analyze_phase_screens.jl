using ProgressMeter
using FFTW
using Random
using DataFrames
using HDF5
using Colors
using FileIO
using OrderedCollections
using Dates: now, format
using Unicode


include(joinpath("..", "src", "estimate_statistics.jl"))
include(joinpath("..", "src", "ft_phase_screen.jl"))
include(joinpath("..", "src", "plot_basics.jl"))

function plot_screen(; Nx=256, Ny=Nx, dx=1e-1 / Nx, dy=dx, Nsh=0, uniform=true, tol=1e-5, seed=1, wrap=true, dump=false, Cn²=1e-14, kwargs...)
    # generate phase screen
    l0 = 5e-3
    L0 = 1e1
    k0 = 2π / 1.55e-6
    z = 1e3
    r0 = (0.423 * k0^2 * z * Cn²)^(-3 / 5)
    @info "d/r0:" dx * Nx / r0

    Random.seed!(seed)

    _screen = zeros(ComplexF64, Nx, Ny)

    if uniform
        ft_phase_screen!(_screen; r0, dx, dy, l0, L0, Nsh, centered=true, kwargs...)
    else
        nuft_phase_screen!(_screen; r0, dx, dy, l0, L0, tol, centered=true, kwargs...)
    end

    screen = real(_screen)

    wrap && (@. screen = (mod2pi(screen + π) - π))

    x = fftshift(fftfreq(Nx, Nx * dx))

    if dump
        img = @. round(UInt8, mod2pi(screen + π) * 255 / 2π)
        save("$(format(now(), "YYYY-mm-dd-HH-MM-SS"))_ps_Dr0_$(round(dx * Nx / r0, sigdigits=2)).png", img)
    else
        Plot(Config(type="heatmap", x=x, y=x, z=screen, zmid=0, colorscale="Greys"), Config(xaxis=Config(title="x"), yaxis=Config(title="y", scaleanchor="x"))) |> display
    end

    nothing
end


function monte_carlo_sf(::AbstractArray{T}, iters, screen_sz, approaches, args, sf_estimator, sf_len; seed=0) where {T}

    # preallocate structure functions
    D = Dict(k => zeros(T, sf_len, iters) for (k, v) ∈ approaches)
    
    prg = Progress(iters, desc="Iterate...")

    # single-threaded FFTW -> threading over iterations more efficient
    n_fftw_threads = FFTW.get_num_threads()
    FFTW.set_num_threads(1)

    Threads.@threads for i ∈ Base.OneTo(iters)
        # preallocating requires thread-local storage, not recommended, see also https://julialang.org/blog/2023/07/PSA-dont-use-threadid/
        screen = zeros(Complex{T}, screen_sz...)

        for (k, f!) ∈ approaches
            # fix random seed --> comparison
            maybe_cuda_seed!(i + seed - 1)
            # compute phase screens
            f!(screen; args[k]...)
            # estimate structure functions
            D[k][:, i] .= sf_estimator(screen)
        end

        next!(prg)
    end

    # reset FFTW threads
    FFTW.set_num_threads(n_fftw_threads)

    return D
end


function monte_carlo_sf(::CuArray{T}, iters, screen_sz, approaches, args, sf_estimator, sf_len; seed=0) where {T}

    # preallocate structure functions
    D = Dict(k => CUDA.zeros(T, sf_len, iters) for (k, v) ∈ approaches)
    
    prg = Progress(iters, desc="Iterate...")

    screen = CUDA.zeros(Complex{T}, screen_sz...)

    for i ∈ Base.OneTo(iters)
        for (k, f!) ∈ approaches
            # fix random seed --> comparison
            maybe_cuda_seed!(i + seed - 1)
            # compute phase screens
            f!(screen; args[k]...)
            # estimate structure functions
            D[k][:, i] .= sf_estimator(screen)
        end

        next!(prg)
    end

    return D
end



function phase_screen_sf(iters=100; Nx=128, Nκ=Nx, Nϕ=Nx, Nsh=7, q=3, Nss=500, showplot=true, kth_sh::Int=max(Nsh, 1), dump=false, d=1e-1, polar=false, chunk=1, dtype=Float64, model=andrews, seed=0)

    # we use two screens (real & imaginary part) in each iteration
    iters >>= 1
    # we generate chunks of phase screens
    iters ÷= chunk

    # atmospheric / simulation parameters
    l0 = 1e-3
    L0 = 1e1
    k0 = 2π / 1.55e-6
    z = 1e3
    Cn² = 1e-14
    r0 = (0.423 * k0^2 * z * Cn²)^(-3 / 5)
    dx = d / Nx # grid spacing [m]

    # tolerance for nufft
    tol = 1e-5

    # which subharmonics to generate
    subharmonics = reverse(range(Nsh, 0, step=-kth_sh))

    # define screens to generate
    approaches = (
        ("FT + $k SH" => ft_phase_screen! for k in subharmonics)...,
        # ("PWD + $k SH" => ft_phase_screen! for k in subharmonics)...,
        # "Log. distr." => nuft_phase_screen!,
        # "Arsinh distr." => nuft_phase_screen!,
        # "Sqrt distr." => nuft_phase_screen!,
        # "Log. polar" => nuft_phase_screen_pol!,
        "Arsinh polar" => nuft_phase_screen_pol!,
        # "Sparse spectrum" => ssft_phase_screen!,
        # "Sparse uniform" => suft_phase_screen!,
    )

    # distinct function arguments
    common = (; r0, dx, l0, L0, centered=true, model)
    args = Dict(
        ("FT + $k SH" => (; common..., Nsh=k, q) for k in subharmonics)...,
        ("PWD + $k SH" => (; common..., Nsh=k, q, pwd=true) for k in subharmonics)...,
        "Log. distr." => (; common..., Nκx=Nκ, tol, fun="log", nthreads=1),
        "Arsinh distr." => (; common..., Nκx=Nκ, tol, fun="asinh", nthreads=1),
        "Sqrt distr." => (; common..., Nκx=Nκ, tol, fun="sqrt", nthreads=1),
        "Log. polar" => (; common..., Nκ, Nϕ, tol, fun="log", nthreads=1),
        "Arsinh polar" => (; common..., Nκ, Nϕ, tol, fun="asinh", nthreads=1),
        "Sparse spectrum" => (; common..., Nκ=Nss, tol, nthreads=1),
        "Sparse uniform" => (; common..., Nκ=Nss, tol, nthreads=1),
    )

    # compute structure function numerically (avoids difficulties in evaluating the Kummer function 1F1), make use of spectral decay
    r = (0:Nx-1) * dx
    κmin = 0
    Φ = phase_spectrum(r0, l0, L0, model)
    @info "Computing target structure function numerically"
    D_th = num_sf(Φ, r, κmin)

    @info "Generate phase screens and estimate structure functions"
    # Structure function of complex valued function = structure function of real + imaginary part
    sf_estimator = if polar
        x -> sf_ft_iso2d(x; tol=1e-6) ./ 2
    else
        x -> (sf_ft(x; dims=1) .+ sf_ft(PermutedDimsArray(x, (2, 1, 3)); dims=1)) ./ 4
    end

    # monte carlo loop 
    D = monte_carlo_sf(maybe_cuda(zeros(dtype, 0)), iters, (Nx, Nx, chunk), approaches, args, sf_estimator, Nx; seed)

    # mean structure function and mean rel. error
    D_mean = Dict(k => vec(mean(Adapt.adapt(Array, D[k]), dims=2)) for (k, v) ∈ approaches)
    # D_err = Dict(k => vec(relrms(Adapt.adapt(Array, D[k]), D_th, dims=2)) for (k, v) ∈ approaches)
    # maybe legacy: compute RMS w.r.t. sample-averaged structure fucntion
    D_err = Dict(k => vec(relrms(D_mean[k], D_th; dims=2)) for (k, v) ∈ approaches)


    sfdata = OrderedDict(("Theory" => D_th, (k => D_mean[k] for (k, v) ∈ approaches)...))
    errdata = OrderedDict((k => D_err[k] for (k, v) ∈ approaches))

    # Theoretical sf plus FT method and all the others
    sf_traces = [Config(type="scatter", x=r, y=v, name=k) for (k, v) ∈ sfdata]
    # Plot rel. errors
    err_traces = [Config(type="scatter", x=r, y=v, name=k) for (k, v) ∈ errdata]

    # Print RMS relative error, compare Paulson 2020
    [print("RMS rel. error of " * string(k) * ": " * string(mean(abs, skipmissing(v))) * "\n") for (k, v) ∈ errdata]

    # Plot structure functions
    p = Plot(sf_traces, Config(xaxis=Config(title="Observation point distance r [m]"), yaxis=Config(title="Dᵩ(r)", rangemode="nonnegative"), title="Iters: $(2iters). Chunksize: $chunk. Seed: $seed"))
    # Plot relative errors
    perr = Plot(err_traces, Config(xaxis=Config(title="Observation point distance r [m]"), yaxis=Config(title="Relative error δᵣ(r)", type="log"), colorway=circshift(default_colorway, -1), showlegend=true, title="Iters: $(2iters). Chunksize: $chunk. Seed: $seed"))


    if showplot
        display(p)
        display(perr)
    end

    if dump
        sf_df = coalesce.(DataFrame(sfdata), NaN)
        err_df = coalesce.(DataFrame(errdata), NaN)

        # hdf5 output
        fn = "$(format(now(), "YYYY-mm-dd-HH-MM-SS"))_sf_iters_$(iters)_N_$(Nx)_d_$(d)"
        
        h5open(fn * ".hdf5", "w") do fid
            create_group(fid, "SF")
            [fid["SF"][string(k)] = v for (k, v) ∈ pairs(eachcol(sf_df))]
            create_group(fid, "RelErr")
            [fid["RelErr"][string(k)] = v for (k, v) ∈ pairs(eachcol(err_df))]

            for (var, val) ∈ pairs((; xaxis=collect(r), model=string(model), d, z, r0, Cn², l0, L0, Nx, Nκ, Nϕ, Nss, Nsh, q, tol))
                attrs(fid)[Unicode.normalize(string(var), compat=true)] = val
            end
        end
    end

    nothing
end