include(joinpath("..", "src", "sampling_requirements.jl"))
include(joinpath("..", "src", "turbulent_channel.jl"))
include(joinpath("..", "src", "monte_carlo.jl"))
include(joinpath("..", "src", "source.jl"))

include("plot_results.jl")

using HDF5
using H5Zblosc
using FourierTools
import FourierTools: center_extract, center_pos
import Dates: format, now, today


function repeated_wave_propagation(utx::AbstractArray{Complex{T}}, midx, domain::Domain{T}, iters::Int=100; chunksize=10000, Cn²=1e-14, l0=1e-3, L0=1e1, λ=1.55e-6, w0=1e-2, uniform::Bool=false, model::Function=andrews, dump=true, fnprefix=timeprefix(), dir=pwd(), meta=(;), showprogress=true, samplingcheck=true) where {T}

    # turbulent channel definition
    channel = TurbulentChannel{T}(Cn², l0, L0, model)
    # compute effective Fried parameter
    r0eff = fried_pw(2π / λ, domain.Dz, channel.Cn²)
    # estimate Fried parameters for partial propagations
    r0 = partial_r0_pw(2π / λ, domain.z, Cn²)

    # compute Rytov variance per step
    σr² = rytov_pw(2π / λ, domain.dz, channel.Cn²)
    (σr² >= 0.1) && @warn "Insufficient number of planes, not within weak fluctuation regime."

    # estimated beam waist radius at receiver
    zr = 2π / λ * w0^2 / 2
    wz = w0 * sqrt(1 + domain.Dz^2 / zr^2)
    # effective transmit aperture
    D1 = 2w0
    # effective receive aperture, super-Gaussian window function reduces effective domain size to 2*0.47*Dxy (1/e, see split-step-method.jl)
    # D2 = min(2domain.rA, 0.94domain.Dx, 0.94domain.Dy, 2wz)
    D2 = 4wz
    D_r0 = 4wz / r0eff

    # check, if sampling requirements for the angular spectrum method are fulfilled, compare book by Schmidt
    samplingcheck && check_sampling(domain, D1, D2; λ, r0=r0eff)

    basefn = fnprefix * "waveprop"
    # append meta-values to the filename
    for (k, v) ∈ pairs(meta)
        if isinteger(v)
            basefn *= "_$(k)_$v"
        else
            basefn *= "_$(k)_$(round(v,sigdigits=3))"
        end
    end
    nchunks = cld(iters, chunksize)

    # transmit power, consides change in grid resolution
    Ptx = sum(abs2, utx, dims=1:2) .* (domain.dx[1] * domain.dy[1])
    # pre-plan fft, has some slight advantage for CUDA
    fftplan = plan_fft!(utx[:, :, 1])

    for (j, chunk) ∈ enumerate(Iterators.partition(1:iters, chunksize))
        urx = maybe_cuda(zeros(Complex{T}, size(utx)..., length(chunk)))
        # monte carlo loop
        mc_iterations!(urx, utx, r0, domain, channel, chunksize, j, nchunks; showprogress, λ, uniform, fftplan)
        # receive power (within aperture)
        Prx = sum(abs2, urx, dims=1:2) .* (domain.dx[end] * domain.dy[end])
        # filename suffix
        suffix = "_$(lpad(j, 1 + round(Int, floor(log10(nchunks))), "0"))"

        dump && h5open(joinpath(dir, basefn * suffix * ".hdf5"), "w") do fid
            showprogress && (@info "Saving chunk $j/$nchunks to disk...")

            fid["RXFields", chunk=(size(utx)..., 1)] = Adapt.adapt(Array, urx)
            fid["TXFields"] = Adapt.adapt(Array, utx)
            fid["PathLoss"] = Adapt.adapt(Array, Prx ./ Ptx)

            # Metadata
            attrs(fid)["modes"] = midx
            attrs(fid)["iters"] = iters
            attrs(fid)["nchunks"] = nchunks
            attrs(fid)["chunksize"] = chunksize
            attrs(fid)["dx"] = collect(domain.dx)
            attrs(fid)["dy"] = collect(domain.dy)
            attrs(fid)["dz"] = domain.dz
            attrs(fid)["Dx"] = collect(domain.Dx)
            attrs(fid)["Dy"] = collect(domain.Dy)
            attrs(fid)["Dz"] = domain.Dz
            attrs(fid)["D_r0"] = D_r0
            attrs(fid)["r0eff"] = r0eff
            attrs(fid)["r0"] = r0
            attrs(fid)["Cn2"] = channel.Cn²
            attrs(fid)["l0"] = channel.l0
            attrs(fid)["L0"] = channel.L0
            attrs(fid)["model"] = string(channel.model)
            attrs(fid)["Nx"] = domain.Nx
            attrs(fid)["Ny"] = domain.Ny
            attrs(fid)["Nz"] = domain.Nz
            attrs(fid)["w0"] = w0
            attrs(fid)["lambda"] = λ
            # trying to overwrite existing attributes throws an error!
            for (k, v) ∈ pairs(meta)
                if !haskey(attrs(fid), string(k))
                    attrs(fid)[string(k)] = v
                else
                    v_h5 = attrs(fid)[string(k)]
                    if v_h5 != v
                        @warn "The output HDF5 file already features the attribute $k = $v_h5. $k = $v from the NamedTuple `meta` is ignored."
                    end
                end
            end
        end
        # TODO: identify memory issues that lead to some GPU-memory not being freed
    end

    nothing
end

function repeated_wave_propagation(iters, T::DataType=Float64; Nx::Int=128, λ=1.55e-6, w0=1e-2, dx1=1e-3, dxn=1e-3, dz=5e1, Nz::Int=21, m=0, n=0, kwargs...)
    beam = Beam{T}(m, n, λ, w0)

    domain = if dx1 == dxn
        Domain{T}(dx1, dz, Nx, Nz)
    else
        Domain{T}(dx1, dxn, dz, Nx, Nz)
    end

    midx = [(m, n)]
    utx = gaussian_source(domain, w0, λ, midx...)

    repeated_wave_propagation(utx, midx, domain, iters; beam.λ, beam.w0, kwargs...)
end

function repeated_wave_propagation_multi(iters, T::DataType=Float64; Nx::Int=128, λ=1.55e-6, w0=1e-2, dx1=1e-3, dxn=1e-3, dz=5e1, Nz::Int=21, ng=3, kwargs...)
    # simulation domain
    domain = if dx1 == dxn
        Domain{T}(dx1, dz, Nx, Nz)
    else
        Domain{T}(dx1, dxn, dz, Nx, Nz)
    end

    # we consider the modes from the first ng mode groups
    midx = sort!([(m, n) for m ∈ range(0, ng - 1) for n ∈ range(0, ng - m - 1)], by=sum)

    # set up transmit modes
    utx = gaussian_source(domain, w0, λ, midx)

    repeated_wave_propagation(utx, midx, domain, iters; λ, w0, kwargs...)
end