include(joinpath("..", "src", "backpropagate.jl"))
include(joinpath("..", "src", "estimate_statistics.jl"))
include(joinpath("..", "src", "sampling_requirements.jl"))
include(joinpath("..", "src", "turbulent_channel.jl"))
include(joinpath("..", "src", "monte_carlo.jl"))
include(joinpath("..", "src", "optical_parameters.jl"))

include("plot_results.jl")

using HDF5
using H5Zblosc
using DataFrames
using FourierTools
using Optim
import FourierTools: center_extract, center_pos
import Dates: format, now, today

# From Zheng 2016 Opt. Expr.: "The first [fiber] is a standard G.652 SMF with fundamental mode diameter of 9 μm. The second one is a 2MF with core diameter of 18 μm and fundamental mode diameter of 15.6 μm. The last one is a 4MF whose core diameter is 24 μm and fundamental mode diameter is 18.2 μm.
# see specs of Corning® SMF-28® Ultra Optical Fiber: https://www.corning.com/media/worldwide/coc/documents/Fiber/product-information-sheets/PI-1424-AEN.pdf
default_fibers = (
    # SMF=(4.1e-6, 0.1, 5.2e-6),
    # FMF3=(9e-6, 0.1, 7.8e-6),
    FMF6=(12e-6, 0.1, 9.1e-6),
)
default_proteus = (; (Symbol("CP$k") => (300e-6, k) for k ∈ 1:6)...)


"""
    fn: base filename of HDF5 file(s) to load data from

"""
function coupling_statistics(templatefn; f=80e-3, wg=FiberModes(default_fibers.FMF6...), basedir=pwd(), showplot=false, dump=true, optim_focus=true, pdf=false, plotmodes=false, samplingcheck=false, showprogress=true)

    # obtain relevant files
    allfiles = readdir(basedir, join=true)
    basefn = templatefn[1:findlast('_', templatefn)]
    hdf5files = filter!(contains(basefn * r"\d+\.hdf5"), allfiles)
    # just in case, sort?
    sort!(hdf5files)

    # load metadata
    metadata = h5open(first(hdf5files), "r") do h5
        Dict(attrs(h5))
    end

    # verify that we have all the chunks at hand
    (metadata["nchunks"] != length(hdf5files)) && @warn "Found $(length(hdf5files)) files, expected $(metadata["nchunks"])"

    dx = metadata["dx"]
    dy = metadata["dy"]
    dz = metadata["dz"]
    # build a domain struct corresponding to receiver plane
    T = eltype(dx)
    domain = Domain{T}(dx[end], dy[end], dz, metadata["Nx"], metadata["Ny"], metadata["Nz"])
    # dummy beam
    beam = Beam(0, 0, metadata["lambda"], metadata["w0"])

    optim_focus && (f = optimize_focus(domain, beam, wg))

    dxf = focal_plane_res(domain, beam, wg, f)[1]
    # sampling requirements satisfied for backpropagation?
    samplingcheck && check_sampling(f, domain.Nx, domain.dx, domain.Dx, dxf, 2wg.a; λ=1.55e-6, R=-f, r0=Inf, c=2, debug=false)
    
    modes = setup_modes(wg, domain, beam, f)
    for (k, mode) ∈ modes
        backprop!(mode, dxf, domain.dx, f, beam.λ)
    end

    # optionally, plot backpropagated fiber modes
    if plotmodes
        x = fftshift(fftfreq(domain.Nx, domain.Dx))
        y = fftshift(fftfreq(domain.Ny, domain.Dy))

        for (k, mode) ∈ modes
            z = fftshift(Adapt.adapt(Array, mode))
            tr = complex_plot(; x, y, z)
            lt = Config(; title=string(k...), xaxis=Config(title="x"), yaxis=Config(title="y", scaleanchor="x", autorange=true))
            Plot(tr, lt) |> display
        end
    end

    # generate coupling efficiency DataFrame
    η = DataFrame(Dict(string(k) => zeros(metadata["iters"]) for k ∈ keys(modes)))

    # fill coupling efficiency DataFrame
    for (j, chunk) ∈ enumerate(Iterators.partition(1:metadata["iters"], metadata["chunksize"]))

        urx = maybe_cuda(h5read(hdf5files[j], "RXFields"))

        prg = Progress(length(chunk), desc="Chunk $j/$(metadata["nchunks"]): "; enabled=showprogress)

        for (ci, i) ∈ enumerate(chunk)
            for (k, mode) ∈ modes
                @views η[!, string(k)][i] = coupling_efficiency(urx[:, :, ci], mode)
            end

            next!(prg)
        end
    end

    stats = pdf ? estimate_pdf : estimate_ccdf

    if showplot
        traces = Config[]

        for k ∈ keys(modes)
            x, y = stats(-10log10.(η[!, string(k)]))
            push!(traces, Config(type="scatter", x=x, y=y, name=string(k...), showlegend=true))
        end
        Llim = 10(fld(max((maximum(t.x) for t ∈ traces)...), 10) + 1)

        p = Plot(traces, Config(xaxis=Config(title="Coupling loss budget L [dB]", range=[0, Llim]), yaxis=Config(title="Outage probability", type="log", nticks=round(Int, log10(metadata["iters"])) + 2), title="Focal length f=$(round(f*1e3, sigdigits=3)) mm"))

        pdf && (p.layout.yaxis.type = "linear")

        display(p)
    end


    if dump
        # dump to .hdf5 file for postprocessing
        fn = basefn * "coupling.hdf5"

        h5open(fn, "w") do fid

            ηgrp = create_group(fid, "Efficiencies")
            [ηgrp["M"*string(k...)] = v for (k, v) ∈ zip(names(η), eachcol(η))]

            # save mode fields, too -> does not hurt
            mgrp = create_group(fid, "Modefields")
            [mgrp["M"*string(k...)] = Adapt.adapt(Array, v) for (k, v) ∈ pairs(modes)]

            attrs(fid)["focallength"] = f
            [attrs(fid)[k] = v for (k, v) ∈ pairs(metadata)]
        end
    end

    return nothing
end

function scintillation_index(fn; basedir=pwd(), debug=false, showplot=false)

    fn = joinpath(basedir, fn)

    # load data & metadata
    urx, metadata = h5open(fn, "r") do h5
        maybe_cuda(read(h5["RXFields"])), Dict(attrs(h5))
    end

    # coordinate axes
    Nx = metadata["Nx"]
    Ny = metadata["Ny"]
    x = fftfreq(Nx, last(metadata["Dx"]))
    y = fftfreq(Ny, last(metadata["Dy"]))
    # other parameters
    λ = metadata["lambda"]
    Dz = metadata["Dz"]
    Cn² = metadata["Cn2"]

    # scintillation index along highest dimension, note that the dumped fields are not fftshifted!
    SI = squeeze(scv(urx; dims=ndims(urx)))
    SI0 = CUDA.@allowscalar first(SI) # without the fftshift, the first element is the on-axis one

    if debug
        @info "Estimated on-axis scintillation index: " SI0

        # for weak fluctuations, the scintillation index leads to the Rytov variance
        σ²ᵣ_pw = rytov_pw(2π / λ, Dz, Cn²)
        @info "Rytov variance (plane wave): " σ²ᵣ_pw
        σ²ᵣ_sw = rytov_sw(2π / λ, Dz, Cn²)
        @info "Rytov variance (spherical wave): " σ²ᵣ_sw

        @info "Theoretical scintillation index (PW, Kolmogorov): " exp(0.49σ²ᵣ_pw / (1 + 1.11σ²ᵣ_pw^(6 // 5))^(7 // 6) + 0.51σ²ᵣ_pw / (1 + 0.69σ²ᵣ_pw^(6 // 5))^(5 // 6)) - 1
    end
    
    if showplot
        # Plot SI distribution
        z = Adapt.adapt(Array, SI[:, :, 1])
        p = plot.heatmap(; x=fftshift(x), y=fftshift(y), z=fftshift(z), zmin=0, zmax=min(2mean(SI), 6))

        p.layout.showscale = true
        p.layout.xaxis.title = "x / r0"
        p.layout.yaxis.title = "y / r0"
        p.layout.yaxis.scaleanchor = "x"

        display(p)
    end

    return SI0
end


function coherence_length(fn; basedir=pwd(), debug=false, collimate=true)

    fn = joinpath(basedir, fn)

    # load data & metadata
    urx, metadata = h5open(fn, "r") do h5
        maybe_cuda(read(h5["RXFields"])), Dict(attrs(h5))
    end

    # coordinate axes
    Nx = metadata["Nx"]
    Ny = metadata["Ny"]
    x = fftfreq(Nx, last(metadata["Dx"]))
    y = fftfreq(Ny, last(metadata["Dy"]))
    # other parameters
    λ = metadata["lambda"]
    Dz = metadata["Dz"]
    r0 = metadata["r0eff"]

    # optionally, collimate before estimating params
    if collimate
        yt = transpose(y)
        r² = similar(urx, Nx, Ny)
        @. r² = x^2 + yt^2
        @. urx *= cispi(1 / (λ * Dz) * r²)
    end
    # fftshift axes
    x = fftshift(x)
    y = fftshift(y)

    # refer to approach by Schmidt
    mask = similar(urx, Nx, Ny)
    fill!(mask, 1)
    aperture!(mask, 1, Nx ÷ 4, centered=true)
    cf = real(cf_ft(fftshift(urx) .* mask; dims=1:2))
    CUDA.@allowscalar cf ./= first(cf)
    mcf = Adapt.adapt(Array, cf)[:, 1]

    # evaluate structure function on grid by integrating the appropriate PSD in "model"
    r = (0:Nx-1) * metadata["dx"][end]
    D_pw = num_sf(getfield(Main, Symbol(metadata["model"]))(1; κ0=2π / metadata["L0"], κl=3.3 / metadata["l0"], κm=5.92 / metadata["l0"]), r, 0.0, 1e-4)

    # analytical expression for plane wave MCF
    mcf_pw(x) = @. exp(-1 // 2 * 0.49 * x^(-5 // 3) * D_pw)
    # now, fit the estimated MCF to the analytical expression to estimate r0
    J(r0) = norm(mcf_pw(r0) .- mcf) # or coupling_efficiency?
    r0_est = Optim.minimizer(optimize(J, metadata["dx"][end], 10r0))

    if debug
        # theoretical mutual correlation function
        mcf_th = mcf_pw(r0)
        mcf_fit = mcf_pw(r0_est)

        @info "Estimated Fried parameter: " r0_est
        @info "Actual Fried parameter: " r0
        # MCF plots
        p = plot.scatter(; x=r ./ r0, y=mcf, name="Estimated MCF")
        p.scatter(; x=r ./ r0, y=mcf_fit, name="Fit")
        p.scatter(; x=r ./ r0, y=mcf_th, name="Theoretical MCF")

        p.layout.showscale = true
        p.layout.xaxis.title = "x / r0"

        display(p)
    end

    return r0_est
end