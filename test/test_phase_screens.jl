include(joinpath("..", "src", "ft_phase_screen.jl"))


function _test_all_methods!(screen)
    # basic ft screen
    @test_nowarn ft_phase_screen!(screen)
    # ft screen with subharmonics
    @test_nowarn ft_phase_screen!(screen, Nsh=1)
    # ft screen with PWD method
    @test_nowarn ft_phase_screen!(screen, pwd=true)
    # ft screen with PWD method + subharmonics
    @test_nowarn ft_phase_screen!(screen, pwd=true, Nsh=1)
    # ft screen with nonuniform sampling & NUFFT
    @test_nowarn nuft_phase_screen!(screen)
    # ft screen with polar nonuniform sampling & NUFFT
    @test_nowarn nuft_phase_screen_pol!(screen)
    # sparse-spectrum screen (Charnotskii)
    @test_nowarn ssft_phase_screen!(screen)
    # sparse-uniform screen (Charnotskii)
    @test_nowarn suft_phase_screen!(screen)
end

function test_screen_gen(T::DataType, N=256)
    @testset "Phase screen generation $T (CPU)" begin
        screen = zeros(Complex{T}, N, N)
        _test_all_methods!(screen)
    end
    
    if CUDA.functional()
        @testset "Phase screen generation $T (GPU)" begin
            screen_gpu = CUDA.zeros(Complex{T}, N, N)
            _test_all_methods!(screen_gpu)
        end
    else
        @warn "CUDA not available. Cannot test GPU-based generation."
    end
end

test_screen_gen(Float32)
test_screen_gen(Float64)