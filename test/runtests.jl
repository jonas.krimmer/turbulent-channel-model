using Test

# basic test: does phase screen generation work?
@testset "turbulent_channel" begin
    include("test_phase_screens.jl")
end