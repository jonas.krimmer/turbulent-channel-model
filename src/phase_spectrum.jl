function kolmogorov(c=1; kwargs...)
    κ -> c * κ^(-11 // 3)
end

function tatarskii(c=1; κm=5.92/1e-2, kwargs...)
    κ -> c * κ^(-11 // 3) * exp(-(κ / κm)^2)
end

function karman(c=1; κ0=2π/10, kwargs...)
    κ -> c * (κ^2 + κ0^2)^(-11 // 6)
end

function modifiedkarman(c=1; κ0=2π/10, κm=5.92/1e-2, kwargs...)
    κ -> c * (κ^2 + κ0^2)^(-11 // 6) * exp(-(κ / κm)^2)
end

function andrews(c=1; κ0=2π/10, κl=3.3/1e-2, kwargs...)
    κ -> c * (1 + 1802 // 1000 * κ / κl - 254 // 1000 * (κ / κl)^(7 // 6)) * (κ^2 + κ0^2)^(-11 // 6) * exp(-(κ / κl)^2)
end



function phase_spectrum(r0::T, l0::T, L0::T, model::Function=andrews) where {T}

    κ0 = 2(π / L0)
    κl = (33 // 10) / l0
    κm = (592 // 100) / l0
    c = (49 // 100) * r0^(-5 // 3)

    model(c; κ0, κl, κm)
end