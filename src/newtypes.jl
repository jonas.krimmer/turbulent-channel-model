include("generate_lg.jl")

using OrderedCollections
using FFTW

struct Domain{T,U}
    # dimensions: transverse & longitudinal
    Dx::U
    Dy::U
    Dz::T
    # step sizes: transverse & longitudinal
    dx::U
    dy::U
    dz::T
    # samples: transverse & longitudinal
    Nx::Int
    Ny::Int
    Nz::Int
    # grid vectors: transverse & longitudinal
    x::Function
    y::Function
    z::AbstractVector{T}
end

# main constructors -> infer dx and dy
function Domain{T}(dx::Real, dy::Real, dz::Real, Nx, Ny, Nz) where {T<:Real}

    z = (0:Nz-1) * dz

    Dx = T(Nx * dx)
    Dy = T(Ny * dy)
    Dz = z[end]

    x(_) = fftfreq(Nx, Dx)
    y(_) = fftfreq(Ny, Dy)

    Domain{T,T}(Dx, Dy, Dz, dx, dy, dz, Nx, Ny, Nz, x, y, z)
end
function Domain{T}(dx::U, dy::U, dz::Real, Nx, Ny, Nz) where {T<:Real,U<:AbstractVector{T}}

    z = (0:Nz-1) * dz

    Dx = Nx .* dx
    Dy = Ny .* dy
    Dz = z[end]

    x(m) = fftfreq(Nx, Dx[m])
    y(m) = fftfreq(Ny, Dy[m])

    Domain{T,U}(Dx, Dy, Dz, dx, dy, dz, Nx, Ny, Nz, x, y, z)
end

# convenience: dx = dy
Domain{T}(dx, dz, Nx, Ny, Nz) where {T<:Real} = Domain{T}(dx, dx, dz, Nx, Ny, Nz)
# convenience: Nx = Ny
Domain{T}(dx, dz, Nx, Nz) where {T<:Real} = Domain{T}(dx, dx, dz, Nx, Nx, Nz)
# convenience: pass dx1 and dx2 to build dx-range with dx = dy and Nx = Ny
Domain{T}(dx1::Real, dx2::Real, dz::Real, Nx, Nz) where {T<:Real} = begin
    dx = range(T(dx1), T(dx2), Nz)
    Domain{T}(dx, dx, dz, Nx, Nx, Nz)
end
# default: use Float64
Domain(args...) = Domain{Float64}(args...)

# provide argumentless version for simplified testing
Domain{T}() where T = Domain{T}(1e-3, 1e2, 256, 11)

struct Beam{T}
    l::Int
    m::Int

    λ::T
    k0::T
    w0::T
    wlm::T
    M²::T

    fun::Function

    function Beam{T}(l=0, m=0, λ=1.55e-6, w0=1e-2) where {T<:Real}
        M² = abs(l) + 2m + 1
        new{T}(l, m, λ, 2π / λ, w0, w0 * sqrt(M²), M², lg_fun_cart(l, m, T(w0), T(λ)))
    end
end
Beam(args...) = Beam{Float64}(args...)


struct TurbulentChannel{T}
    Cn²::T # structure constant of refractive index
    l0::T # inner scale of turbulence
    L0::T # outer scale of turbulence
    model::Function # power spectrum of phase fluctuations, e.g., Kolmogorov, Karman, ...

    TurbulentChannel{T}(Cn²=1e-14, l0=1e-2, L0=1e2, model=andrews) where {T} = begin
        if model === kolmogorov
            l0 = 0
            L0 = Inf
        elseif model === karman
            l0 = 0
        elseif model === tatarskii
            L0 = Inf
        end
        new{T}(Cn², l0, L0, model)
    end
end
TurbulentChannel(args...) = TurbulentChannel{Float64}(args...)


# Far from optimal
struct FiberModes{T}
    a::T # core radius
    NA::T # numerical aperture
    MFR::T # mode field radius (fundamental mode)

    modes::OrderedDict{Tuple{Int,Int,Symbol},AbstractMatrix{Complex{T}}}

    # parameter which is not used for FiberModes
    FiberModes{T}(a, NA, MFR=NaN; kwargs...) where {T<:Real} = new{T}(a, NA, MFR, OrderedDict{Tuple{Int,Int,Symbol},AbstractMatrix{Complex{T}}}())
end
FiberModes(a, NA, MFR=NaN; kwargs...) = FiberModes{Float64}(a, NA, MFR; kwargs...)


struct FreeSpaceModes{T}
    w::T # beam waist of fundamental beam mode
    nmodes::Int # number of beam modes
    which::Symbol

    modes::OrderedDict{NTuple{2,Int},AbstractMatrix{Complex{T}}}

    FreeSpaceModes{T}(w, n; which=:HG) where {T} = new{T}(w, n, which, OrderedDict{NTuple{2,Int},AbstractMatrix{Complex{T}}}())
end
FreeSpaceModes(w, n; which=:HG) = FreeSpaceModes{Float64}(w, n; which)

const Modes = Union{FiberModes,FreeSpaceModes}
generate_wgs(f=FiberModes; which=:HG, args...) = NamedTuple(k => f(v...; which) for (k, v) ∈ pairs(args))