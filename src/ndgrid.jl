using Base.Cartesian

"""
ndgrid(x...)

Calculate a Cartesian grid in an n-dimensional space from input vectors x_1, ..., x_n.

"""

function ndgrid(x::Vararg{Any,N}) where {N}
    dims = map(length, x)
    X = ntuple(k -> similar(x[k], dims), Val(N))
    # _ndgrid!(X, x)
    _ndgrid_broadcast!(X, x)

    return X
end


macro refk(k, A::Symbol, ex)
    vars = Base.Cartesian.inlineanonymous(ex, k)
    Expr(:escape, Expr(:ref, A, vars))
end

@generated function _ndgrid!(dest::NTuple{N,Any}, src::NTuple{N,Any}) where {N}
    quote
        @nexprs $N k -> begin
            x = dest[k]
            y = src[k]
            @nloops $N i x begin
                @inbounds (@nref $N x i) = (@refk k y i)
            end
        end
    end
end

function _ndgrid_broadcast!(dest::NTuple{N,Any}, src::NTuple{N,Any}) where {N}
    for k ∈ 1:N
        inds = ntuple(i -> (i == k ? length(src[k]) : 1), Val(N))
        dest[k] .= reshape(src[k], inds)
    end
end