using FFTW
using CUDA

"""
asm_simple!(u, tf)

Propagate the input field distribution u (shifted s.t. it is compatible with the FFT/IFFT) according to the angular spectrum method. After the fft, apply the propagator tf in-place and perform the ifft.
"""
function asm_simple!(u, tf)
    fft!(u)
    @inbounds @. u *= tf
    ifft!(u)
end
# use provided fftplan which has to operate in-place!
function asm_simple!(u, tf, fftplan)
    fftplan * u
    @inbounds @. u *= tf
    fftplan \ u
end

"""
onestep_prop_ir!(u,dx1,k0,z)

Propagate the input field distribution u (shifted s.t. it is compatible with the FFT/IFFT). This function evaluates the Fresnel diffraction integral in a single step. Source and observation plane grid differ!
"""
function onestep_prop_ir!(u, dx1, k0, z)

    N = size(u, 1)
    # source plane grid
    xy1 = fftfreq(N, N * dx1)
    # observation plane grid
    dx2 = 2π * z / (k0 * N * dx1)
    xy2 = fftfreq(N, N * dx2)

    # apply quadratic phase in source plane
    @inbounds for m ∈ Base.OneTo(N)
        @simd for n ∈ Base.OneTo(N)
            r1² = xy1[n]^2 + xy1[m]^2
            u[n, m] *= cis(k0 * r1² / 2z)
        end
    end

    # FFT
    fft!(u)

    # apply quadratic phase in observation plane
    a = -1im * k0 / (2π * z)
    @inbounds for m in Base.OneTo(N)
        @simd for n in Base.OneTo(N)
            r2² = xy2[n]^2 + xy2[m]^2
            u[n, m] *= a * cis(k0 * r2² / 2z)
        end
    end

    return u, dx2
end


"""
twostep_prop_ir!(u,dx1,dx2,k0,z)

Propagate the input field distribution u (shifted s.t. it is compatible with the FFT/IFFT). This function makes use of the impulse-response-based two-step method and hence allows for different spatial resolutions in the source (dx1) and observation plane (dx2). Make sure that the sampling criterion is fulfilled (oversampling --> IR oversampled, TF undersampled). For further information on the function see chapter 6 in the book by Schmidt (2010). For further information on the sampling see chapter 5 in the the book by Voelz (2011).
"""
function twostep_prop_ir!(u, dx1, dx2, k0, Δz)

    N = size(u, 1)
    # source plane grid
    xy1 = fftfreq(N, N * dx1)
    # magnification
    mag = dx2 / dx1

    # intermediate plane grid
    Δz1 = Δz / (1 - mag)
    Δz2 = Δz - Δz1
    dxi = 2π * abs(Δz1) / (k0 * N * dx1)
    xyi = fftfreq(N, N * dxi)

    # observation plane grid
    xy2 = fftfreq(N, N * dx2)

    # apply quadratic phase in source plane
    @inbounds for m ∈ Base.OneTo(N)
        @simd for n ∈ Base.OneTo(N)
            r1² = xy1[n]^2 + xy1[m]^2
            u[n, m] *= cis(k0 / 2Δz1 * r1²)
        end
    end

    # FFT
    fft!(u)
    u .*= dx1^2

    # apply quadratic phase in intermediate plane
    a = -1im * k0 / (2π * Δz1)
    @inbounds for m in Base.OneTo(N)
        @simd for n in Base.OneTo(N)
            ri² = xyi[n]^2 + xyi[m]^2
            u[n, m] *= a * cis(k0 * Δz / (2Δz1 * Δz2) * ri²)
        end
    end

    # FFT
    fft!(u)
    u .*= dxi^2

    # apply quadratic phase in observation plane
    a = -1im * k0 / (2π * Δz2)
    @inbounds for m in Base.OneTo(N)
        @simd for n in Base.OneTo(N)
            r2² = xy2[n]^2 + xy2[m]^2
            u[n, m] *= a * cis(k0 / 2Δz2 * r2²)
        end
    end

    u
end


"""
twostep_prop_ang!(u,dx1,dx2,k0,z)

Propagate the input field distribution u (shifted s.t. it is compatible with the FFT/IFFT). This function makes use of the transfer-function-based (angular spectrum) two-step method and hence allows for different spatial resolutions in the source (dx1) and observation plane (dx2). Make sure that the sampling criterion is fulfilled (undersampling --> IR undersampled, TF oversampled). For further information on the function see chapter 6 in the book by Schmidt (2010). For further information on the sampling see chapter 5 in the the book by Voelz (2011).
"""
function twostep_prop_ang!(u, dx1, dx2, k0, Δz, κlim=Inf)

    N = size(u, 1)

    # source plane grid
    xy1 = fftfreq(N, N * dx1)

    # magnification
    mag = dx2 / dx1
    dκ = 2π / (N * dx1)
    κxy = fftfreq(N, N * dκ)

    # observation plane grid
    xy2 = fftfreq(N, N * dx2)

    # apply quadratic phase in source plane
    @inbounds for m ∈ Base.OneTo(N)
        @simd for n ∈ Base.OneTo(N)
            r1² = xy1[n]^2 + xy1[m]^2
            u[n, m] *= cis(k0 / 2Δz * (1 - mag) * r1²) / mag
        end
    end

    # FFT
    fft!(u)

    # apply quadratic phase in intermediate plane and apply circular brickwall filter
    κc² = 2(N * dκ / 2 * κlim)^2
    @inbounds for m in Base.OneTo(N)
        @simd for n in Base.OneTo(N)
            κ² = κxy[n]^2 + κxy[m]^2
            u[n, m] *= (κ² > κc²) ? zero(eltype(u)) : cis(-Δz / (2mag * k0) * κ²)
        end
    end

    # IFFT
    ifft!(u)

    # apply quadratic phase in observation plane
    @inbounds for m in Base.OneTo(N)
        @simd for n in Base.OneTo(N)
            r2² = xy2[n]^2 + xy2[m]^2
            u[n, m] *= cis(k0 / 2Δz * (1 - 1 / mag) * r2²)
        end
    end

    u
end

function twostep_prop_ang!(u::CuArray{Complex{T}}, dx1, dx2, k0, Δz, κlim=Inf) where {T}

    N = size(u, 1)
    r1² = similar(u, T, N, N)
    κ² = similar(u, T, N, N)
    r2² = similar(u, T, N, N)

    # source plane grid
    xy1 = fftfreq(N, N * dx1)
    xy1t = transpose(xy1)
    @. r1² = xy1^2 + xy1t^2

    # magnification
    mag = dx2 / dx1
    dκ = 2π / (N * dx1)
    κxy = fftfreq(N, N * dκ)
    κxyt = transpose(κxy)
    @. κ² = κxy^2 + κxyt^2

    # observation plane grid
    xy2 = fftfreq(N, N * dx2)
    xy2t = transpose(xy2)
    @. r2² = xy2^2 + xy2t^2

    # apply quadratic phase in source plane
    @. u *= cis(k0 / 2Δz * (1 - mag) * r1²) / mag

    # FFT
    fft!(u)

    # apply quadratic phase in intermediate plane and apply circular brickwall filter
    κc² = 2(N * dκ / 2 * κlim)^2
    @. u[κ² > κc², :] = 0
    @. u *= cis(-Δz / (2mag * k0) * κ²)

    # IFFT
    ifft!(u)

    # apply quadratic phase in observation plane
    @. u *= cis(k0 / 2Δz * (1 - 1 / mag) * r2²)
    
    u
end
