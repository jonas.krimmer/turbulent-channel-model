import Base: size, length, first, last, getindex

logrange_signed(start::Real, stop::Real, length::Integer) = generalized_range(x -> sign(x) * log(abs(x) + 1), x -> sign(x) * (exp(abs(x)) - 1), start, stop, length)
asinhrange(start::Real, stop::Real, length::Integer) = generalized_range(asinh, sinh, start, stop, length)
sqrtrange_signed(start::Real, stop::Real, length::Integer) = generalized_range(x -> sign(x) * sqrt(abs(x)), x -> sign(x) * x^2, start, stop, length)



generalized_range(fun::Function, ifun::Function, start::Real, stop::Real, length::Integer) = GeneralizedRange(fun, ifun, start, stop, Int(length))
generalized_range(fun::Function, ifun::Function, start::Real, stop::Real; length::Integer) = generalized_range(fun, ifun, start, stop, length)

"""
    GeneralizedRange{T}(fun, ifun, start, stop, len) <: AbstractVector{T}

A range whose elements are spaced according to the function `fun` between `start` and `stop`,
    with spacing controlled by `len`. Returned by [`generalized_range`](@ref).
    
Like [`LinRange`](@ref), the first and last elements will be exactly those
provided, but intermediate values may have small floating-point errors.
These are calculated using the logs of the endpoints, which are
stored on construction, often in higher precision than `T`.
"""
struct GeneralizedRange{T<:Real,X,F1,F2} <: AbstractArray{T,1}
    fun::F1
    ifun::F2
    start::T
    stop::T
    len::Int
    extra::Tuple{X,X}
    function GeneralizedRange{T}(fun::Function, ifun::Function, start::T, stop::T, len::Int) where {T<:Real}
        if T <: Integer
            throw(ArgumentError("GeneralizedRange{T} does not support integer types"))
        end
        if !isfinite(start) || !isfinite(stop)
            throw(DomainError((start, stop),
                "GeneralizedRange is only defined for finite start & stop"))
        elseif len < 0
            throw(ArgumentError(LazyString(
                "GeneralizedRange(", start, ", ", stop, ", ", len, "): can't have negative length")))
        elseif len == 1 && start != stop
            throw(ArgumentError(LazyString(
                "GeneralizedRange(", start, ", ", stop, ", ", len, "): endpoints differ, while length is 1")))
        end
        ex = _generalized_range_extra(fun, start, stop, len)
        new{T,typeof(ex[1]),typeof(fun),typeof(ifun)}(fun, ifun, start, stop, len, ex)
    end
end

function GeneralizedRange{T}(fun::Function, ifun::Function, start::Real, stop::Real, len::Integer) where {T}
    GeneralizedRange{T}(fun, ifun, convert(T, start), convert(T, stop), convert(Int, len))
end
function GeneralizedRange(fun::Function, ifun::Function, start::Real, stop::Real, len::Integer)
    T = float(promote_type(typeof(start), typeof(stop)))
    GeneralizedRange{T}(fun, ifun, convert(T, start), convert(T, stop), convert(Int, len))
end

Base.size(r::GeneralizedRange) = (r.len,)
Base.length(r::GeneralizedRange) = r.len

Base.first(r::GeneralizedRange) = r.start
Base.last(r::GeneralizedRange) = r.stop

Base.copy(r::GeneralizedRange) = r

function _generalized_range_extra(fun::Function, a::Real, b::Real, len::Int)
    fa = fun(1.0a)  # widen to at least Float64
    fb = fun(1.0b)
    (fa / (len - 1), fb / (len - 1))
end

function Base.getindex(r::GeneralizedRange{T,X}, i::Int) where {T,X}
    @inline
    @boundscheck checkbounds(r, i)
    i == 1 && return r.start
    i == r.len && return r.stop

    fx = (r.len - i) * r.extra[1] + (i - 1) * r.extra[2]
    x = r.ifun(fx)
    return T(x)
end



function centralize(x::AbstractVector{T}) where {T<:Number}
    collect(T, ((x[k] + x[k-1]) / 2 for k ∈ Iterators.drop(eachindex(x), 1)))
end