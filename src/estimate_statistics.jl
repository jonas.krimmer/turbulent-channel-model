using FINUFFT
using FFTW
using Bessels
using StatsBase
using Statistics
using AverageShiftedHistograms
using QuadGK

# numerical structure / covariance function for psds of isotropic random fields
num_sf(S::Function, r, κmin=zero(eltype(r)), rtol=1e-8) = quadgk(κ -> 4π * (1 .- besselj0.(κ * r)) * S(κ) * κ, κmin, Inf, rtol=rtol) |> first
num_cov(S::Function, r, κmin=zero(eltype(r)), rtol=1e-8) = quadgk(κ -> 2π * besselj0.(κ * r) * S(κ) * κ, κmin, Inf, rtol=rtol) |> first



sf(x::AbstractArray{T}; dim=1, sflen=size(x, dim)) where {T} = sf!(zeros(real(T), sflen), x; dim)

function sf!(D::AbstractVector, x::AbstractArray{T,N}; dim=1) where {T,N}

    ax(k) = CartesianIndices(ntuple(d -> (d == dim) ? Base.OneTo(size(x, d) - k + 1) : axes(x, d), Val(N)))
    offset(k) = CartesianIndex(ntuple(d -> (d == dim) ? k - 1 : 0, Val(N)))

    @inbounds for k ∈ eachindex(D)
        @simd for I ∈ ax(k)
            D[k] += abs2(x[I] - x[I+offset(k)]) # magnitude squared for complex values! See Yaglom 2004 p. 88
        end
        D[k] /= length(ax(k))
    end

    D
end

function _indicator_array(x0::AbstractArray{T}, ::Colon) where {T}
    fftsize = 2 .* size(x0)
    w = similar(x0, fftsize...)
    fill!(w, zero(T))
    I = CartesianIndices(size(x0))
    w[I] .= one(T)
    w
end
function _indicator_array(x0::AbstractArray{T,N}, dims) where {T,N}
    fftsize = ntuple(k -> k ∈ dims ? 2size(x0, k) : 1, Val(N))
    w = similar(x0, fftsize...)
    fill!(w, zero(T))
    I = CartesianIndices(ntuple(k -> k ∈ dims ? size(x0, k) : 1, Val(N)))
    w[I] .= one(T)
    w
end

_dim2region(nd, ::Colon) = 1:nd
_dim2region(nd, dims) = dims

# avoid aliasing by zero-padding; avoid -1 for more efficient FFT
function _padded_array(x0::AbstractArray{T}, ::Colon) where {T}
    paddedsize = 2 .* size(x0)
    x = similar(x0, paddedsize...)
    fill!(x, zero(T))
    x[axes(x0)...] .= x0
    x
end
function _padded_array(x0::AbstractArray{T,N}, dims) where {T,N}
    paddedsize = ntuple(k -> k ∈ dims ? 2size(x0, k) : size(x0, k), Val(N))
    x = similar(x0, paddedsize...)
    fill!(x, zero(T))
    x[axes(x0)...] .= x0
    x
end

function sf_ft(x0::AbstractArray{T,N}; dims=:, averaging=true) where {T,N}
    # IDEA: rewrite with pre-allocated output that determines the dimensions
    # estimate structure function along dimensions specified by dims, compare Marcotte 1996

    # window function/indicator matrix
    w = _indicator_array(x0, dims)
    # zeropad
    x = _padded_array(x0, dims)

    region = _dim2region(N, dims)
    Px = plan_fft(x, region)
    Pw = plan_fft(w, region)

    X = Px * x
    @. x = abs2(x)
    X2 = Px * x
    W2 = Pw * w # w^2 = w

    # compute variogram
    V = @. 2 * (real(X2 * conj(W2)) - abs2(X))
    # compute number of pairs at all lags
    P = @. abs2(W2)

    D0 = real(rfft(V, region) ./ rfft(P, region))

    idx = CartesianIndices(x0)
    D = D0[idx]

    if averaging
        mean_exclude_dims(D, dims) |> squeeze
    else
        D
    end
end

function sf_ft_iso2d(x0::AbstractArray{T,N}; tol=1e-5, nthreads=1, averaging=true) where {T,N}

    # (nu)fft region
    region = 1:2
    # window function/indicator matrix, same size along free-running dimensions
    w = _indicator_array(x0, region)

    # polar grid for nufft: we need radii up to 2π to take the zero-padding into account
    r = Adapt.adapt(containertype(x0), range(0, real(T)(2π), length=size(w, 2)) |> collect)
    ϕ = Adapt.adapt(containertype(x0), range(0, real(T)(2π), length=size(w, 1)) |> collect)
    grid = ndgrid(ϕ, r)
    pol2cart!(grid...)

    # zeropad
    x = _padded_array(x0, region)

    Px = plan_fft(x, region)
    Pw = plan_fft(w, region)

    X = Px * x
    @. x = abs2(x)
    X2 = Px * x
    W2 = Pw * w # w^2 = w

    # compute variogram
    @. X = 2 * (real(X2 * conj(W2)) - abs2(X))
    # compute number of pairs at all lags
    @. W2 = abs2(W2)

    Nx = length(w)
    Nt = length(x) ÷ Nx
    D0_n = similar(x0, complex(T), Nx, Nt)
    D0_d = similar(x0, complex(T), Nx)

    thread_arg = containertype(x0) <: CuArray ? (;) : (; nthreads)
    nufft2d2!(grid..., D0_n, 1, tol, X; modeord=1, thread_arg...)
    nufft2d2!(grid..., D0_d, 1, tol, W2; modeord=1, thread_arg...)

    D0_n ./= D0_d
    D0 = reshape(real(D0_n), size(x)...)

    idx = CartesianIndices(x0)
    D = D0[idx]

    if averaging
        mean_exclude_dims(D, 2) |> squeeze
    else
        D
    end
end

function cf_ft(x0::AbstractArray{T,N}; dims=:, averaging=true) where {T,N}

    # window function/indicator matrix
    w = _indicator_array(x0, dims)
    # zeropad
    x = _padded_array(x0, dims)

    region = _dim2region(N, dims)
    X = fft(x, region)
    W = fft(w, region)
    X2 = abs2.(X)
    W2 = abs2.(W)

    B0 = real(rfft(X2, region) ./ rfft(W2, region))

    idx = CartesianIndices(x0)
    B = B0[idx]

    if averaging
        mean_exclude_dims(B, dims) |> squeeze
    else
        B
    end
end

function cf_ft_iso2d(x0::AbstractArray{T,N}; tol=1e-5, nthreads=1, averaging=true) where {T,N}
    # (nu)fft region
    region = 1:2
    # window function/indicator matrix, same size along free-running dimensions
    w = _indicator_array(x0, region)

    # polar grid for nufft: we need radii up to 2π to take the zero-padding into account
    r = Adapt.adapt(containertype(x0), range(0, real(T)(2π), length=size(w, 2)) |> collect)
    ϕ = Adapt.adapt(containertype(x0), range(0, real(T)(2π), length=size(w, 1)) |> collect)
    grid = ndgrid(ϕ, r)
    pol2cart!(grid...)

    # zeropad
    x = _padded_array(x0, region)

    X = fft(x, region)
    W = fft(w, region)

    # correlate
    @. X = abs2(X)
    @. W = abs2(W)

    Nx = length(w)
    Nt = length(x) ÷ Nx
    B0_n = similar(x0, complex(T), Nx, Nt)
    B0_d = similar(x0, complex(T), Nx)

    thread_arg = containertype(x0) <: CuArray ? (;) : (; nthreads)
    nufft2d2!(grid..., B0_n, 1, tol, X; modeord=1, thread_arg...)
    nufft2d2!(grid..., B0_d, 1, tol, W; modeord=1, thread_arg...)

    B0_n ./= B0_d
    B0 = reshape(real(B0_n), size(x)...)

    idx = CartesianIndices(x0)
    B = B0[idx]

    if averaging
        mean_exclude_dims(B, 2) |> squeeze
    else
        B
    end
end


"""
    scv(u)

    Estimate scintillation index, i.e., the normalized variance, of an array of complex amplitude matrices u. Compare bottom of Andrews 2019 p. 48 (68).
    In statistics, this quantity is called the **squared coefficient of variation (SCV)**.
"""
function scv(u::AbstractArray{T,N}; dims=:) where {T,N}
    I = abs2.(u)
    var(I; dims, corrected=false) ./ mean(I; dims) .^ 2
end


function estimate_ccdf(v)
    # local ccdf
    cdffun = ecdf(v)

    l, u = extrema(v)

    # rice rule
    k = ceil(Int, 2cbrt(length(v)))
    x = range(l, u, length=k)

    ccdf = @. 1 - cdffun(x)

    x, ccdf
end

function estimate_pdf(v)

    l, u = extrema(v)
    # rice rule
    n = length(v)
    k = ceil(Int, 2cbrt(n))
    x = range(l, u, length=k)

    pdf = ash(v, rng=x, kernel=Kernels.epanechnikov).density

    x, pdf
end