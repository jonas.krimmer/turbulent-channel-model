using LinearAlgebra
using FFTW
using PlotlyLight
using PlotlyKaleido
using Cobweb
using Colors
using ColorSchemes
using StatsBase

try
    PlotlyKaleido.start()  # start Kaleido server
catch err
    @warn err
end
preset.template.plotly_white!() # initialize plotly template
# use custom colorscale from Okabe Ito -> color deficiency compatible and configure default styling
# default_colorway = hex.(ColorSchemes.okabe_ito)
default_colorway = ["#636EFA", "#EF553B", "#00CC96", "#AB63FA", "#FFA15A", "#19D3F3", "#FF6692", "#B6E880", "#FF97FF", "#FECB52"]

PlotlyLight.settings.layout = merge(
    Config(
        xaxis=Config(ticks="inside", showline=true, zeroline=false), #, automargin=true, constrain="domain", linewidth=1),
        yaxis=Config(ticks="inside", showline=true, zeroline=false), #, automargin=true, constrain="domain", linewidth=1),
        legend=Config(orientation="h", xanchor="center", x=0.5, y=1.2),
        colorway=default_colorway
    ),
    PlotlyLight.settings.layout
)

"""

    complex_plot(; demean, normalize, z, kwargs)

    Heatmap-like plot of an image representing magnitude and phase of a complex-valued 2D array.

"""
function complex_plot(; demean=true, normalize=false, z=[], x::Ax=range(1, size(z, 1)), y::Ay=range(1, size(z, 2)), fixhover=true, kwargs...) where {Ax<:Union{AbstractRange,Frequencies},Ay<:Union{AbstractRange,Frequencies}}

    # constant lightness colormap from cmocean -> perceptually uniform!
    cmap = ColorSchemes.phase

    if !isempty(z)
        mag = @. abs(z)
        arg = @. angle(z) + π

        # normalize magnitude to 0 - 1 range
        normalize && (mag .-= minimum(mag))
        normalize!(mag, Inf)

        if demean
            arg .-= angle(mean(z))
            # wrap phase
            @. arg = mod2pi(arg)
        end

        arg ./= 2π
    end

    plotcolors = similar(z, Vector{Float64})

    for k ∈ eachindex(z)
        _rgb_in = cmap[arg[k]]
        _hsv_in = convert(HSV, _rgb_in)
        _hsv_out = HSV(_hsv_in.h, _hsv_in.s, mag[k])
        _rgb_out = convert(RGB, _hsv_out)
        plotcolors[k] = 255 .* [_rgb_out.r; _rgb_out.g; _rgb_out.b]
    end

    hoverargs = if fixhover
        (; customdata=[[m, a] for (m, a) ∈ zip(mag, arg)], hovertemplate=raw"(x,y): (%{x},%{y})<br> Mag.: %{customdata[0]} <br> Arg.: %{customdata[1]} <br>")
    else
        (;)
    end

    # automatically convert x and y to x0, dx and y0, dy
    x0, y0 = minimum(x), minimum(y)
    dx, dy = step(x), step(y)

    Config(; z=plotcolors, x0, dx, y0, dy, colormodel="rgb", type="image", hoverargs..., kwargs...)
end