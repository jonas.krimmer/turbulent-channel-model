"""
hg(x, y, z, l, m, w0, λ)

Compute the HG_{l,m}-mode at positions given by x, y, and z, where l is the horizontal (x) and m the vertical (y) mode index (both integers or zero). w0 is the beam radius of the corresponding fundamental Gaussian beam and λ the vacuum wavelength.
"""
hg(x::AbstractArray{T1,3}, y::AbstractArray{T2,3}, z::AbstractArray{T3,3}, args...) where {T1,T2,T3<:Number} = broadcast(hg_fun(args...), x, y, z)

hg(x::AbstractMatrix{T1}, y::AbstractMatrix{T2}, z::T3, args...) where {T1,T2,T3<:Number} = broadcast(hg_fun(args...), x, y, z)

hg(x::Number, y::Number, z::Number, args...) = broadcast(hg_fun(args...), x, y, z)

# now, the dimension-extending version
function hg(x, y, z, args...)
    fun = hg_fun(args...)
    [fun(x...) for x ∈ Iterators.product(x, y, z)]
end

"""
hg_fun(l, m, w0, λ)

Generate anonymous function for HG_{l,m}-mode, where l is the horizontal (x) and m the vertical (y) mode index (both integers, m,n >= 0). w0 is the beam radius of the corresponding fundamental Gaussian beam and λ the vacuum wavelength.
"""
function hg_fun(l=0, m=0, w0=1e-2, λ=1.55e-6)

    # l and m have to be ≥ 0
    @assert l ≥ 0
    @assert m ≥ 0

    k0 = 2π / λ
    #  rayleight length
    zr = k0 * w0^2 / 2

    # 1/e beam radius
    w(z) = w0 * sqrt(1 + (z / zr)^2)
    #  radius of curvature of phase front
    R(z) = z + zr^2 / z
    #  Gouy phase
    φ(z) = (1 + l + m) * atan(z, zr)


    # HG modes with propagator
    (x, y, z) -> w0 / w(z) * hermite(l, sqrt(2) * x / w(z)) * hermite(m, sqrt(2) * y / w(z)) * exp(-(x^2 + y^2) / w(z)^2) * cis(-k0  / 2R(z) * (x^2 + y^2) + φ(z) - k0 * z)
end

"""
hermite(n, x)

Compute the value of the Hermite polynomial H_n(x). For further information see NIST Handbook, Olver 2010, p.443 and https://dlmf.nist.gov/18.9
"""
function hermite(n::Int, x::T) where {T}

    p0, p1 = one(T), 2x
    n == 0 && return p0
    for k ∈ 1:n-1
        p1, p0 = 2 * (x * p1 - k * p0), p1
    end
    p1
end


function plot_hg(; m=0, n=0, λ=1.55e-6, w0=1e-2, z=0, Dx=1e-1, Nx=256)

    # define grid
    x = fftfreq(Nx, Dx) |> fftshift
    y = x

    # generate and evaluate function
    u = hg(x, y, z, m, n, w0, λ)

    tr = complex_plot(; z=u, x0=first(x), dx=Dx / Nx, y0=first(y), dy=Dx / Nx)
    lt = Config(; xaxis=Config(title="x"), yaxis=Config(title="y", scaleanchor="x", autorange=true))
    Plot(tr, lt)
end