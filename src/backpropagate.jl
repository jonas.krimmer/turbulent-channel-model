include("plot_basics.jl")
include("propagators.jl")

function backprop!(u, dxf, dx, f, λ=1.55e-6; debug=false)
    twostep_prop_ang!(u, dxf, dx, 2π / λ, -f)
    lens!(u, dx, 2π / λ, -f)

    debug && Plot(complex_plot(z=Adapt.adapt(Array, u), dx=dx, dy=dx)) |> display
end
    
# using Integrals
# include("generate_lp.jl")
# include("helpers.jl")
# include("ndgrid.jl")

# function test_gen_huygens_fresnel_backprop(; N=256, D=1e-3, f=80e-3, λ=1.55e-6)

#     # free-space dimensions
#     dx = D / N
#     # fiber dimensions
#     a = 4.5e-6
#     Df = 6a
#     dxf = Df / N

#     # rf, ϕf = polargrid(N, dxf, N, dxf)
#     # u = lp(rf, ϕf; l=0, m=1, a=a, V=1.7)
#     # Plot(complex_plot(z=u, x0=-Df/2, dx=dxf, y0=-Df/2, dy=dxf))

#     # uout = complex(ifftshift(u))
#     # twostep_prop_ang!(uout, dxf, dx, 2π / λ, -f)
#     # lens!(uout, dx, 2π / λ, -f, Inf)
#     # uout = fftshift(uout)

#     # generalized Huygens-Fresnel, requires "Integrals" package
#     LP = lp_fun(0, 1, a, 1.7)
#     ufun(x, y) = LP(hypot(x, y), atan(y, x))

#     xy = fftshift(fftfreq(N, D))

#     gen_fresnel_integral(x1, x2) =
#         let A = 1, B = -f, D = 0
#             -1im / (B * λ) * ufun(x1[1], x1[2]) * cispi(1 / (λ * B) * (D * (x2[1]^2 + x2[2]^2) - 2 * x1[1] * x2[1] - 2 * x1[2] * x2[2] + A * (x1[1]^2 + x1[2]^2)))
#         end

#     integrand!(y, u, p) = begin 
#         for (I, x2) ∈ enumerate(Iterators.product(xy, xy))
#             y[I] = gen_fresnel_integral(u, x2)
#         end
#     end

#     prob = IntegralProblem(IntegralFunction(integrand!, zeros(ComplexF64, N, N)), ([-Df, -Df], [Df, Df]))

#     @time uout = solve(prob, HCubatureJL(), reltol=1e-3).u

#     xyf = fftshift(fftfreq(N, Df))
#     u = [ufun(x[1], x[2]) for x ∈ Iterators.product(xyf, xyf)]

#     Plot(complex_plot(z=uout, x0=-D / 2, dx=dx, y0=-D / 2, dy=dx))
# end