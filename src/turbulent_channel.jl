using FINUFFT # SEGFAULT may occur if FINUFFT is not loaded sufficiently early TODO: check if this issue persists
using StatsBase
using ProgressMeter
using FFTW
using Random
using OrderedCollections

include("newtypes.jl")
include("source.jl")
include("ft_phase_screen.jl")
include("generate_lg.jl")
include("freespace_coupling.jl")
include("split_step_method.jl")
include("plot_basics.jl")
include("optical_parameters.jl")


"""
    screen_to_refraction_op(screen [, r0])

Convert (an array) of complex-valued phase screens to a refraction operator.

The input `screen` is an array of shape `(height, width, depth)` where each element is a complex number. Both the real and the imaginary path represent the phase shift at the corresponding coordinate. The output `R` is an array of shape `(height, width, 2*depth)` representing the refraction operator according to the input `screen`. Specifying an additional input vector `r0` enables modifying the turbulence strength of the phase screens.
"""
function screen_to_refraction_op(screen::AbstractArray{T,3}) where {T<:Complex}
    R = similar(screen, size(screen, 1), size(screen, 2), 2size(screen, 3))

    @views @inbounds for m ∈ axes(screen, 3)
        @. R[:, :, 2m-1] = cis(real(screen[:, :, m]))
        @. R[:, :, 2m] = cis(imag(screen[:, :, m]))
    end

    R
end
function screen_to_refraction_op(screen::AbstractArray{T,3}, r0::AbstractVector) where {T<:Complex}
    R = similar(screen, size(screen, 1), size(screen, 2), 2size(screen, 3))

    @views @inbounds for m ∈ axes(screen, 3)
        @. R[:, :, 2m-1] = real(screen[:, :, m])
        @. R[:, :, 2m] = imag(screen[:, :, m])
    end

    # now, scale by r0^(-5/6) -> the phase spectrum is proportional to r0^(-5/3) and scales the variance -> we need another square-root
    @views for m ∈ axes(r0, 1)
        @. R[:, :, m] *= r0[m]^(-5 // 6)
    end

    # finally apply complex exponential
    @. R = cis(R)
end

function turbulent_channel!(u::AbstractMatrix{Complex{T}}, r0::AbstractVector, domain::Domain{T}=Domain{T}(), channel::TurbulentChannel{T}=TurbulentChannel{T}(); λ=1.55e-6, uniform::Bool=false, fftplan=plan_fft!(u)) where {T<:Union{Float32,Float64}}

    # choose number of subharmonics Nsh such that 3^Nsh * domain.d ≥ L0
    Nsh = round(Int, cld(log(channel.L0 / min(domain.Dx, domain.Dy)), log(3))) # we assume L0 > d
    # we need Nz-1 screens since Nz takes the source plane into account
    Ncplx = cld(domain.Nz, 2)

    # preallocate phase screen array
    screen = similar(u, size(u)..., Ncplx)

    # Uniform sampling with subharmonics / nonuniform sampling
    uniform ? ft_phase_screen!(screen; r0=one(T), dx=domain.dx, dy=domain.dy, channel.l0, channel.L0, centered=false, Nsh, channel.model) :
    nuft_phase_screen!(screen; r0=one(T), dx=domain.dx, dy=domain.dy, channel.l0, channel.L0, centered=false, nthreads=1, tol=1e-5, channel.model)

    R = screen_to_refraction_op(screen, r0)
    split_step_sym!(u, λ, domain.dx, domain.dy, domain.z, R; fftplan)
end

function turbulent_channel!(u::AbstractMatrix{Complex{T}}, r0::AbstractVector, domain::Domain{T,<:AbstractVector}=Domain{T}(1e-3, 2e-3, 1e2, 256, 11), channel::TurbulentChannel{T}=TurbulentChannel{T}(); λ=1.55e-6, uniform::Bool=false, fftplan=plan_fft!(u)) where {T<:Union{Float32,Float64}}

    # preallocate refraction operator array
    R = similar(u, domain.Nx, domain.Ny, domain.Nz)
    # complex-valued array to store phase screen generated with nu-ft (output always complex-valued)
    screen = similar(u, domain.Nx, domain.Ny)

    for m ∈ 1:domain.Nz
        # choose number of subharmonics Nsh such that 3^Nsh * domain.d ≥ L0
        Nsh = round(Int, cld(log(channel.L0 / min(domain.Dx[m], domain.Dy[m])), log(3))) # we assume L0 > d

        # Uniform sampling with subharmonics / nonuniform sampling
        uniform ? ft_phase_screen!(screen; r0=r0[m], dx=domain.dx[m], dy=domain.dy[m], channel.l0, channel.L0, centered=false, Nsh, channel.model, fftplan) :
        nuft_phase_screen!(screen; r0=r0[m], dx=domain.dx[m], dy=domain.dy[m], channel.l0, channel.L0, centered=false, nthreads=1, tol=1e-5, channel.model)

        @. R[:, :, m] = cis(real(screen))
    end

    split_step_asym!(u, λ, domain.dx, domain.z, R; fftplan)
end

function turbulent_channel(T::DataType=Float64; domain::Domain=Domain{T}(0.3 / 256, 1e2, 256, 11), channel::TurbulentChannel=TurbulentChannel{T}(1e-14, 1e-2, 1e2, andrews), beam::Beam=Beam{T}(0, 0, 1.55e-6, 1e-2), kwargs...)

    # generate Gaussian beam
    u = gaussian_source(domain, beam.w0, beam.λ, (beam.l, beam.m))

    # estimate proper r0
    r0 = partial_r0_pw(beam.k0, domain.z, channel.Cn²)

    turbulent_channel!(u, r0, domain, channel; λ=beam.λ, kwargs...)
end
