Base.@irrational sqrt2 1.4142135623730950488 √big(2)

"""
lg(r, ϕ, z, l, m, w0, λ)

Compute the LG_{l,m}-mode at the cylindrical coordinates r, ϕ, and z, where l is the azimuthal and m the radial mode index (both integers, m >= 0). w0 is the beam radius of the corresponding fundamental Gaussian beam and λ the vacuum wavelength.
"""
lg(r::AbstractArray{T1,3}, ϕ::AbstractArray{T2,3}, z::AbstractArray{T3,3}, args...) where {T1,T2,T3<:Number} = broadcast(lg_fun(args...), r, ϕ, z)

lg(r::AbstractMatrix{T1}, ϕ::AbstractMatrix{T2}, z::T3, args...) where {T1,T2,T3<:Number} = broadcast(lg_fun(args...), r, ϕ, z)

lg(r::Number, ϕ::Number, z::Number, args...) = broadcast(lg_fun(args...), r, ϕ, z)

# now, the dimension-extending version
function lg(r, ϕ, z, args...)
    fun = lg_fun(args...)
    [fun(x...) for x ∈ Iterators.product(r, ϕ, z)]
end


"""
lg_cart(x, y, z, l, m, w0, λ)

Compute the LG_{l,m}-mode at the Cartesian coordinates x, y, and z, where l is the azimuthal and m the radial mode index (both integers, m >= 0). w0 is the beam radius of the corresponding fundamental Gaussian beam and λ the vacuum wavelength.
"""
lg_cart(x::AbstractArray{T1,3}, y::AbstractArray{T2,3}, z::AbstractArray{T3,3}, args...) where {T1,T2,T3<:Number} = broadcast(lg_fun_cart(args...), x, y, z)

lg_cart(x::AbstractMatrix{T1}, y::AbstractMatrix{T2}, z::T3, args...) where {T1,T2,T3<:Number} = broadcast(lg_fun_cart(args...), x, y, z)

lg_cart(x::Number, y::Number, z::Number, args...) = broadcast(lg_fun_cart(args...), x, y, z)

# now, the dimension-extending version
function lg_cart(x, y, z, args...)
    fun = lg_fun_cart(args...)
    [fun(x...) for x ∈ Iterators.product(x, y, z)]
end



"""
lg_fun(l, m, w0, λ)

Generate anonymous function for LG_{l,m} beam mode, where l is the azimuthal and m the radial mode index (both integers, m >= 0). w0 is the beam radius of the corresponding fundamental Gaussian beam and λ the vacuum wavelength.
"""
function lg_fun(l=0, m=0, w0=1e-2, λ=1.55e-6)
    # m has to be ≥ 0
    @assert m ≥ 0

    k0 = 2(π / λ)
    #  rayleigh length
    zr = k0 * w0^2 / 2

    # 1/e beam radius
    w(z) = w0 * sqrt(1 + (z / zr)^2)
    #  radius of curvature of phase front
    R(z) = z + zr^2 / z
    #  Gouy phase
    φ(z) = (1 + 2m + abs(l)) * atan(z, zr)


    # LG modes with propagator
    (r, ϕ, z) -> w0 / w(z) * (sqrt2 * r / w(z))^abs(l) * exp(-(r / w(z))^2) * laguerre(m, abs(l), 2 * (r / w(z))^2) * cis(-(l * ϕ + k0 * r^2 / 2R(z) - φ(z) + k0 * z))
end
function lg_fun_cart(args...)
    r(x, y) = sqrt(x^2 + y^2)
    ϕ(x, y) = atan(y, x)

    lg = lg_fun(args...)
    (x, y, z) -> lg(r(x, y), ϕ(x, y), z)
end


"""
laguerre(n, α, x)

Compute the value of the generalized Laguerre polynomial L_n^α(x). For further information see DLMF (https://dlmf.nist.gov/18.9) and https://discourse.julialang.org/t/laguerre-polynomials-on-gpu/99571/
"""
laguerre(n::Int, x::T) where {T} = laguerre(n::Int, zero(T), x::T)
function laguerre(n::Int, α::Number, x::T) where {T}

    p0, p1 = one(T), -x + (α + 1)
    n == 0 && return p0
    for k ∈ 1:n-1
        p1, p0 = ((2k + α + 1) / (k + 1) - x / (k + 1)) * p1 - (k + α) / (k + 1) * p0, p1
    end
    p1
end


function plot_lg(; l=0, m=0, λ=1.55e-6, w0=1e-2, z=0, Dx=1e-1, Nx=256)

    # define grid
    x = fftfreq(Nx, Dx) |> fftshift
    y = x

    # generate and evaluate function
    u = lg_cart(x, y, z, l, m, w0, λ)

    tr = complex_plot(; z=u, x0=first(x), dx=Dx / Nx, y0=first(y), dy=Dx / Nx)
    lt = Config(; xaxis=Config(title="x"), yaxis=Config(title="y", scaleanchor="x", autorange=true))
    Plot(tr, lt)
end