using HypergeometricFunctions
using LinearAlgebra
using Optim

# Fried parameter / fried coherence length
fried_pw(k0, Δz, Cn²) = (0.423 * k0^2 * Δz * Cn²)^(-3 // 5)
fried_sw(k0, Δz, Cn²) = (3 // 8 * 0.423 * k0^2 * Δz * Cn²)^(-3 // 5)

fried_pw_layered(k0, Δz::AbstractVector, Cn²::Number) = begin
    r0 = 0
    for m ∈ axes(Δz, 1)
        r0 += fried_pw(k0, Δz[m], Cn²)^(-5 // 3)
    end
    r0^(-3 // 5)
end
fried_pw_layered(k0, Δz::AbstractVector, Cn²::AbstractVector) = begin
    r0 = 0
    for m ∈ axes(Δz, 1)
        r0 += fried_pw(k0, Δz[m], Cn²[m])^(-5 // 3)
    end
    r0^(-3 // 5)
end

fried_sw_layered(k0, Δz::AbstractVector, Cn²::Number) = begin
    z = cumsum(Δz)
    r0 = 0
    for m ∈ axes(Δz, 1)
        r0 += (fried_pw(k0, Δz[m], Cn²)^(-5 // 3) * (z[m] / z[end])^(5 // 3))
    end
    r0^(-3 // 5)
end
fried_sw_layered(k0, Δz::AbstractVector, Cn²::AbstractVector) = begin
    z = cumsum(Δz)
    r0 = 0
    for m ∈ axes(Δz, 1)
        r0 += (fried_pw(k0, Δz[m], Cn²[m])^(-5 // 3) * (z[m] / z[end])^(5 // 3))
    end
    r0^(-3 // 5)
end

# Rytov variance
# for weak fluctuations, the scintillation index corresponds to the Rytov variance
rytov_pw(k0, z, Cn²) = 1.23Cn² * k0^(7 // 6) * z^(11 // 6)
rytov_sw(k0, z, Cn²) = 0.5Cn² * k0^(7 // 6) * z^(11 // 6)


# Scintillation index
# plane wave - compare Andrews 2019 - Field Guide ... p. 50/51 : the inner and outer scale are considered assuming the Andrews spectrum
scintillation_index_pw(k0, Δz, Cn², l0=0, L0=Inf; extended=true) = begin

    σ²_R = rytov_pw(k0, Δz, Cn²)

    κl = 3.3 / l0
    Qₗ = κl^2 * Δz / k0
    Q̂₀ = 64π^2 * Δz / (k0 * L0^2)

    # p. 51
    c1(Qₗ) = 2.61 / (2.61 + Qₗ + 0.45σ²_R * Qₗ^(7 // 6))
    σ²_lnX(Qₗ) = 0.16σ²_R * (Qₗ * c1(Qₗ))^(7 // 6) * (1 + 1.75 * sqrt(c1(Qₗ)) - 0.25 * c1(Qₗ)^(7 // 12))

    c2(Qₗ, Q̂₀) = 2.61Q̂₀ / (2.61 * (Q̂₀ + Qₗ) + Q̂₀ * Qₗ * (1 + 0.45σ²_R * Qₗ^(1 // 6)))
    σ²_lnX(Qₗ, Q̂₀) = 0.16σ²_R * (Qₗ * c2(Qₗ, Q̂₀))^(7 // 6) * (1 + 1.75 * sqrt(c2(Qₗ, Q̂₀)) - 0.25 * c2(Qₗ, Q̂₀)^(7 // 12))

    if iszero(l0)
        (L0 < Inf) && @warn "Outer scale L0=$L0 ignored."
        return exp(0.49σ²_R / (1 + 1.11σ²_R^(6 // 5))^(7 // 6) + 0.51σ²_R / (1 + 0.69σ²_R^(6 // 5))^(5 // 6)) - 1
    else
        # bottom of p. 50
        c3 = atan(Qₗ)
        σ²_pl = 3.86σ²_R * ((1 + 1 / Qₗ^2)^(11 // 12) * (sin(11 // 6 * c3) + 1.507 / (1 + Qₗ^2)^(1 // 4) * sin(4 // 3 * c3) - 0.273 / (1 + Qₗ^2)^(7 // 24) * sin(5 // 4 * c3)) - 3.50 / Qₗ^(5 // 6))

        if extended
            return exp(σ²_lnX(Qₗ) - σ²_lnX(Qₗ, Q̂₀) + (0.51 * σ²_pl) / (1 + 0.69 * σ²_pl^(6 // 5))^(5 // 6)) - 1
        else
            return σ²_pl
        end
    end
end

# the inner and outer scale are considered assuming the Andrews spectrum
scintillation_index_sw(k0, Δz, Cn², l0=0, L0=Inf; extended=true) = begin

    β₀² = rytov_sw(k0, Δz, Cn²)

    κl = 3.3 / l0
    Qₗ = κl^2 * Δz / k0
    Q̂₀ = 64π^2 * Δz / (k0 * L0^2)

    # p. 53
    c1(Qₗ) = 8.56 / (8.56 + Qₗ + 0.20β₀² * Qₗ^(7 // 6))
    σ²_lnX(Qₗ) = 0.04β₀² * (c1(Qₗ) * Qₗ)^(7 // 6) * (1 + 1.75 * sqrt(c1(Qₗ)) - 0.25 * c1(Qₗ)^(7 // 12))

    c2(Qₗ, Q̂₀) = 8.56Q̂₀ / (8.56 * (Q̂₀ + Qₗ) + Q̂₀ * Qₗ * (1 + 0.20β₀² * Qₗ^(1 // 6)))
    σ²_lnX(Qₗ, Q̂₀) = 0.04β₀² * (Qₗ * c2(Qₗ, Q̂₀))^(7 // 6) * (1 + 1.75 * sqrt(c2(Qₗ, Q̂₀)) - 0.25 * c2(Qₗ, Q̂₀)^(7 // 12))

    if iszero(l0)
        # (L0 < Inf) && @warn "Outer scale L0=$L0 ignored."
        return exp(0.49β₀² / (1 + 0.56β₀²^(6 // 5))^(7 // 6) + 0.51β₀² / (1 + 0.69β₀²^(6 // 5))^(5 // 6)) - 1
    else
        # bottom of p. 52
        c3 = atan(Qₗ, 3)
        σ²_sph = 9.65β₀² * (0.40 * (1 + 9 / Qₗ^2)^(11 // 12) * (sin(11 // 6 * c3) + 2.610 / (9 + Qₗ^2)^(1 // 4) * sin(4 // 3 * c3) - 0.518 / (9 + Qₗ^2)^(7 // 24) * sin(5 // 4 * c3)) - 3.50 / Qₗ^(5 // 6))

        # make sure we do not end up with negative variances ... they might be an artifact from the (propbably) approximative nature of the expression for σ²_sph
        if σ²_sph < 0
            return zero(σ²_sph)
        end
        if extended
            return exp(σ²_lnX(Qₗ) - σ²_lnX(Qₗ, Q̂₀) + (0.51 * σ²_sph) / (1 + 0.69 * σ²_sph^(6 / 5))^(5 / 6)) - 1
        else
            return σ²_sph
        end
    end
end

# on-axis SI for Gaussian beam: the inner and outer scale are considered assuming the Andrews spectrum
scintillation_index_gaussian(k0, Δz, Cn², w0, l0=0, L0=Inf; extended=true) = begin

    σ²_R = rytov_pw(k0, Δz, Cn²)

    κl = 3.3 / l0
    Qₗ = κl^2 * Δz / k0
    Q̂₀ = 64π^2 * Δz / (k0 * L0^2)

    # beam parameters in Andrew's weird notation
    Λ0 = 2Δz / (k0 * w0^2) # inverse Rayleigh length
    Θ0 = 1 # 1 - Dz / F0 # F0 is the radius of curvature at z=0, we consider a collimated beam, i.e., F0 = ∞
    Θ = Θ0 / (Θ0^2 + Λ0^2)
    Λ = Λ0 / (Θ0^2 + Λ0^2)
    Θ̄ = 1 - Θ # compare p. 19
    W = w0 * sqrt(Θ0^2 + Λ0^2) # compare p. 18

    # p. 56
    η_X = inv(0.38 / (1 - 3.21Θ̄ + 5.29Θ̄^2) + 0.47σ²_R * Qₗ^(1 // 6) * ((1 // 3 - Θ̄ / 2 + Θ̄^2 / 5) / (1 + 2.20Θ̄))^(6 // 7))
    η_X0 = η_X * Q̂₀ / (η_X + Q̂₀)

    σ²_lnX(Qₗ, η_X0) = 0.49σ²_R * (1 // 3 - Θ̄ / 2 + Θ̄^2 / 5) * (η_X0 * Qₗ / (η_X0 + Qₗ))^(7 // 6) * (1 + 1.75 * sqrt(η_X0 / (η_X0 + Qₗ)) - 0.25 * (η_X0 / (η_X0 + Qₗ))^(7 // 12))
    σ²_lnX(Qₗ) = σ²_lnX(Qₗ, η_X)

    # compute scintillation index for Gaussian beam assuming Kolmogorov spectrum and weak fluctuations
    # exact solution, holds only for r < W!
    σ²_I(r) = 3.86σ²_R * real(cispi(5 // 12) * _₂F₁(-5 // 6, 11 // 6, 17 // 6, Θ̄ + 1im * Λ)) - 2.65σ²_R * Λ^(5 // 6) * _₁F₁(-5 // 6, 1, 2r^2 / W^2)
    # approximation
    # σ²_I(r) = 3.86σ²_R * (0.4 * ((1 + 2Θ)^2 + 4Λ^2)^(5 // 12) * cos(5 // 6 * atan(1 + 2Θ, 2Λ)) - 11 // 16 * Λ^(5 // 6)) + 4.42σ²_R * Λ^(5 // 6) * r^2 / W^2
    σ²_B = σ²_I(0)

    # the scintillation index of the Gaussian beam is composed of two parts, a radial and a longitudinal component. However, for r=0, i.e., on axis, the radial component vanishes, compare equation at the top of p. 56
    if iszero(l0)
        (L0 < Inf) && @warn "Outer scale L0=$L0 ignored."
        # extend validity into strong turbulence regime
        return exp(0.49σ²_B / (1 + 0.56 * (1 + Θ) * σ²_B^(6 // 5))^(7 // 6) + 0.51σ²_B / (1 + 0.69σ²_B^(6 // 5))^(5 // 6)) - 1
    else
        # bottom of p. 55
        φ₁ = atan(2Λ, 1 + 2Θ)
        φ₂ = atan((1 + 2Θ) * Qₗ, 3 + 2Λ * Qₗ)
        # upper half of p. 55

        σ²_G = 3.86σ²_R * (0.40 * ((1 + 2Θ)^2 + (2Λ + 3 / Qₗ)^2)^(11 // 12) / sqrt((1 + 2Θ)^2 + 4Λ^2) *
                           (sin(11 / 6 * φ₂ + φ₁) + 2.610 / ((1 + 2Θ)^2 * Qₗ^2 + (3 + 2Λ * Qₗ)^2)^(1 / 4) * sin(4 / 3 * φ₂ + φ₁)
                            - 0.518 / ((1 + 2Θ)^2 * Qₗ^2 + (3 + 2Λ * Qₗ)^2)^(7 / 24) * sin(5 / 4 * φ₂ + φ₁))
                           - 13.401Λ / (Qₗ^(11 / 6) * ((1 + 2Θ)^2 + 4Λ^2)) - 11 / 6 * Qₗ^(-5 / 6) * ((1 + 0.31Λ * Qₗ)^(5 / 6) + 1.096 * (1 + 0.27Λ * Qₗ)^(1 / 3) - 0.186 * (1 + 0.24Λ * Qₗ)^(1 / 4)))
        if extended
            exp(σ²_lnX(Qₗ) - σ²_lnX(Qₗ, η_X0) + (0.51 * σ²_G) / (1 + 0.69 * σ²_G^(6 / 5))^(5 / 6)) - 1
        else
            σ²_G
        end
    end
end


# log-amplitude variances (evaluate integrals (9.63) and (9.64) with wolframalpha, where gamma(17/6) / gamma(7/3) ≈ 1.448411512633859)
sigma_chi_pw(k0, Δz, Cn²) = 0.563 * k0^(7 / 6) * Δz^(5 / 6) * Cn² * (6Δz / 11)
sigma_chi_sw(k0, Δz, Cn²) = 0.563 * k0^(7 / 6) * Cn² * (3 * sqrt(π) / 22 / 2^(2 / 3) * Δz^(11 / 6) * 1.448411512633859)

sigma_chi_pw_layered(k0, Δz::AbstractVector, Cn²::Number) = begin
    z = cumsum(Δz)

    summation = 0
    for m ∈ axes(Δz, 1)
        summation += fried_pw(k0, Δz[m], Cn²)^(-5 / 3) * (1 - z[m] / z[end])^(5 / 6)
    end
    1.33 * k0^(-5 / 6) * z[end]^(5 / 6) * summation
end
sigma_chi_pw_layered(k0, Δz::AbstractVector, Cn²::AbstractVector) = begin
    z = cumsum(Δz)

    summation = 0
    for m ∈ axes(Δz, 1)
        summation += fried_pw(k0, Δz[m], Cn²[m])^(-5 / 3) * (1 - z[m] / z[end])^(5 / 6)
    end
    1.33 * k0^(-5 / 6) * z[end]^(5 / 6) * summation
end

sigma_chi_sw_layered(k0, Δz::AbstractVector, Cn²::Number) = begin
    z = cumsum(Δz)

    summation = 0
    for m ∈ axes(Δz, 1)
        summation += fried_pw(k0, Δz[m], Cn²)^(-5 / 3) * (z[m] / z[end])^(5 / 6) * (1 - z[m] / z[end])^(5 / 6)
    end
    1.33 * k0^(-5 / 6) * z[end]^(5 / 6) * summation
end
sigma_chi_sw_layered(k0, Δz::AbstractVector, Cn²::AbstractVector) = begin
    z = cumsum(Δz)

    summation = 0
    for m ∈ axes(Δz, 1)
        summation += fried_pw(k0, Δz[m], Cn²[m])^(-5 / 3) * (z[m] / z[end])^(5 / 6) * (1 - z[m] / z[end])^(5 / 6)
    end
    1.33 * k0^(-5 / 6) * z[end]^(5 / 6) * summation
end


"""
To properly represent the atmosphere, we have to find optimal r0s for each partial propagation such that the first few moments match.
"""
partial_r0_pw(k0, z, Cn²; debug=false) = partial_r0_pw!(zero(z), k0, z, Cn²; debug)
function partial_r0_pw!(r0s::AbstractVector{T}, k0, z::AbstractVector{T}, Cn²; debug=false) where {T}
    Nz = length(z)

    # handle case of Cn² = 0
    if iszero(Cn²)
        fill!(r0s, Inf)
        return r0s
    end

    # define target values
    r0 = fried_pw(k0, last(z), Cn²)
    σ²χ = sigma_chi_pw(k0, last(z), Cn²)

    # define A-matrix for plane wave, compare Schmidt (9.75)
    A = zeros(2, Nz)
    @. A[1, :] = 1
    @views @. A[2, 1:end-1] = (1 - z[1:end-1] / z[end])^(5 / 6)
    A[2, end] = 0
    # define target vector for plane wave
    b = [r0^(-5 / 3); σ²χ / 1.33 * (k0 / z[end])^(5 / 6)]

    # Objective function
    J(X) = norm(A * X .- b)

    # Constraints
    x1 = zeros(Nz)
    σ²χmax = 0.3  # Maximum log-amplitude variance per partial propagation ≈ sqrt(0.1) -> TODO: verify relation to rytov variance
    x2 = σ²χmax / 1.33 * (k0 / z[end])^(5 / 6) ./ A[2, :]
    # lower limit to local r0 for zero matrix entries 
    zeromask = @. A[2, :] == 0
    @. x2[zeromask] = 50^(-5 / 3)

    # Initial guess
    x0 = fill(fried_pw(k0, last(z) / Nz, Cn²)^(-5 / 3), Nz)
    @. x0[zeromask] = x2[zeromask] / 2

    # Optimization using Optim.jl
    result = optimize(J, x1, x2, x0, Fminbox(), autodiff=:forward)
    X = Optim.minimizer(result)

    # Convert optimization result to screen r0s
    @. r0s = X^(-3 / 5)

    # check result
    bp = A * X
    r0res = bp[1]^(-3 / 5)
    σ²χres = bp[2] * 1.33 * (z[end] / k0)^(5 / 6)

    abs((r0res - r0) / r0) > 1e-3 && @warn "Total r0: $(r0res). Target r0: $(r0)."
    abs((σ²χres - σ²χ) / σ²χ) > 1e-3 && @warn "Total σ²χ: $(σ²χres). Target σ²χ: $(σ²χ)."

    if debug
        @info "Coefficient matrix:"
        display(A)
        @info "Lower limit for r0:"
        display(x2 .^ (-3 / 5))

        # Check resulting r0sw & rytov
        bp = A * X
        r0res = bp[1]^(-3 / 5)
        σ²χres = bp[2] * 1.33 * (z[end] / k0)^(5 / 6)
        @info "Effective values: " (r0res, σ²χres)
        @info "Target values: " (r0, σ²χ)
    end

    return r0s
end

partial_r0_sw(k0, z, Cn²; debug=false) = partial_r0_sw!(zero(z), k0, z, Cn²; debug)
function partial_r0_sw!(r0s::AbstractVector{T}, k0, z::AbstractVector{T}, Cn²; debug=false) where {T}
    Nz = length(z)

    # handle case of Cn² = 0
    if iszero(Cn²)
        fill!(r0s, Inf)
        return r0s
    end

    # define target values
    r0 = fried_sw(k0, last(z), Cn²)
    σ²χ = sigma_chi_sw(k0, last(z), Cn²)

    # define A-matrix for plane wave, compare Schmidt (9.75)
    A = zeros(2, Nz)
    @. A[1, :] = (z / z[end])^(5 / 3)
    @views @. A[2, 1:end-1] = (1 - z[1:end-1] / z[end])^(5 / 6) * (z[1:end-1] / z[end])^(5 / 6)
    A[2, end] = 0
    # define target vector for spherical wave
    b = [r0^(-5 / 3); σ²χ / 1.33 * (k0 / z[end])^(5 / 6)]

    # Objective function
    J(X) = norm(A * X .- b)

    # Constraints
    x1 = zeros(Nz)
    σ²χmax = 0.3  # Maximum log-amplitude variance per partial propagation ≈ sqrt(0.1) -> TODO: verify relation to rytov variance
    x2 = σ²χmax / 1.33 * (k0 / z[end])^(5 / 6) ./ A[2, :]
    # lower limit to local r0 for zero matrix entries 
    zeromask = @. A[2, :] == 0
    @. x2[zeromask] = 50^(-5 / 3)

    # Initial guess
    x0 = fill(r0 * Nz / 3, Nz)
    @. x0[zeromask] = x2[zeromask] / 2

    # Optimization using Optim.jl, IPNewton can often handle initial guesses that violate the constraints
    # dfc = TwiceDifferentiableConstraints(x1, x2)
    # result = optimize(J, dfc, x0, IPNewton(), autodiff=:forward)
    # By adjusting the initial guess properly, we can avoid the constraint violation
    result = optimize(J, x1, x2, x0, Fminbox(), autodiff=:forward)
    X = Optim.minimizer(result)

    # Convert optimization result to screen r0s
    @. r0s = X^(-3 / 5)

    # check result
    bp = A * X
    r0res = bp[1]^(-3 / 5)
    σ²χres = bp[2] * 1.33 * (z[end] / k0)^(5 / 6)

    abs((r0res - r0) / r0) > 1e-3 && @warn "Total r0: $(r0res). Target r0: $(r0)."
    abs((σ²χres - σ²χ) / σ²χ) > 1e-3 && @warn "Total σ²χ: $(σ²χres). Target σ²χ: $(σ²χ)."

    if debug
        @info "Coefficient matrix:"
        display(A)
        @info "Lower limit for r0:"
        display(x2 .^ (-3 / 5))

        # Check resulting r0sw & rytov
        bp = A * X
        r0res = bp[1]^(-3 / 5)
        σ²χres = bp[2] * 1.33 * (z[end] / k0)^(5 / 6)
        @info "Effective values: " (r0res, σ²χres)
        @info "Target values: " (r0, σ²χ)
    end

    return r0s
end