include("helpers.jl")
include("generate_lg.jl")
include("newtypes.jl")

function gaussian_source(domain::Domain{T}, w0, λ, midx=(0, 0)) where {T}
    utx = zeros(Complex{T}, domain.Nx, domain.Ny)
    broadcast!(lg_fun_cart(midx..., T(w0), T(λ)), utx, domain.x(1), transpose(domain.y(1)), zero(T))

    maybe_cuda(utx)
end
function gaussian_source(domain::Domain{T}, w0, λ, midx::AbstractVector=[(0, 0)]) where {T}
    utx = zeros(Complex{T}, domain.Nx, domain.Ny, length(midx))

    for (k, m) ∈ enumerate(midx)
        broadcast!(lg_fun_cart(m..., T(w0), T(λ)), view(utx, :, :, k), domain.x(1), transpose(domain.y(1)), zero(T))
    end

    maybe_cuda(utx)
end
"""
    point_source(domain[, λ, ROI])

Combination of sinc and Gaussian point-source models according to Schmidt 2010 p. 111, i.e., listing 6.9
"""
function point_source(domain::Domain{T}, λ=1.55e-6, ROI=0.94domain.Dx[end]) where {T}
    utx = maybe_cuda(zeros(Complex{T}, domain.Nx, domain.Ny))
    x = domain.x(1)
    yt = domain.y(1) |> transpose

    arg = ROI / (λ * domain.Dz)

    @. utx = λ * domain.Dz * cispi((x^2 + yt^2) / (λ * domain.Dz)) * arg^2 * sinc(arg * x) * sinc(arg * yt) * exp(-(arg / 4)^2 * (x^2 + yt^2))
end

function super_gaussian_source(domain::Domain{T}, w=0.25, N=8) where {T}
    utx = maybe_cuda(zeros(Complex{T}, domain.Nx, domain.Ny))
    x = domain.x(1)
    yt = domain.y(1) |> transpose
    @. utx = exp(-((x^2 + yt^2) / T(w)^2)^N)
end

function plane_source(domain::Domain{T}) where {T}
    maybe_cuda(fill(one(Complex{T}), domain.Nx, domain.Ny))
end