include("propagators.jl")

using LinearAlgebra
using FFTW

function isuniform(x::AbstractVector{T}, ε=2eps(T)) where {T<:AbstractFloat}
    (length(x) <= 1) && return true
    !isfinite(last(x) - first(x)) && return false

    # Create uniform range as reference. If deviation from reference exceeds ϵ, spacing is nonuniform.
    n = length(x)
    xu = range(first(x), last(x), length=n)

    return all(i -> abs(x[i] - xu[i]) <= ε, Base.OneTo(n))
end

"""
freespace_tf_fr()

Compute the transfer function of free-space propagation under the Fresnel approximation. We drop the constant phase term `-k_0 \\Delta z`. This change is crucial if we want to perform single-precision arithmetic!
"""
# freespace_tf_fr(κx, κy, k0, Δz) = cis(-Δz * (k0 - (κx^2 + κy^2) / 2k0))
freespace_tf_fr(κx, κy, k0, Δz) = cis(Δz / 2k0 * (κx^2 + κy^2))

"""
freespace_tf()

Compute the transfer function of free-space propagation.
"""
freespace_tf(κx, κy, k0, Δz) = cis(-Δz * sqrt(k0^2 - κx^2 - κy^2))


"""
split_step_sym!(u,λ,dx,z,R)

Propagate the input field distribution u (shifted to agree with FFTW alignment) with a wavelength λ, generated on a transverse grid with resolution dx, along the z direction where z is a vector containing the discrete z-positions. R is a vector of matrices representing the refraction operators. This function implements the **symmetric** split-step method.
pre-plan fft, critical for CUDA-version: The fft-planning is super costly why pre-planning is more than worth the extra code
"""
function split_step_sym!(u::AbstractMatrix{Complex{T}}, λ, dx, dy, z, R::AbstractArray{Complex{T},3}; fftplan=plan_fft!(u)) where {T<:Union{Float32,Float64}}

    # uniform rectangular grid
    M, N = size(u)

    # Super-Gaussian window
    mg = fftfreq(M, one(T))
    ng = fftfreq(N, one(T)) |> transpose
    w = similar(u, T)
    @. w = exp(-((mg^2 + ng^2) / T(0.47)^2)^8)

    # Vaccum wavenumber
    k0 = 2(π / λ)

    # angular spatial frequencies
    dκx = 2(π / (M * dx))
    dκy = 2(π / (N * dy))
    κx = fftfreq(M, M * dκx)
    κy = fftfreq(M, N * dκy) |> transpose
    
    # propagation step sizes
    Δz = diff(z)
    # uniform z axis?
    uflag = isuniform(z)
    # transfer function
    tf = similar(u)
    @. tf = freespace_tf_fr(κx, κy, k0, Δz[1] / 2)

    @views for m ∈ eachindex(Δz)
        # Windowing
        @. u *= w
        # If z is not equidistantly spaced, update transfer function
        !uflag && (@. tf = freespace_tf_fr(κx, κy, k0, Δz[m] / 2))
        # Propagation
        asm_simple!(u, tf, fftplan)
        # Windowing
        @. u *= w
        # Refraction
        @. u *= R[:, :, m]
        # Propagation
        asm_simple!(u, tf, fftplan)
    end

    u
end


"""
split_step_asym!(u,λ,dx,z,R)

Propagate the input field distribution u (shifted to agree with FFTW alignment) with a wavelength λ along the z direction where z is a vector containing the discrete z-positions. The resolution of the transverse grid at each of the z-positions is given by the vector dx. R is a vector of matrices representing the refraction operators. This function implements an **asymmetric** split-step method.
pre-plan fft, critical for CUDA-version: The fft-planning is super costly why pre-planning is more than worth the extra code
"""
function split_step_asym!(u::AbstractMatrix{Complex{T}}, λ, dx::Number, dy::Number, z::AbstractVector, R::AbstractArray{Complex{T},3}; fftplan=plan_fft!(u)) where {T<:Union{Float32,Float64}}

    # uniform rectangular grid
    M, N = size(u)

    # Super-Gaussian window
    mg = fftfreq(M, one(T))
    ng = fftfreq(N, one(T)) |> transpose
    w = similar(u, T)
    @. w = exp(-((mg^2 + ng^2) / T(0.47)^2)^8)

    # Vaccum wavenumber
    k0 = 2(π / λ)

    # angular spatial frequencies
    dκx = 2(π / (M * dx))
    dκy = 2(π / (N * dy))
    κx = fftfreq(M, M * dκx)
    κy = fftfreq(M, N * dκy) |> transpose
    
    # propagation step sizes
    Δz = diff(z)
    # uniform z axis?
    uflag = isuniform(z)
    # transfer function
    tf = similar(u)
    @. tf = freespace_tf_fr(κx, κy, k0, Δz[1])

    @views for m ∈ eachindex(Δz)
        # If z is not equidistantly spaced, update transfer function
        !uflag && (@. tf = freespace_tf_fr(κx, κy, k0, Δz[m]))
        # Windowing
        @. u *= w
        # Refraction
        @. u *= R[:, :, m]
        # Propagation
        asm_simple!(u, tf, fftplan)
    end

    u
end

# TODO: (maybe) make cpu and gpu optimized version
function split_step_asym!(u::AbstractMatrix{Complex{T}}, λ, dx::AbstractVector, z::AbstractVector, R::AbstractArray{Complex{T},3}; fftplan=plan_fft!(u), κlim=Inf) where {T}

    Nx, Ny = size(u)

    # preallocate spatial/spectral grids
    r1² = similar(u, T, Nx, Ny)
    κ² = similar(u, T, Nx, Ny)
    r2² = similar(u, T, Nx, Ny)

    # Super-Gaussian window
    mg = fftfreq(Nx, one(T))
    ng = fftfreq(Ny, one(T)) |> transpose
    w = similar(u, T)
    @. w = exp(-((mg^2 + ng^2) / T(0.47)^2)^8)

    # plane distance
    Δz = diff(z)

    # source plane grid
    x1 = fftfreq(Nx, Nx * dx[begin])
    y1 = fftfreq(Ny, Ny * dx[begin]) |> transpose
    @. r1² = x1^2 + y1^2

    mag = dx[begin+1] / dx[begin]
    # apply quadratic phase in source plane
    @. u *= cispi(-1 / (λ * Δz[begin]) * (1 - mag) * r1²)

    @views for m ∈ eachindex(Δz)
        # magnification
        mag = dx[m+1] / dx[m]
        # scale amplitude to account for (potential) grid size change
        @. u /= mag
        # apply phase screen
        @. u *= R[:, :, m]
        # apply super Gaussian window
        @. u *= w

        # FFT -> propagate to intermediate plane
        fftplan * u

        # intermediate plane grid (spectral)
        dκx = 2π / (Nx * dx[m])
        dκy = 2π / (Ny * dx[m])
        κx = fftfreq(Nx, Nx * dκx)
        κy = fftfreq(Ny, Ny * dκy) |> transpose
        @. κ² = κx^2 + κy^2

        # apply (circular/elliptical) brickwall filter
        if isfinite(κlim)
            κc² = (Nx * dκx / 2 * κlim)^2 + (Ny * dκy / 2 * κlim)^2
            @. u[κ²>κc², :] = 0
        end
        
        # apply quadratic phase in intermediate plane
        @. u *= cis(Δz[m] * λ / (4mag * π) * κ²)

        # IFFT -> propagate to target plane
        fftplan \ u
    end

    # apply final phase screen
    @views @. u *= R[:, :, end]

    # observation plane grid
    x2 = fftfreq(Nx, Nx * dx[end])
    y2 = fftfreq(Ny, Ny * dx[end]) |> transpose
    @. r2² = x2^2 + y2^2

    # final magnification
    mag = dx[end] / dx[end-1]

    # apply quadratic phase in target plane
    @. u *= cispi(-1 / (λ * Δz[end]) * (1 - 1 / mag) * r2²)

    u
end