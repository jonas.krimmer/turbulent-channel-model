# functionalize monte carlo loop to enable type-stable multi-threading only for CPU arrays
function mc_iterations!(urx::CuArray{T,3}, utx::CuArray{T,2}, r0, domain::Domain, channel::TurbulentChannel, chunksize=1, chunk=1, nchunks=1; showprogress, kwargs...) where {T<:Complex}

    niters = size(urx, 3)
    prg = Progress(niters, desc="Chunk $chunk/$nchunks: ", enabled=showprogress)

    for i ∈ 1:niters
        maybe_cuda_seed!((chunk - 1) * chunksize + i)

        u = view(urx, :, :, i)
        copy!(u, utx)
        turbulent_channel!(u, r0, domain, channel; kwargs...)

        next!(prg)
    end
end
function mc_iterations!(urx::AbstractArray{T,3}, utx::AbstractArray{T,2}, r0, domain::Domain, channel::TurbulentChannel, chunksize=1, chunk=1, nchunks=1; showprogress, kwargs...) where {T<:Complex}

    niters = size(urx, 3)
    prg = Progress(niters, desc="Chunk $chunk/$nchunks: ", enabled=showprogress)

    n_fftw_threads = FFTW.get_num_threads()
    FFTW.set_num_threads(1)
    n_blas_threads = BLAS.get_num_threads()
    BLAS.set_num_threads(1)

    Threads.@threads :static for i ∈ 1:niters
        maybe_cuda_seed!((chunk - 1) * chunksize + i)

        u = view(urx, :, :, i)
        copy!(u, utx)
        turbulent_channel!(u, r0, domain, channel; kwargs...)

        next!(prg)
    end

    FFTW.set_num_threads(n_fftw_threads)
    BLAS.set_num_threads(n_blas_threads)
end

function mc_iterations!(urx::CuArray{T,4}, utx::CuArray{T,3}, r0, domain::Domain, channel::TurbulentChannel, chunksize=1, chunk=1, nchunks=1; showprogress, kwargs...) where {T<:Complex}

    niters = size(urx, 4)
    nmodes = size(urx, 3)
    prg = Progress(niters * nmodes, desc="Chunk $chunk/$nchunks: ", enabled=showprogress)

    for i ∈ 1:niters
        for m ∈ axes(utx, 3)
            maybe_cuda_seed!((chunk - 1) * chunksize + i)

            u = view(urx, :, :, m, i)
            copy!(u, view(utx, :, :, m))
            turbulent_channel!(u, r0, domain, channel; kwargs...)

            next!(prg)
        end
    end
end
function mc_iterations!(urx::AbstractArray{T,4}, utx::AbstractArray{T,3}, r0, domain::Domain, channel::TurbulentChannel, chunksize=1, chunk=1, nchunks=1; showprogress, kwargs...) where {T<:Complex}

    niters = size(urx, 4)
    nmodes = size(urx, 3)
    prg = Progress(niters * nmodes, desc="Chunk $chunk/$nchunks: ", enabled=showprogress)

    n_fftw_threads = FFTW.get_num_threads()
    FFTW.set_num_threads(1)
    n_blas_threads = BLAS.get_num_threads()
    BLAS.set_num_threads(1)

    Threads.@threads :static for i ∈ 1:niters
        for m ∈ axes(utx, 3)
            maybe_cuda_seed!((chunk - 1) * chunksize + i)

            u = view(urx, :, :, m, i)
            copy!(u, view(utx, :, :, m))
            turbulent_channel!(u, r0, domain, channel; kwargs...)

            next!(prg)
        end
    end

    FFTW.set_num_threads(n_fftw_threads)
    BLAS.set_num_threads(n_blas_threads)
end
