using Bessels
using Roots

"""
lp(r, ϕ, l, m, ψ=0)

Calculate distribution of the LP_{l,m} beam mode at positions given by r and ϕ, where l is the azimuthal and m the radial mode index (both integers, m > 0). even_odd determines whether the azimuthal dependency follows a sine (odd) or a cosine (even) pattern. a is the fiber core radius and V the normalized frequency.
"""
function lp!(u::AbstractArray{T}, r::AbstractArray{R}, ϕ::AbstractArray{R}, ψ=0; l::Integer=0, m::Integer=1, a=4.5e-6, V=1.7) where {T<:Union{R,Complex{R}}} where {R<:Real}
    lpfun = lp_fun(l, m, a, V)
    map!((r, ϕ) -> lpfun(r, ϕ, ψ), u, r, ϕ)
end
function lp_cart!(u::AbstractArray{T}, x::AbstractArray{R}, y::AbstractArray{R}, ψ=0; l::Integer=0, m::Integer=1, a=4.5e-6, V=1.7) where {T<:Union{R,Complex{R}}} where {R<:Real}
    lpfun = lp_fun_cart(l, m, a, V)
    map!((x, y) -> lpfun(x, y, ψ), u, x, y)
end

function lp(r::AbstractArray{T}, ϕ::AbstractArray{T}, ψ; l::Integer=0, m::Integer=1, a=4.5e-6, V=1.7) where {T<:Real}
    u = similar(r)
    lp!(u, r, ϕ, ψ; l, m, a, V)
end
function lp(r::T, ϕ::T, ψ=0; l::Integer=0, m::Integer=1, a=4.5e-6, V=1.7) where {T<:Real}
    lpfun = lp_fun(l, m, a, V)
    lpfun(r, ϕ, ψ)
end
function lp_cart(x::AbstractArray{T}, y::AbstractArray{T}, ψ=0; l::Integer=0, m::Integer=1, a=4.5e-6, V=1.7) where {T<:Real}
    u = similar(x)
    lp_cart!(u, x, y, ψ; l, m, a, V)
end
function lp_cart(x::T, y::T, ψ=0; l::Integer=0, m::Integer=1, a=4.5e-6, V=1.7) where {T<:Real}
    lpfun = lp_fun_cart(l, m, a, V)
    lpfun(x, y, ψ)
end


"""
lp_fun(l, m[, a, V])

Generate anonymous function for LP_{l,m} beam mode, where l is the azimuthal and m the radial mode index (both integers, m > 0). a is the fiber core radius and V the normalized frequency.
"""
function lp_fun(l::Integer, m::Integer, a=4.5e-6, V=1.7)
    # obtain normalized transverse propagation constant from dispersion relation
    u = solve_dispersion_relation(l, m, V)
    w = sqrt(V^2 - u^2)

    (r, ϕ, ψ=0) -> (r ≤ a) ? besselj(l, u * r / a) * cos(l * ϕ + ψ) : besselj(l, u) / besselk(l, w) * besselk(l, w * r / a) * cos(l * ϕ + ψ)
end

function lp_fun_cart(args...)
    r(x, y) = sqrt(x^2 + y^2)
    ϕ(x, y) = atan(y, x)

    lp = lp_fun(args...)
    (x, y, ψ=0) -> lp(r(x, y), ϕ(x, y), ψ)
end

"""
solve_dispersion_relation(l, m, V)

Solve dispersion relation for LP_{l,m} in fiber with normalized frequency V. l is the azimuthal and m the radial mode index.
"""
function solve_dispersion_relation(l::Integer, m::Integer, V::Real)
    ϵ = 1e-14

    w(u) = sqrt(V^2 - u^2)
    f(u) = u * besselj(l - 1, u) / besselj(l, u) + w(u) * besselk(l - 1, w(u)) / besselk(l, w(u))

    lower = isone(m) ? 0 : besselj_l_kth_zero(l, m - 1)
    lower += ϵ
    upper = min(besselj_l_kth_zero(l, m), V) - ϵ

    (lower >= V) && throw("The cut-off frequency of mode ($l,$m) Vc=$lower exceeds V=$V.")

    find_zero(f, (lower, upper))
end


"""
besselj_l_kth_zero(l, k)

Obtain kth zero of J_l(x)
"""
function besselj_l_kth_zero(l::Integer, k::Integer)
    # J_{-l} = (-1)^l * J_l
    l = abs(l)

    # see NIST Handbook, Olver 2010 (10.21 (vi))
    a = (k + l//2 - 1//4)*π
    µ = 4*l^2
    # this approximation holds for large zeros, but should provide a fairly accurate starting point 
    j_nu_k = a - (µ-1)/8/a - 4*(µ-1)*(7µ-31)/3/(8a)^3 - 32*(µ-1)*(83*µ^2 - 982µ + 3779)/15/(8a)^5 - 64*(µ-1)*(6949*µ^3 - 153855*µ^2 + 1585743µ - 6277237)/105/(8a)^7
    
    # refine
    find_zero(x -> besselj(l, x), j_nu_k)
end


function plot_lp(; l=0, m=1, V=1.7, a=4.5e-6, Dx=30e-6, Nx=256, ψ=0)
    # define grid
    x = fftfreq(Nx, Dx) |> fftshift
    y = x
    X, Y = ndgrid(x, y)

    # generate and evaluate function
    u = lp_cart(X, Y, ψ; l, m, a, V)

    # plot and highlight core-cladding interface
    shapes = [Config(type="circle", x0=-a, x1=a, y0=-a, y1=a, line=Config(width=2, dash="dash", color="white"))]
    tr = complex_plot(; z=u, x0=first(x), dx=Dx / Nx, y0=first(y), dy=Dx / Nx)
    lt = Config(; xaxis=Config(title="x"), yaxis=Config(title="y", scaleanchor="x", autorange=true), shapes)
    Plot(tr, lt)
end