include("generate_lp.jl")
include("generate_hg.jl")

"""
    freespace_coupling!(u, domain, beam, modes)

    Simulate the coupling to the given modes.

"""
function freespace_coupling!(u, domain, beam, wg, f, dxf=focal_plane_res(domain, beam, wg, f)[1])

    focus!(u, domain.dx[end], dxf, beam.k0, f)

    η = OrderedDict{Tuple,Float64}()

    for (k, fields) ∈ wg.modes
        push!(η, k => coupling_efficiency(u, fields))
    end

    return η
end
freespace_coupling(u, args...) = freespace_coupling!(copy(u), args...)

function focal_plane_res(domain, beam, wg, f)
    # # critical sampling
    beam.λ * f / domain.Dx, beam.λ * f / domain.Dy
    # 6wg.a / domain.Nx, 6wg.a / domain.Ny
end



_magnification_ref(wg::T) where {T<:FiberModes} = isnan(wg.MFR) ? wg.a : wg.MFR
_magnification_ref(wg::T, k) where {T<:FiberModes} = begin
    V = k * wg.a * wg.NA
    # https://fibercore.humaneticsgroup.com/services-support/fiberpaedia/m/mode-field-diameter-mfd
    isnan(wg.MFR) ? wg.a * (0.65 + 1.619 / V^(3 / 2) + 2.879 / V^6) : wg.MFR
end
_magnification_ref(wg::T) where {T<:FreeSpaceModes} = wg.w
_magnification_ref(wg::T, k) where {T<:FreeSpaceModes} = _magnification_ref(wg)

"""
    optimize_focus(domain, beam, fiber, Cn²[, l0])

    Optimize the coupling based on the incoming beam, aperture size, and (optionally) turbulence.

"""
function optimize_focus(domain::Domain, beam::Beam, wg::Modes; debug=false)

    # determine optimal focal length
    m0 = _magnification_ref(wg, beam.k0) / beam.w0

    zR = beam.k0 * beam.w0^2 / 2
    p = -2domain.Dz / (domain.Dz^2 + zR^2)
    q = (1 - 1 / m0^2) / (domain.Dz^2 + zR^2)
    f = inv(-p / 2 + sqrt(p^2 / 4 - q))

    # focused beam (without turbulence): compare equation (11) from paper by Self
    w = beam.w0 / sqrt((1 - domain.Dz / f)^2 + (zR / f)^2)

    if debug
        @info "Magnification: " m0
        @info "Focal length: " f
        @info "Spot size: " w
    end

    return f
end


"""
    aperture!(u, dx, rA)

    Apply a circular aperture (radius rA) to the incoming field.

"""
function aperture!(u::AbstractArray{T,D}, dx, rA; centered=false) where {T,D}

    M, N = size(u)
    x = fftfreq(M, M * dx)
    y = fftfreq(N, N * dx)
    centered && (x = fftshift(x))
    centered && (y = fftshift(y))

    # squared aperture radius
    rA² = rA^2

    # free-running dimensions
    colons = ntuple(_ -> :, Val(D - 2))

    @inbounds for n ∈ eachindex(y)
        for m ∈ eachindex(x)
            r² = x[m]^2 + y[n]^2
            (r² > rA²) && (u[m, n, colons...] .= 0)
        end
    end
end

function aperture!(u::CuArray{T,D}, dx, rA; centered=false) where {T,D}

    M, N = size(u)
    x = fftfreq(M, M * dx)
    y = fftfreq(N, N * dx)
    centered && (x = fftshift(x))
    centered && (y = fftshift(y))

    # squared aperture radius
    rA² = rA^2

    # free-running dimensions
    colons = ntuple(_ -> :, Val(D - 2))

    yt = transpose(y)

    r² = similar(u, real(T), M, N)
    @. r² = x^2 + yt^2

    @. u[r²>rA², colons...] = 0

    nothing
end

"""
    lens(u)

    Apply a circular lens with a parabolic phase profile to the incoming field.

"""
function lens!(u::AbstractArray{T}, dx, k, f; centered=false) where {T<:Complex}

    M, N = size(u)
    x = fftfreq(M, M * dx)
    y = fftfreq(N, N * dx)
    centered && (x = fftshift(x))
    centered && (y = fftshift(y))

    # apply parabolic phase profile of lens
    @inbounds for n ∈ eachindex(y)
        @simd for m ∈ eachindex(x)
            r² = x[m]^2 + y[n]^2
            u[m, n] *= cis(-k / 2f * r²)
        end
    end
end

function lens!(u::CuArray{Complex{T}}, dx, k, f; centered=false) where {T}

    M, N = size(u)
    x = fftfreq(M, M * dx)
    y = fftfreq(N, N * dx)
    centered && (x = fftshift(x))
    centered && (y = fftshift(y))

    yt = transpose(y)
    r² = similar(u, T, M, N)
    @. r² = x^2 + yt^2

    @. u *= cis(-k / 2f * r²)
end


"""
    focus!(u)

    Project the complex amplitude of the Gauss-Laguerre beam u defined on the grid (x,y) onto a grid with resolution dx2. Apply an aperture with radius rA.

"""
function focus!(u::AbstractArray{T}, dx1, dx2, k, f) where {T<:Complex}
    lens!(u, dx1, k, f)
    twostep_prop_ang!(u, dx1, dx2, k, f) # propagate to focus
end


"""
    overlap_integral(u,v)

    Calculates the overlap integral of two complex field amplitudes (simplified orthogonality relation). The underlying grid is assumed to be uniform in each direction. Adapted from OWF Lecture Notes equation 6.36.

"""
function overlap_integral(u, v)

    nu = sum(abs2, u)
    iszero(nu) && return NaN
    nv = sum(abs2, v)
    iszero(nv) && return NaN

    dot(u, v) / sqrt(nu * nv)
end


"""
    coupling_efficiency(u,v)

    Calculates the power coupling efficiency of two complex field amplitudes (simplified orthogonality relation). The underlying grid is assumed to be uniform in each direction. For more information see OWF Lecture Notes equation 6.34.

"""
function coupling_efficiency(u, v)

    nu = sum(abs2, u)
    iszero(nu) && return NaN
    nv = sum(abs2, v)
    iszero(nv) && return NaN

    abs2(dot(u, v)) / (nu * nv)
end


"""
    generate_fiber_modes(M,N,dx,a,V)

    Calculate the fiber modes of a fiber with core radius a, normalized frequency V. The square grid has a spatial resolution dx and is composed of NxN samples.

"""
function generate_fiber_modes(M, N, dx, dy, a, V)

    T = Float64
    r, ϕ = maybe_cuda.(polargrid(M, dx, N, dy))

    fmodes = OrderedDict{Tuple{Int,Int,Symbol},containertype(r){Complex{T}}}()
    cutoff = Dict{Tuple{Int,Int,Symbol},T}()

    rad = 1
    # radial along columns
    while true
        az = 0
        # azimuthal along rows
        while true
            try
                solve_dispersion_relation(az, rad, V)
            catch
                break
            end

            # pair even and odd modes if odd mode does not vanish, i.e., if az != 0 retain save cutoff values
            if iszero(az)
                key = (az, rad, Symbol(""))
                push!(fmodes, key => similar(r, T, M, N))
                lp!(fmodes[key], r, ϕ; l=az, m=rad, a, V)
                push!(cutoff, key => isone(rad) ? 0 : besselj_l_kth_zero(az - 1, rad))
            else
                keye, keyo = (az, rad, :e), (az, rad, :o)
                push!(fmodes, keye => similar(r, T, M, N), keyo => similar(r, T, M, N))
                lp!(fmodes[keye], r, ϕ, 0; l=az, m=rad, a, V)
                lp!(fmodes[keyo], r, ϕ, π / 2; l=az, m=rad, a, V)
                c = besselj_l_kth_zero(az - 1, rad)
                push!(cutoff, keye => c, keyo => c)
            end

            az += 1
        end

        iszero(az) && break
        rad += 1
    end

    sort!(fmodes, by=x -> cutoff[x])
end


function generate_oam_modes(M, N, dx, dy, λ; Nmodes=6, w=300e-6)
    r, ϕ = maybe_cuda.(polargrid(M, dx, N, dy))
    OrderedDict((ℓ, 0) => lg(r, ϕ, 0, ℓ, 0, w, λ) for ℓ ∈ 0:Nmodes-1)
end


function generate_lg_modes(M, N, dx, dy, λ; Nmodes=6, w=300e-6)
    r, ϕ = maybe_cuda.(polargrid(M, dx, N, dy))

    ng = cld(sqrt(8Nmodes + 1) - 1, 2) |> Int

    midx = sort!([(m, n) for m ∈ range(0, ng - 1) for n ∈ range(0, ng - m - 1)], by=sum)
    deleteat!(midx, Nmodes+1:length(midx))

    OrderedDict((m, n) => lg(r, ϕ, 0, m, n, w, λ) for (m, n) ∈ midx)
end

function generate_hg_modes(M, N, dx, dy, λ; Nmodes=6, w=300e-6)
    x, y = maybe_cuda.(cartgrid(M, dx, N, dy))

    ## Our Proteus-C features the first 6 HG modes, i.e., the first three HG mode groups
    ng = cld(sqrt(8Nmodes + 1) - 1, 2) |> Int

    midx = sort!([(m, n) for m ∈ range(0, ng - 1) for n ∈ range(0, ng - m - 1)], by=sum)
    deleteat!(midx, Nmodes+1:length(midx))

    OrderedDict((m, n) => hg(x, y, 0, m, n, w, λ) for (m, n) ∈ midx)
end

function setup_modes(wg::T, domain::Domain, beam::Beam, f) where {T<:FiberModes}
    dxf, dyf = focal_plane_res(domain, beam, wg, f)
    generate_fiber_modes(domain.Nx, domain.Ny, dxf, dyf, wg.a, wg.a * beam.k0 * wg.NA)
end
function setup_modes(wg::T, domain::Domain, beam::Beam, f) where {T<:FreeSpaceModes}
    dxf, dyf = focal_plane_res(domain, beam, wg, f)
    if wg.which === :LG
        generate_lg_modes(domain.Nx, domain.Ny, dxf, dyf, beam.λ, w=wg.w, Nmodes=wg.nmodes)
    elseif wg.which === :OAM
        generate_oam_modes(domain.Nx, domain.Ny, dxf, dyf, beam.λ, w=wg.w, Nmodes=wg.nmodes)
    else # which === :HG
        generate_hg_modes(domain.Nx, domain.Ny, dxf, dyf, beam.λ, w=wg.w, Nmodes=wg.nmodes)
    end
end