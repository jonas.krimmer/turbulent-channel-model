include("newtypes.jl")
#= 
**COMMENTS**
dx1 and dx2 are the spatial resolution in the source and observation plane
D1 and D2 are the maximum spatial extents (essentially, the ROIs) of the field in the source and observation plane -> in general, the ROI in the observation plane can only be a subset of the simulated domain to avoid aliasing
=#

function check_geometry_sampling(z::Number, N, dx1, D1, dx2=dx1, D2=D1/2; λ=1.55e-6, debug=false)

    # compare Schmidt 2010 (7.14)
    if dx1 != dx2
        dx2max = (-D2 * dx1 + λ * z) / D1
        
        if dx2max < 0
            @warn "$(D2 * dx1) = D2 * dx1 > λ * z = $(λ * z)"
        end
        req1 = dx2 ≤ dx2max
        # if true, the selected grid spacings adequately sample the spatial bandwidth that affects the observation-plane region of interest
        if req1
            debug && (@info "Criterion (1) (dx2): dx2 ≤ $dx2max")
        else
            @warn "Criterion (1) (dx2) not satisfied: $dx2 > $dx2max"
            if dx2max < 0
                @info "The ROI of the observation plane might have been chosen too large."
            end
        end
    else
        dx1max = λ * z / (D1 + D2)
        req1 = dx1 ≤ dx1max

        if req1
            debug && (@info "Criterion (1) (dx1): $dx1 ≤ $dx1max")
        else
            @warn "Criterion (1) (dx1) not satisfied: $dx1 > $dx1max"
        end
    end

    # determine the necessary spatial extent of the observation-plane grid, compare Schmidt 2010 (7.20)
    Nmin = D1 / 2dx1 + D2 / 2dx2 + λ * z / (2 * dx1 * dx2)

    req2 = N ≥ Nmin
    if req2
        debug && (@info "Criterion (2) (N): N ≥ $Nmin")
    else
        @warn "Criterion (2) (N) not satisfied: $N < $Nmin"
    end

    return req1, req2
end

# special case for single propagation, i.e., two planes
function check_asm_sampling(Dz::Number, N, dx1, D1, dx2=dx1, D2=D1; λ=1.55e-6, R=Inf, debug=false)
    
    dx2min = (1 + Dz / R) * dx1 - λ * Dz / D1
    dx2max = (1 + Dz / R) * dx1 + λ * Dz / D1
    req3 = dx2min ≤ dx2 ≤ dx2max
    
    if req3
        debug && (@info "Criterion (3) (dxn): $dx2min ≤ dx2 ≤ $dx2max")
    else
        @warn "Criterion (3) (dxn) not satisfied: $dx2 ∉ ($dx2min, $dx2max)"
    end

    # for two planes
    Δzmax = (dx1 * dx2) * N / λ
    req4 = Dz ≤ Δzmax
    
    if req4
        debug && (@info "Criterion (4) (Δz): Δz ≤ $Δzmax")
    else
        @warn "Criterion (4) (Δz) not satisfied: $Dz > $Δzmax"
    end

    return req3, req4
end

function check_asm_sampling(z::AbstractVector, N, dx1, D1, dxn=dx1, Dn=D1; λ=1.55e-6, R=Inf, debug=false)
    # total propagation length
    Dz = abs(z[end] - z[begin])
    # step size
    Δz = diff(z)

    dxnmin = (1 + Dz / R) * dx1 - λ * Dz / D1
    dxnmax = (1 + Dz / R) * dx1 + λ * Dz / D1
    req3 = dxnmin ≤ dxn ≤ dxnmax
    
    if req3
        debug && (@info "Criterion (3) (dxn): $dxnmin ≤ dxn ≤ $dxnmax")
    else
        @warn "Criterion (3) (dxn) not satisfied: $dxn ∉ ($dxnmin, $dxnmax)"
    end

    # for multiple planes, define a maximum propagation step and make sure all steps are shorter
    Δzmax = min(dx1, dxn)^2 * N / λ
    req4 = maximum(abs, Δz) ≤ Δzmax
    
    if req4
        debug && (@info "Criterion (4) (Δz): Δz ≤ $Δzmax")
    else
        @warn "Criterion (4) (Δz) not satisfied: $(maximum(abs, Δz)) > $Δzmax"
    end

    return req3, req4
end

function check_sampling(domain::Domain, D1, Dn; λ=1.55e-6, R=Inf, r0=Inf, c=2, debug=false)

    # increase ROI sizes according to turbulence strength: The approach of Coy is to model the turbulence-induced beam spreading as if it were caused by a diffraction grating with period equal to r0 (Schmidt, p. 173)
    D1p = D1 + c * λ * domain.Dz / r0
    Dnp = Dn + c * λ * domain.Dz / r0

    # see Schmidt 2010, p. 143 & 144 (consider Errata!)
    req1, req2 = check_geometry_sampling(domain.Dz, domain.Nx, first(domain.dx), D1p, last(domain.dx), Dnp; λ, debug)
    req3, req4 = check_asm_sampling(domain.z, domain.Nx, first(domain.dx), D1p, last(domain.dx), Dnp; λ, R, debug)

    return req1, req2, req3, req4
end

function check_sampling(z, N, dx1, D1, dxn, Dn; λ=1.55e-6, R=Inf, r0=Inf, c=2, debug=false)

    Dz = last(z)
    # increase ROI sizes according to turbulence strength: The approach of Coy is to model the turbulence-induced beam spreading as if it were caused by a diffraction grating with period equal to r0 (Schmidt, p. 173)
    D1p = D1 + c * λ * Dz / r0
    Dnp = Dn + c * λ * Dz / r0

    # see Schmidt 2010, p. 143 & 144 (consider Errata!)
    req1, req2 = check_geometry_sampling(Dz, N, dx1, D1p, dxn, Dnp; λ, debug)
    req3, req4 = check_asm_sampling(z, N, dx1, D1p, dxn, Dnp; λ, R, debug)

    return req1, req2, req3, req4
end


function check_ps_sampling(dx, l0)
    (dx / l0 > 2) && (@warn "Resolution dx=$dx insufficient for sampling of inner scale l0=$l0.")
end