using FINUFFT
using FFTW
using StatsBase
using Random
using FourierTools
using CUDA
import Adapt

include("helpers.jl")
include("ndgrid.jl")
include("phase_spectrum.jl")
include("nonuniform_ranges.jl")


shift_screen!(dest::AbstractArray{T,N}, src::AbstractArray{Complex{T},N}; centered=false) where {T<:Real,N} = centered ? fftshift!(dest, real(src), (1, 2)) : copy!(dest, real(src))
shift_screen!(dest::AbstractArray{T,N}, src::AbstractArray{T,N}; centered=false) where {T<:Number,N} = centered ? fftshift!(dest, src, (1, 2)) : copy!(dest, src)

function uniform_phase_correction!(dest::AbstractArray{T,N}, dx, dy, δκx, δκy) where {T<:Complex,N}
    Nx, Ny = size(dest)
    # assume δκx and δκy feature same dimensions, i.e., 1x1x(...)
    Nδ = ntuple(d -> size(δκx, d + 2), Val(N - 2))
    x = fftfreq(Nx, Nx * dx)
    yt = fftfreq(Ny, Ny * dy) |> transpose

    # make shifts GPU-ready if necessary
    _δκx = Adapt.adapt(containertype(dest), δκx)
    _δκy = Adapt.adapt(containertype(dest), δκy)

    # @inbounds @. dest *= cis(x * δκx + yt * δκy)
    phasor = similar(dest, T, Nx, Ny, Nδ...)
    @inbounds @. phasor = cis(x * _δκx + yt * _δκy)
    @inbounds @. dest *= phasor
end

function demean_screen!(screen::AbstractArray{T,N}) where {T,N}
    @inbounds for I ∈ CartesianIndices(((axes(screen, d) for d ∈ 3:N)...,))
        vs = view(screen, :, :, I)
        vs .-= mean(vs)
    end
end


"""
    spectral_coefficients!(...)

    Cartesian grid, i.e., two distinct frequency vectors
"""
function spectral_coefficients!(c::AbstractArray{Complex{T},N}, Φ::Function, κx::AbstractVector, κy::AbstractVector, dκx::Number, dκy::Number; δκx=zero(T), δκy=zero(T)) where {T<:Real,N}

    # scaling function: factor two for correct variance in real and imaginary parts
    σ(κx, κy, δκx, δκy) = sqrt(2 * Φ(sqrt((κx + δκx)^2 + (κy + δκy)^2)) * dκx * dκy)
    # assume δκx and δκy feature same dimensions, i.e., 1x1x(Ntr) or scalar
    Nδ = ntuple(d -> size(δκx, d + 2), Val(N - 2))
    # preallocate σ array -> broadcasting is not clever enough yet(?) to consider constant dimensions
    Σ = similar(c, T, length(κx), length(κy), Nδ...)

    # fill c with normal random numbers
    randn!(c)

    # adapt frequency vectors to GPU if necessary
    κx = Adapt.adapt(containertype(c), κx)
    κyt = Adapt.adapt(containertype(c), transpose(κy))
    δκx = Adapt.adapt(containertype(c), δκx)
    δκy = Adapt.adapt(containertype(c), δκy)

    # apply σ array
    @. Σ = σ(κx, κyt, δκx, δκy)
    @inbounds @. c *= Σ
end

function spectral_coefficients!(c::AbstractArray{Complex{T}}, Φ::Function, κx::AbstractVector, κy::AbstractVector, dκx::AbstractVector, dκy::AbstractVector) where {T<:Real}

    # scaling function: factor two for correct variance in real and imaginary parts
    σ(κx, κy, dκx, dκy) = sqrt(2 * Φ(sqrt(κx^2 + κy^2)) * dκx * dκy)
    # preallocate σ array -> broadcasting is not clever enough yet(?) to consider constant dimensions
    Σ = similar(c, T, length(κx), length(κy))

    # fill c with normal random numbers
    randn!(c)

    # adapt frequency vectors to GPU if necessary
    κx = Adapt.adapt(containertype(c), κx)
    κyt = Adapt.adapt(containertype(c), transpose(κy))
    dκx = Adapt.adapt(containertype(c), dκx)
    dκyt = Adapt.adapt(containertype(c), transpose(dκy))

    # apply σ array (expensive for higher dimensions)
    # @inbounds @. c *= σ(κx, κyt, dκx, dκyt)

    # apply σ array
    @. Σ = σ(κx, κyt, dκx, dκyt)
    @inbounds @. c *= Σ
end

"""
    spectral_coefficients!(...)

    Polar grid, i.e., one distinct frequency vector
"""
function spectral_coefficients!(c::AbstractArray{Complex{T}}, Φ::Function, κ::AbstractVector, dκ::AbstractVector, dϕ::Number) where {T<:Real}

    # scaling function: factor two for correct variance in real and imaginary parts
    σ(κ, dκ) = sqrt(2 * Φ(κ) * κ * dκ * dϕ)
    # preallocate σ array -> broadcasting is not clever enough yet(?) to consider constant dimensions
    Σ = similar(c, T, 1, length(κ))

    # fill c with normal random numbers
    randn!(c)

    # adapt frequency vectors to GPU if necessary
    κt = Adapt.adapt(containertype(c), transpose(κ))
    dκt = Adapt.adapt(containertype(c), transpose(dκ))

    # apply σ array (expensive for higher dimensions)
    # @inbounds @. c *= σ(κt, dκt)

    # apply σ array
    @. Σ = σ(κt, dκt)
    @inbounds @. c *= Σ
end

function add_subharmonic!(dest::AbstractArray{T}, cn, x::AbstractVector, y::AbstractVector, κx, κy) where {T<:Complex}
    yt = transpose(y)
    _cn = Adapt.adapt(containertype(dest), cn)
    _κx = Adapt.adapt(containertype(dest), κx)
    _κy = Adapt.adapt(containertype(dest), κy)
    @. dest += _cn * cis(_κx * x + _κy * yt)
end


"""
    randu_offsets

    random frequency offsets (-0.5, 0.5) w.r.t. spectral resolution for the PWD method -> the code is quite ugly but works

"""
function _randu_offset!(x::AbstractArray, d::Number)
    rand!(x)
    @. x -= 1 // 2
    @. x *= d
end

function randu_offsets!(δκx::AbstractArray{T}, δκy::AbstractArray{T}, dκx::Number, dκy::Number; pwd=true) where {T}
    if pwd
        _randu_offset!(δκx, dκx)
        _randu_offset!(δκy, dκy)
    else
        fill!(δκx, zero(T))
        fill!(δκy, zero(T))
    end

    δκx, δκy
end

function randu_offsets(T::DataType, dκx, dκy; pwd=true)
    if pwd
        (rand(T) - 1 // 2) * dκx, (rand(T) - 1 // 2) * dκy
    else
        zero(T), zero(T)
    end
end

function randu_offsets(T::DataType, dκx, dκy, Ntr...; pwd=true)
    if pwd
        randu_offsets!(zeros(T, 1, 1, Ntr...), zeros(T, 1, 1, Ntr...), dκx, dκy)
    else
        zero(T), zero(T)
    end
end



function ft_phase_screen(; r0=0.1, Nx=256, dx=2 / Nx, dy=dx, l0=1e-2, L0=1e2, both=false, kwargs...)

    screen = Matrix{ComplexF64}(undef, Nx, Nx)
    ft_phase_screen!(screen; r0, dx, dy, l0, L0, kwargs...)

    both ? reim(screen) : real(screen)
end
"""
    ft_phase_screen!
    
    adapted from Schmidt 2010 - Numerical Simulation of Optical Wave Propagation With Examples in MATLAB
    random offset approach adapted from Paulson (2019), Journal of the Optical Society of America B, Vol. 36
"""
function ft_phase_screen!(screen::AbstractArray{Complex{T},N}; r0=T(0.1), dx=T(2 / size(screen, 1)), dy=dx, l0=T(1e-2), L0=T(1e2), Nsh=0, q=3, centered=true, pwd=false, model=andrews, fftplan=plan_bfft!(screen, 1:2)) where {T<:Union{Float32,Float64},N}

    # setup the spectrum
    Φ = phase_spectrum(r0, l0, L0, model)

    # preallocate complex Fourier coefficients
    Nx, Ny = size(screen)
    cn = similar(screen)
    # number of screens
    Ntr = ntuple(d -> size(screen, d + 2), Val(N - 2))

    # frequency grid [rad/m] 
    dκx = T(2π / (Nx * dx))
    dκy = T(2π / (Ny * dy))
    κx = fftfreq(Nx, Nx * dκx)
    κy = fftfreq(Ny, Ny * dκy)

    # random uniform frequency offsets -> no DC remaining, instantiate on GPU for reproducible rng state
    δκx = similar(screen, T, 1, 1, Ntr...)
    δκy = similar(screen, T, 1, 1, Ntr...)
    randu_offsets!(δκx, δκy, dκx, dκy; pwd)

    # "Fourier" coefficients
    spectral_coefficients!(cn, Φ, κx, κy, dκx, dκy; δκx, δκy)

    # set DC component to zero: "Because the zero frequency  component of the phase-screen simulation alters only the total phase of the simulation, it does not change the spatial statistics of the fields, and therefore it is customary to set the zero frequency component to zeros" (Frehlich 2000). In case of random offsets, there is no DC component left  
    !(pwd && iszero(Nsh)) && begin
        I1 = CartesianIndex((1, 1, (1 for d ∈ 3:N)...))
        I2 = CartesianIndex((1, 1, Ntr...))
        cn[I1:I2] .= zero(Complex{T})
    end

    # synthesize the phase screen by superimposing the involved harmonics with an inverse DFT
    screenshifted = fftplan * cn

    # apply phase shift in spatial domain to compensate frequency shift in Fourier domain
    pwd && uniform_phase_correction!(screenshifted, dx, dy, δκx, δκy) 
    
    shift_screen!(screen, screenshifted; centered)
    
    # demean in case of PWD method
    pwd && demean_screen!(screen)

    # now, add subharmonics if we are supposed to do so
    if Nsh > 0
        # preallocate spatial grid arrays
        x = fftfreq(Nx, Nx * dx) |> (centered ? fftshift : identity)
        y = fftfreq(Ny, Ny * dy) |> (centered ? fftshift : identity)

        # preallocate spectral weight for subharmonics
        csh = similar(screen, Complex{T}, 1, 1, Ntr...)
        # preallocate frequency offset vectors
        δκx = similar(screen, T, 1, 1, Ntr...)
        δκy = similar(screen, T, 1, 1, Ntr...)

        # loop over frequency grids with spacing 1/(q^p*L)
        for p ∈ Base.OneTo(Nsh)
            # subharmonic frequency grid [rad/m] 
            dκx = T(2π / (Nx * dx * q^p))
            dκy = T(2π / (Ny * dy * q^p))
            κshx = fftfreq(q, q * dκx)
            κshy = fftfreq(q, q * dκy)

            for κy ∈ κshy, κx ∈ κshx
                # skip DC, also for PWD!
                iszero(κx) && iszero(κy) && continue

                # choose a new uniform random offset for each element of the summation
                randu_offsets!(δκx, δκy, dκx, dκy; pwd)

                # update κx and κy to contain the random offsets needed for the pwd method
                @. δκx += κx
                @. δκy += κy

                # generate coefficients
                randn!(csh)
                @. csh *= sqrt(2 * Φ(sqrt(δκx^2 + δκy^2)) * dκx * dκy)
                add_subharmonic!(screen, csh, x, y, δκx, δκy)
            end
        end

        # final (close-to-DC) contribution
        if pwd
            dκx = T(2π / (Nx * dx * q^(Nsh + 1)))
            dκy = T(2π / (Ny * dy * q^(Nsh + 1)))
            randu_offsets!(δκx, δκy, dκx, dκy; pwd)

            randn!(csh)
            @. csh *= sqrt(2 * Φ(sqrt(δκx^2 + δκy^2)) * dκx * dκy)
            add_subharmonic!(screen, csh, x, y, δκx, δκy)
        end

        demean_screen!(screen)
    end

    screen
end

function ft_phase_screen!(screen::AbstractArray{T,N}; r0=T(0.1), dx=T(2 / size(screen, 1)), dy=dx, l0=T(1e-2), L0=T(1e2), centered=true, model=andrews) where {T<:Union{Float32,Float64},N}

    # setup the spectrum
    Φ = phase_spectrum(r0, l0, L0, model)

    # preallocate complex Fourier coefficients
    Nx, Ny = size(screen)
    cn = similar(screen, Complex{T}, rft_size(size(screen)))

    # frequency grid [rad/m] 
    dκx = T(2π / (Nx * dx))
    dκy = T(2π / (Ny * dy))
    κx = rfftfreq(Nx, Nx * dκx)
    κy = fftfreq(Ny, Ny * dκy)

    # "Fourier" coefficients
    spectral_coefficients!(cn, Φ, κx, κy, dκx, dκy)

    sz = size(cn)
    I1 = CartesianIndex((1, 1, (1 for d ∈ 3:N)...))
    I2 = CartesianIndex((1, 1, (sz[d] for d ∈ 3:N)...))
    cn[I1:I2] .= zero(Complex{T})

    # synthesize the phase screen by superimposing the involved harmonics with an inverse DFT
    screenshifted = brfft(cn, Nx, 1:2)

    shift_screen!(screen, screenshifted; centered)

    screen
end

function nu_spectral_grid(N, dx::T; sym::Bool=true, fun::String="asinh", κmax=π / dx, κmin=sym ? -κmax : zero(T)) where {T<:Real}

    rngfun = if fun == "asinh"
        asinhrange
    elseif fun == "log"
        logrange_signed
    elseif fun == "sqrt"
        sqrtrange_signed
    else
        error("$fun sampling not implemented")
    end

    κ = collect(rngfun(κmin, κmax, N))
    # Compute the central differences
    dκ = central_diff(κ)

    κ, dκ
end


function nuft_phase_screen(; r0=0.1, Nx=256, dx=2 / Nx, dy=dx, l0=1e-2, L0=1e2, both::Bool=false, kwargs...)

    screen = Matrix{ComplexF64}(undef, Nx, Nx)
    nuft_phase_screen!(screen; r0, dx, dy, l0, L0, kwargs...)

    both ? reim(screen) : real(screen)
end


function nuft_phase_screen!(screen::AbstractArray{Complex{T},N}; r0=0.1, dx=2 / size(screen, 1), dy=dx, l0=1e-2, L0=1e2, Nκx=size(screen, 1), Nκy=size(screen, 2), centered::Bool=true, tol=1e-5, fun::String="asinh", nthreads=FFTW.get_num_threads(), upsampfac=2.0, model=andrews, kwargs...) where {T<:Union{Float32,Float64},N}

    # setup the spectrum
    Φ = phase_spectrum(r0, l0, L0, model)

    # preallocate complex fourier coefficients
    cn = similar(screen, Complex{T}, Nκx, Nκy, (size(screen, d) for d ∈ 3:N)...)

    # preallocate frequency grid
    # IDEA: double the variance and use only the positive frequency range?
    κx, dκx = Adapt.adapt(containertype(screen), nu_spectral_grid(Nκx, T(dx); sym=true, fun))
    κy, dκy = Adapt.adapt(containertype(screen), nu_spectral_grid(Nκy, T(dy); sym=true, fun))
    spectral_coefficients!(cn, Φ, κx, κy, dκx, dκy)

    # frequencies in range [-3π,3π] required ⟹ normalization
    κx .*= dx # frequencies in range [-3π,3π] required ⟹ normalization
    κy .*= dy
    grid = ndgrid(κx, κy)

    # CMCL mode ordering (neg. to pos.) ⟹ 0 OR FFTW mode ordering ⟹ 1
    modeord = Int(!centered)

    # synthesize the phase screen and make zero-mean
    thread_arg = containertype(screen) <: CuArray ? (;) : (; nthreads)
    nufft2d1!(grid..., cn, 1, tol, screen; upsampfac, modeord, thread_arg..., kwargs...)
    demean_screen!(screen)

    screen
end


function nuft_phase_screen_pol(; r0=0.1, Nx=256, dx=2 / Nx, dy=dx, l0=1e-2, L0=1e2, both::Bool=false, kwargs...)

    screen = Matrix{ComplexF64}(undef, Nx, Nx)
    nuft_phase_screen_pol!(screen; r0, dx, dy, l0, L0, kwargs...)

    both ? reim(screen) : real(screen)
end

function nuft_phase_screen_pol!(screen::AbstractArray{Complex{T},N}; r0=0.1, dx=2 / size(screen, 1), dy=dx, l0=1e-2, L0=1e2, Nκ=size(screen, 1), Nϕ=size(screen, 1), centered::Bool=true, tol=1e-5, fun="asinh", nthreads=FFTW.get_num_threads(), upsampfac=2.0, model=andrews, kwargs...) where {T<:Union{Float32,Float64},N}

    # setup the spectrum
    Φ = phase_spectrum(r0, l0, L0, model)

    # preallocate complex fourier coefficients
    cn = similar(screen, Complex{T}, Nϕ, Nκ, (size(screen, d) for d ∈ 3:N)...)

    # mean spatial resolution
    dxy = (dx + dy) / 2
    # preallocate frequency grid
    κ, dκ = Adapt.adapt.(containertype(screen), nu_spectral_grid(Nκ, T(dxy); sym=false, fun))
    ϕ = Adapt.adapt(containertype(screen), collect(range(T(0), length=Nϕ)))
    dϕ = T(2π / Nϕ)
    spectral_coefficients!(cn, Φ, κ, dκ, dϕ)

    # frequencies in range [-3π,3π] required ⟹ normalization
    κ .*= dxy
    ϕ .*= dϕ
    grid = ndgrid(ϕ, κ)
    pol2cart!(grid...)

    # CMCL mode ordering (neg. to pos.) ⟹ 0 OR FFTW mode ordering ⟹ 1
    modeord = centered ? 0 : 1

    # synthesize the phase screen and make zero-mean
    thread_arg = containertype(screen) <: CuArray ? (;) : (; nthreads)
    nufft2d1!(grid..., cn, 1, tol, screen; upsampfac, modeord, thread_arg..., kwargs...)
    demean_screen!(screen)

    screen
end



"""
    ssft_phase_screen!()

    Generate phase screens on a Nx by Ny square grid using the **sparse-spectrum** technique with Nκ spectral components.
    We use a power spectrum with inner scale l0 and outer scale L0. 
    The wavenumbers are log-uniformly distributed in the range of [κmin, κmax], where the uniform distributions leads to individual "rings," as discussed in Section 2.2.
    This code is based on Charnotskii, Mikhail. "Comparison of four techniques for turbulent phase screens simulation." JOSA A 37(5). 2020.
"""
function ssft_phase_screen!(screen::AbstractArray{Complex{T},N}; r0=0.1, dx=2 / size(screen, 1), dy=dx, l0=1e-2, L0=1e2, Nκ=500, centered::Bool=true, model=andrews, tol=1e-5, upsampfac=2.0, nthreads=FFTW.get_num_threads(), kwargs...) where {T<:Union{Float32,Float64},N}
    # TODO: Proper chunking not working due to different grids for each screen -> cannot be realized with nufft2d1...

    # setup the spectrum
    Φ = phase_spectrum(r0, l0, L0, model)

    # output grid size
    Nx, Ny = size(screen)
    Ntr = length(screen) ÷ (Nx * Ny)

    # Low- and high-frequency limit for SS algorithm
    dxy = max(dx, dy)
    κmin = T(π / 5L0)
    κmax = T(π / max(dxy, l0))

    # randomly distributed wavenumbers
    κ0 = generalized_range(log, exp, κmin, κmax, Nκ + 1)

    # κ = [sqrt(κ0[n]^2 + rand() * (κ0[n+1]^2 - κ0[n]^2)) for n ∈ 1:Nκ]
    # make sure to generate random numbers on GPU
    κ = similar(screen, T, Nκ)
    rand!(κ)
    @views begin
        @. κ *= (κ0[2:Nκ+1]^2 - κ0[1:Nκ]^2)
        @. κ += κ0[1:Nκ]^2
        @. κ = sqrt(κ)
    end

    # corresponding random phases
    ϕ = similar(screen, T, Nκ)
    rand!(ϕ)
    ϕ .*= 2π

    # spectral weights
    w = Adapt.adapt(containertype(screen), [quadgk(κ -> Φ(κ) * κ, κ0[n], κ0[n+1]; rtol=tol)[1] for n ∈ 1:Nκ])

    # random coefficients with spectral weights
    cn = similar(screen, Complex{T}, Nκ, Ntr)
    randn!(cn)
    @. cn *= sqrt(4π * w)

    # frequencies in range [-3π,3π] required ⟹ normalization
    κ .*= dxy
    grid = κ .* cos.(ϕ), κ .* sin.(ϕ)

    # CMCL mode ordering (neg. to pos.) ⟹ 0 OR FFTW mode ordering ⟹ 1
    modeord = Int(!centered)

    # synthesize the phase screen and make zero-mean
    thread_arg = containertype(screen) <: CuArray ? (;) : (; nthreads)
    nufft2d1!(grid..., cn, 1, tol, screen; upsampfac, modeord, thread_arg..., kwargs...)
    demean_screen!(screen)

    screen
end

"""
    suft_phase_screen!()

    Generate phase screens on a Nx by Ny square grid using the **sparse-uniform** technique with Nκ spectral components.
    We use a power spectrum with inner scale l0 and outer scale L0. 
    The wavenumbers are log-uniformly distributed in the range of [κmin, κmax], where the uniform distributions leads to individual "rings," as discussed in Section 2.2.
    This code is based on Charnotskii, Mikhail. "Comparison of four techniques for turbulent phase screens simulation." JOSA A 37(5). 2020.
"""
function suft_phase_screen!(screen::AbstractArray{Complex{T},N}; r0=0.1, dx=2 / size(screen, 1), dy=dx, l0=1e-2, L0=1e2, Nκ=500, centered::Bool=true, model=andrews, tol=1e-5, upsampfac=2.0, nthreads=FFTW.get_num_threads(), kwargs...) where {T<:Union{Float32,Float64},N}
    # TODO: Proper chunking not working due to different grids for each screen -> cannot be realized with nufft2d1...

    # setup the spectrum
    Φ = phase_spectrum(r0, l0, L0, model)

    # output grid size
    Nx, Ny = size(screen)
    Ntr = length(screen) ÷ (Nx * Ny)

    # Low- and high-frequency limit for SS algorithm
    dxy = max(dx, dy)
    κmin = T(π / 5L0)
    κmax = T(π / max(dxy, l0))

    # randomly distributed wavenumbers
    κ0 = generalized_range(log, exp, κmin, κmax, Nκ + 1)

    # κ = [sqrt(κ0[n]^2 + rand() * (κ0[n+1]^2 - κ0[n]^2)) for n ∈ 1:Nκ]
    # make sure to generate random numbers on GPU
    κ = similar(screen, T, Nκ)
    rand!(κ)
    @views begin
        @. κ *= (κ0[2:Nκ+1]^2 - κ0[1:Nκ]^2)
        @. κ += κ0[1:Nκ]^2
        @. κ = sqrt(κ)
    end

    # corresponding random phases
    ϕ = similar(screen, T, Nκ)
    rand!(ϕ)
    ϕ .*= 2π

    # random coefficients with spectral weights
    cn = similar(screen, Complex{T}, Nκ, Ntr)
    randn!(cn)
    @views @. cn *= sqrt(2π * Φ(κ) * (κ0[begin+1:end]^2 - κ0[begin:end-1]^2))

    # frequencies in range [-3π,3π] required ⟹ normalization
    κ .*= dxy
    grid = κ .* cos.(ϕ), κ .* sin.(ϕ)

    # CMCL mode ordering (neg. to pos.) ⟹ 0 OR FFTW mode ordering ⟹ 1
    modeord = Int(!centered)

    # synthesize the phase screen and make zero-mean
    thread_arg = containertype(screen) <: CuArray ? (;) : (; nthreads)
    nufft2d1!(grid..., cn, 1, tol, screen; upsampfac, modeord, thread_arg..., kwargs...)
    demean_screen!(screen)

    screen
end