using CUDA
using Dates

if CUDA.functional()
    maybe_cuda(x::AbstractArray) = CuArray(x)
else
    maybe_cuda(x::AbstractArray) = x
end

if CUDA.functional()
    maybe_cuda_seed!(i) = CUDA.seed!(i)
else
    maybe_cuda_seed!(i) = Random.seed!(i)
end

# get Array-type, i.e., CuArray or Array
containertype(::T) where {T} = T.name.wrapper


# generate current time prefix
timeprefix(t::DateTime=now(); fmt="YYYY-mm-dd-HH-MM-SS") = "$(Dates.format(t, fmt))_"

function cartgrid(M, dx, N, dy)
    x = fftfreq(M, M * dx)
    y = fftfreq(N, N * dy)

    ndgrid(x, y)
end

function polargrid(M, dx, N, dy)
    x = fftfreq(M, M * dx)
    y = fftfreq(N, N * dy)

    grid = ndgrid(x, y)
    cart2pol!(grid...)
    ϕ, r = grid
    r, ϕ
end


pol2cart(ϕ, r) = r .* sincos(ϕ) # returns (y, x)

# special CUDA kernel
function pol2cart_kernel!(ϕ, r)
    idx = threadIdx().x + (blockIdx().x - 1) * blockDim().x

    # safety-check as blocklen * nthreads might be larger than the number of array elements
    if idx ≤ length(ϕ)
        # Calculate Cartesian coordinates in place
        y, x = pol2cart(ϕ[idx], r[idx])

        # Write back into the input arrays: ϕ -> x, r -> y
        r[idx] = y
        ϕ[idx] = x
    end

    return
end

# special function operating in-place on 2d grid
function pol2cart!(ϕ, r) # ϕ -> x, r -> y
    @inbounds for k ∈ eachindex(ϕ, r)
        r[k], ϕ[k] = pol2cart(ϕ[k], r[k])# r[k] .* sincos(ϕ[k])
    end
end
function pol2cart!(ϕ::CuArray, r::CuArray)
    N = length(ϕ)
    threads = min(N, 1024)
    blocks = cld(N, threads)
    @cuda threads = threads blocks = blocks pol2cart_kernel!(ϕ, r)
end


cart2pol(x, y) = atan(y, x), hypot(x, y)

# special CUDA kernel
function cart2pol_kernel!(x, y)
    idx = threadIdx().x + (blockIdx().x - 1) * blockDim().x

    # safety-check as blocklen * nthreads might be larger than the number of array elements
    if idx ≤ length(x)
        # Calculate Cartesian coordinates in place
        ϕ, r = cart2pol(x[idx], y[idx])

        # Write back into the input arrays: x -> ϕ, y -> r
        x[idx] = ϕ
        y[idx] = r
    end

    return
end

# special function operating in-place on 2d grid
function cart2pol!(x, y) # x -> ϕ, y -> r
    @inbounds for k ∈ eachindex(x, y)
        x[k], y[k] = cart2pol(x[k], y[k])
    end
end
function cart2pol!(x::CuArray, y::CuArray)
    N = length(x)
    threads = min(N, 1024)
    blocks = cld(N, threads)
    @cuda threads = threads blocks = blocks cart2pol_kernel!(x, y)
end

function mean_exclude_dims(f, x::AbstractArray{T,N}, dims) where {T,N}
    mean(f, x; dims=Iterators.filter(!in(dims), 1:N))
end
function mean_exclude_dims(x::AbstractArray{T,N}, dims) where {T,N}
    mean(x; dims=Iterators.filter(!in(dims), 1:N))
end
mean_exclude_dims(f, x::AbstractArray{T,N}, ::Colon) where {T,N} = x
mean_exclude_dims(x::AbstractArray{T,N}, ::Colon) where {T,N} = x


"""
squeeze: remove singleton dimensions
"""
function squeeze(A::AbstractArray{T,N}) where {T,N}
    singleton_dims = tuple((d for d in 1:N if isone(size(A, d)))...)
    return dropdims(A; dims=singleton_dims)
end


"""
The central_diff function computes a central finite difference approximation of the first derivative of a given vector x. This method is particularly useful for approximating derivatives when dealing with discrete data points.

Arguments

    x::AbstractVector{T}: An abstract vector of any numeric type T, representing the data points from which the finite differences are computed.

Returns

A vector of the same type and length as x, containing the finite difference approximations. Specifically:

    For the first element, a forward difference is used: x_2 − x_1.
    For the last element, a backward difference is used: x_N − x_N−1​, where N is the length of the vector.
    For all other elements, a central difference is applied: (x_n+1 − x_n−1)/2, providing an approximation of the derivative at each internal point.
"""
function central_diff(x::AbstractVector{T}) where {T}
    
    N = lastindex(x)
    collect(T, (
        if n == firstindex(x)
            x[n + 1] - x[n]
        elseif n == lastindex(x)
            x[N] - x[N - 1]
        else
            (x[n+1] - x[n-1]) / 2
        end
        for n ∈ axes(x, 1)
    ))
end

"""
relrms(x, y; dims=:)

The relrms function calculates the relative error between two arrays x and y. It is useful for measuring the difference between a set of observed values (x) and reference values (y).

Arguments

    x: The observed values (can be any numeric type).
    y: The reference values (can be any numeric type). This value is compared to x to compute the relative error.

Returns

    If y is zero, the function returns missing, as the relative error is undefined in this case (division by zero).
    Otherwise, it returns the absolute relative error, calculated as ∣1−x/y∣​, which quantifies the difference between x and y in proportion to y.

"""
function relrms(x, y; dims=:) 
    relerr(x, y) = iszero(y) ? missing : 1 - x / y
    r = @. relerr(x, y)
    mean(abs2, r; dims) .|> sqrt
end


function remove_dumps(dir=pwd(), fext=".hdf5")
    fns = readdir(dir)
    # gather all hdf5 files in dir and all folders starting with a date in the YYYY-MM-DD format
    filterfun(x) = begin
        endswith(x, fext) || (isdir(x) && occursin(r"^\d{4}-\d{2}-\d{2}", x))
    end
    filter!(filterfun, fns)
    
    if !isempty(fns)
        # display all these files
        @info "Found the following files/folders in the directory $dir:"
        show(IOContext(stdout, :limit => false), "text/plain", fns)
        println("\n")

        @warn "Are you sure you want to delete all these files/folders? (y/N)"
        response = lowercase(strip(readline()))

        if response == "y"
            println("Deleting...")
            rm.(fns; recursive=true)
            return
        else
            println("Exiting...")
            return
        end
    end
end